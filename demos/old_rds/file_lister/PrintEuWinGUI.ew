-- Printer functions by Rod Damon

include Ptext.e -- contains printer text routines 
include EWGUties.ew -- utility functions and procedures for EuWinGUI
include wildcard.e

global sequence Paper_Layout
atom prnfont, prnfontI, headerfont

---------------------------------------------------------------------------
--**** This procedure returns the Printer paper sizes & printer font sizes
-- Default_Settings(Printer_Select, Style, Line_Width)

-- return settings stored in global sequence Paper_Layout are:
--  paper_width =   Paper_Layout[1][1][1] -- full page width
--  paper_length =  Paper_Layout[1][1][2] -- full page length
--  left_margin =   Paper_Layout[1][2][1] -- # of characters
--  top_margin =    Paper_Layout[1][2][2] -- # of lines
--  right_margin =  Paper_Layout[1][2][3] -- # of characters
--  bottom_margin = Paper_Layout[1][2][4] -- # of lines
--  fontX =         Paper_Layout[2][1] -- average X in points
--  fontY =         Paper_Layout[2][2] -- Y in points
--  fontMX =        Paper_Layout[2][3] -- max font X in points 
--  X_Page_Width =  Paper_Layout[3][1] -- max char per page width
--  Y_Page_Length = Paper_Layout[3][2] -- max lines per page length
--------------------------------------------------------------------
--***** Print the document
-- Print_Text(Title, doc, Printer_Select, Style, Line_Width)
    ---- Style = {fixed, bold, italic, underline}
--------------------------------------------------------------------
-- EuWinGUI printer routines

--SelectPrinter(atom boolflag)
--NewDocument(sequence documentname)
--NewPage()
--atom GetPageSize(atom reqdim)
--atom GetPrinterStringSize(sequence string, atom font, atom reqdim)
--SetPrinterPenColor(atom color)
--SetPrinterFont(atom fonthandle)
--PrintString(atom xpos, atom ypos, sequence string)
--PrintMB(atom mb, atom xpos, atom ypos, atom mbwidth, atom mbheight)
--StartPrinting()

global procedure Default_Settings(object Printer_Select, sequence Style, integer Line_Width)
--********* Set Default Printer Paper Layout 
    --global sequence Paper_Layout ** declared earlier
    --NOTE:  Line_Width of 120 =  8 point font
    --       Line_Width of 96  = 10 point font
    --       Line_Width of 80  = 12 point font
    --       Line_Width of 70  = 14 point font
    --Any Line_Width can be used as EWGUties.ew will adjust to fit
    
    integer charheight, charwidth, max_charwidth, pgw, pgh, CharsPerLine, LinesPerPage

    integer B, I, U

--  Style[1] Fixed font (1=yes)     
    B = Style[2] -- bold (1=yes)
    I = Style[3] -- italic (1=yes)
    U = Style[4] -- underlined (1=yes)

    CharsPerLine = 200 -- set a ridicules CharPerLine
    LinesPerPage = 100 -- set a ridicules LinesPerPage
        
    if equal("YES",upper(Printer_Select)) then
        SelectPrinter(False) -- Allow printer selection
    else
        SelectPrinter(True) -- Use default printer
    end if

    pgw = GetPageSize(X_DIM) -- page width

--global function GetFontLines(sequence fontfamilyname, atom bold, atom italic, atom underlined, atom desiredlinespp)
    if Style[1] = 1 then -- use FIXED font
    -- find characters per line font size
        while CharsPerLine>Line_Width+2 do
            prnfont = GetFontLines("Courier New",B,I,U,LinesPerPage) -- Fixed font
            max_charwidth = GetPrinterStringSize("Test lines", prnfont, X_DIM)/10
            CharsPerLine = floor(pgw / max_charwidth)
            if LinesPerPage = 100 then -- on first run estimate closer
                LinesPerPage = floor((100/CharsPerLine)*(Line_Width+2)) + 3
            end if
            LinesPerPage = LinesPerPage - 1
        end while
        prnfontI = GetFontLines("Courier New",B,1,U,LinesPerPage) -- Italic
    else -- use VARIABLE font
    -- find characters per line font size
        while CharsPerLine>Line_Width+2 do
            prnfont = GetFontLines("MS Sans Serif",B,I,U,LinesPerPage) -- variable font
            max_charwidth = GetPrinterStringSize("XXXXXXXXXX", prnfont, X_DIM)/10
            CharsPerLine = floor(pgw / max_charwidth)
            if LinesPerPage = 100 then -- on first run estimate closer
                LinesPerPage = floor((100/CharsPerLine)*(Line_Width+2)) + 3
            end if
            LinesPerPage = LinesPerPage - 1
        end while
        prnfontI = GetFontLines("MS Sans Serif",B,1,U,LinesPerPage) -- Italic
    end if

    SetPrinterFont(prnfont)

-- set up a header font using fixed font
    headerfont = GetFontLines("Courier New",1,0,1,LinesPerPage) -- Fixed font

-- Find the default sizes and store them in the default sequence Paper_Layout
    charheight = GetPrinterStringSize("Test line", prnfont, Y_DIM)
    charwidth = GetPrinterStringSize("Test lines", prnfont, X_DIM)/10
    pgw = GetPageSize(X_DIM) -- page width
    Paper_Layout[1][1][1] = pgw -- store page width
    pgh = GetPageSize(Y_DIM) -- page length
    LinesPerPage = floor((pgh/charheight))

    Paper_Layout[1][1][2] = pgh -- store page length
    Paper_Layout = {Paper_Layout[1]} -- Margins [1]
    Paper_Layout &= {{charwidth,charheight,max_charwidth}} -- font size [2]
    Paper_Layout &= {{CharsPerLine,LinesPerPage}} -- max width,max length [3]
end procedure

global procedure Print_Text(object Title, sequence doc, object Printer_Select, sequence Style, integer Line_Width)
    -- Title is the print Title 
    -- doc is the sequence to print
    -- Printer_Select is use default printer or select a printer "YES" "NO"
    -- Style is fixed font or variable font = {0,0,0,0}
                      -- {atom fixed, atom bold, atom italic, atom underlined}
    -- Line_Width is the number of characters per line
    
    integer wborder, hborder, lineheight, linespp, lp, charwidth,
            Top_Margin, Bottom_Margin, Left_Margin, Right_Margin, Page_Number

    sequence line
    
    if equal("YES",upper(Printer_Select)) then
        if Style[1] = 1 then
            Default_Settings("YES",Style,Line_Width) -- Allow printer selection/Fixed font
        else
            Default_Settings("NO",Style,Line_Width) -- Allow printer selection/Variable font
        end if
    end if

-- Get the page layout
    Top_Margin = Paper_Layout[1][2][2] -- # of lines
    Bottom_Margin = Paper_Layout[1][2][4] -- # of lines
    Left_Margin = Paper_Layout[1][2][1] -- # of characters
    Right_Margin = Paper_Layout[1][2][3] -- # of characters

-- Wrap lines to fit the page width - Left & Right margins
    doc = Wrap_Lines(doc, Line_Width-Left_Margin-Right_Margin+2)
        
-- Set up the print page for the printer
    lineheight = Paper_Layout[2][2] -- character height
    charwidth = Paper_Layout[2][3] -- max character width
    wborder = Left_Margin * charwidth -- left margin
    hborder = Top_Margin * lineheight -- top margin
    linespp = Paper_Layout[3][2]-Top_Margin-Bottom_Margin -- lines per page
    
--Now we have enough data to create a new printing document; the document's name is 
--only used as a reference
    NewDocument(Title)

--Once the document is created, we start printing on it all the lines
--lp is a counter indicating the lines already printed on the current page
    lp = 0
    
-- print the Title if used (longer 0)
    if length(Title) then
    --Print the line on the current page
        SetPrinterFont(headerfont)
        PrintString(wborder,hborder+(lineheight*lp),Title & " -- Page 1")
        Page_Number = 1
        lp = 2 -- skip a line
    end if

--Let's get the program's line to print
    for t = 1 to length(doc) do
        line = doc[t]
        --The line is a comment? Print it red, otherwise print it black
        if(match("--",Ltrim(line)) = 1) then
          SetPrinterFont(prnfontI) -- Italic
          SetPrinterPenColor(CL_RED)
        else
          SetPrinterFont(prnfont)
          SetPrinterPenColor(CL_BLACK)
        end if

-- check for doc form feed line
        if length(line)>0 and (line[1]=12 or line[1]=127 or line[1]=128) then -- form feed 
    --...start a new blank one and reset the line counter
            NewPage()
            lp = 0
        else
    --Print the line on the current page
            PrintString(wborder,hborder+(lineheight*lp),line)
            lp += 1
    --If the current page has been filled up...
            if lp >= linespp then
    --...start a new blank one and reset the line counter
                NewPage()
                lp = 0
        -- print the Title if used (longer 0)
                if length(Title) then
            --Print the line on the current page
                    Page_Number += 1
                    SetPrinterFont(headerfont)
                    PrintString(wborder,hborder+(lineheight*lp),Title & " -- Page " & sprint(Page_Number))
                    lp = 2 -- skip a line
                end if
            end if
        end if  
    end for

--Once all the lines has been sent to the printer, we can actually close the document
--and start drawing it on paper
    StartPrinting()

end procedure

-- initialize the printer Paper_Layout sequence upon starting
Paper_Layout = {}
Paper_Layout &= {{{0,0},{0,0,0,0}}} -- No Margins [1]
    -- start out with default printer & fixed font
    -- Default_Settings(Printer_Select, Style, Line_Width)
Default_Settings("NO" , {1,0,0,0} ,80)

----***********************************
--- Sample program usage

--sequence doc

--doc = Get_File("PrintEuWinGUI.ew")

--if length(doc) then
    ---- Print_Text(Title, doc, Printer_Select, Style, Line_Width)
    ---- Style = {fixed, bold, italic, underline}
--  Print_Text("PrintEuWinGUI.ew",doc,"YES",{1,0,0,0},80)
--end if
