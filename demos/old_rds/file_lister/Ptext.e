-- By Rod Damon
----------- Ptext.e General text printer & file path routines rev. 09/22/2005

---------------------------------------------------------------------------
--global function Get_File(sequence fName)
    -- get a sequence file and convert TABS to spaces, check for form feeds
---------------------------------------------------------------------------
--global function Break_Text( sequence text ) --- from WinLib32 generic.ex
    -- text returned from a MLE is delimited with CrLf.
    -- break the text into seperate lines
    -- skip added page break lines
---------------------------------------------------------------------------
--global function Make_MLE(sequence text)
    -- convert a sequence file to a MLE file
---------------------------------------------------------------------------
--global function Wrap_Lines(sequence doc, integer max_width)
    -- wrap long lines to printer paper max width of characters per line
---------------------------------------------------------------------------
--global function Page_Breaks(sequence text, integer max_lines, integer add)
    -- add=1 add page breaks to a text file based on max lines per printer page
    -- add=0 remove page breaks from the sequence
---------------------------------------------------------------------------
---------------------------------------------------------------------------
-- Library's Purpose:  Path String Manipulation

-- Copyright 1998 : Mathew Hounsell
-- Version : 1.0
-- Licence : FreeCode
---------------------------------------------------------------------------
-- NAME    : split_path_file()
-- PURPOSE : Determines a files name and from it's full path.
-- ARGS    : sequence s - the files full path.
-- RETURNS : sequence - in the form { path, filename }
-- NOTES   : If it find no '\' path is empty.
--           If there is a path it will be returned ending in a '\'.
--           If there is a drive letter at start of s it will be returned
--              as is at the start of the path.
---------------------------------------------------------------------------
-- NAME    : split_name_ext()
-- PURPOSE : Gets a files name and it's extension from it's full file name.
-- ARGS    : sequence s - the full full name.
-- RETURNS : sequence - of the form { name, extension }
-- NOTE    : If it doesn't find a '.' the extension is empty.
--           If there is an extension it will be returned preceded by a '.'.
---------------------------------------------------------------------------
-- NAME    : split_drive_path()
-- PURPOSE : Gets a files drive from it's full path.
-- ARGS    : sequence s - The files full path.
-- RETURNS : sequence - Of the form { drive, path }
-- NOTES   : If there is no ':' the drive will be returned as empty.
--           If there is a drive it will be returned ending in a ':'.
---------------------------------------------------------------------------
-- NAME    : split_drive_path_name_ext()
-- PURPOSE : Determines a files drive, path, name and extenssion from
--              it's full path.
-- ARGS    : sequence s - The files full path.
-- RETURNS : sequence - Of the form { drive, path, name, extension }
-- NOTES   : If it was unable to find a part it will be empty.
--           If there is a path it will be returned ending in a '\'.
--           If there is a drive it will be returned ending in a ':'.
--           If there is an extension it will be returned preceded by a '.'.
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --

----------------------------------------------------
-- Standard string definitions from - strings.e
----------------------------------------------------
function Rtrim(sequence a)
-- trim spaces, tabs, \r, \n from right side of text

    integer c, x
    x = length(a)
    while x>0 do
        c = a[x]
-- if not a space or tab or carriage return or new line
        if not (c=' ' or c=9 or c=10 or c=13) then
            exit
        else
            x -= 1
        end if
    end while
    if x>0 then
        a = a[1..x]
        return a
    else
        return {}
    end if
end function
----------------------------------------------------
global function Ltrim(sequence a)
-- ltrim trim white spaces & tabs from left side

    integer i, x, c
    i = length(a)
    x = 1
    while x<=i do
        c = a[x]
        if not (c=' ' or c=9) then
        exit
        else
        x = x+1
        end if
    end while
    a = a[x..i]
    return a
end function
----------------------------------------------------
global function Padleft(sequence s, integer t)
-- pad left side of string with spaces to length t

    while length(s)<t do
        s=" " & s
    end while
    return s
end function
----------------------------------------------------
global function Padright(sequence s, integer t)
-- pad right side of string with spaces to length

    while length(s)<t do
        s=s & " "
    end while
    return s
end function
----------------------------------------------------
global function Centerit(sequence s, integer t)
-- center a string between length t

    atom sl, -- length s/2
     tl  -- length t/2
    sl=length(s)*.5
    tl=t*.5

    while length(s)<(tl+sl) do
        s=" " & s
    end while
    return s
end function
----------------------------------------------------

global function Get_File(sequence fName)
-- get a sequence file and convert TABS to spaces, check for form feeds
    integer fn
    object s
    sequence text
    integer tabpos
    integer tabcnt
    integer vTabSize -- number of spaces for tabs 
            vTabSize = 8
                
    fn = open(fName, "r")
    text = ""
    if fn = -1 then
        return text
    end if
    text = {}
    s = gets(fn)
    while sequence(s) do
    -- Convert TABS to spaces
        tabpos = find(9, s) -- \t
        while tabpos != 0 do
            tabcnt = vTabSize - remainder(tabpos, vTabSize)
            s = s[1..tabpos-1] & repeat(' ', tabcnt) & s[tabpos+1.. length(s)]
            tabpos = find(9, s)
        end while
    -- add page break if ascii 12 or ascii 127 is found
        if find(12,s)=1 or find(127,s)=1 or find(128,s)=1 then
            s = 12 & "................................ file page break ..........."
        end if

        text = append(text,Rtrim(s))
        s = gets(fn)
    end while
    close(fn)
    return text
end function

global function Break_Text( sequence text ) --- from WinLib32 generic.ex
    -- text returned from a MLE is delimited with CrLf.
    -- break the text into seperate lines
    -- skip added page break lines
    
    integer at
    sequence doc
    sequence lEOL
    sequence CrLf
    
    CrLf = {'\r','\n'}

    -- convert into lines
    at = match( CrLf, text )
    if at != 0 then
        lEOL = CrLf
    else
        at = match( {13}, text )
        if at != 0 then
            lEOL = {13}
        else
            lEOL = {10}
        end if
    end if
    doc = {}

    while (1 = 1) do
    
        -- look for an end of line
        at = match( lEOL, text )
        if at = 0 then            
        
            -- any text remaining?
            if length( text ) then
                doc = append( doc, text )
            end if
            
            -- leave loop
            exit         
            
        end if

        -- add text to document if not an added page break
        if match("... page break #",text[1..at])=0 then       
            doc = append( doc, text[1..at-1] )
        end if
        
        -- remove from text
        text = text[at+length(lEOL)..length( text )]
        
    end while

    return doc
end function

global function Make_MLE(sequence text)
-- convert a sequence file to a MLE file
    object s
    sequence MLE
    
    MLE = {}
    for i = 1 to length(text) do
        s = text[i]
    -- 1st right trim the line then
    -- Add CrLf to each line for MLE file          
        s = Rtrim(s) & {13,10} -- CrLf \r\n
        MLE &= s        
    end for

    return MLE
end function

global function Wrap_Lines(sequence doc, integer max_width)
-- wrap long lines to printer paper max width
    sequence text, text_line
    integer X_Page_Width

    X_Page_Width = max_width -- max char per page width 
    text = {}

    for i=1 to length(doc) do
        doc[i] = Rtrim(doc[i])
        if length(doc[i])>X_Page_Width then
            text_line = {}
            while length(doc[i]) > X_Page_Width do
                -- text line is longer than max page_width
                for ii=X_Page_Width to 1 by -1 do
                    if doc[i][ii]=' ' then -- find the 1st space < page_width
                        text_line = append(text_line,doc[i][1..ii-1]) 
                        doc[i] = doc[i][ii+1..length(doc[i])]
                        exit
                    elsif ii=1 then -- No Spaces found < page_width
                        text_line = append(text_line,doc[i][1..X_Page_Width])
                        doc[i] = doc[i][X_Page_Width+1..length(doc[i])]
                    end if              
                end for         
            end while       
            for ii=1 to length(text_line) do
                text = append(text,text_line[ii])
            end for
            text = append(text,doc[i])
        else
            text = append(text,doc[i])
        end if
    end for

    return text
end function

global function Page_Breaks(sequence text, integer max_lines, integer add)
-- add or remove page breaks to a text file based on max lines per printer page
    sequence doc
    integer Y_Page_Length, line, page
    object text_line, page_break
    
    Y_Page_Length = max_lines -- max lines per page length

    if length(text)<=Y_Page_Length then
        return text
    end if

    doc = {}
    line = 1
    page = 1

    if add=1 then -- add page breaks to text file
        for i = 1 to length(text) do
            text_line = text[i]
    
            if find(12,text_line)=1 or find(127,text_line)=1 or find(128,text_line)=1 then -- file page break
                page += 1
                line = 0                        
            end if

            if line>Y_Page_Length then -- add page break line
                page_break = 12 & sprintf("....................... page break #%d ...........",{page})
                doc = append(doc,page_break)
                page += 1
                line = 1
            end if
            doc = append(doc,text_line)
            line += 1                       
        end for
    elsif add=0 then -- remove added page break lines
        for i = 1 to length(text) do
            text_line = text[i]
            if match(" page break",text_line) and find(12,text_line)=1 then
                if match("................................ file page break ...........",text_line)=2 then
                    doc = append(doc,12) -- add line only if a file page break
                end if
            else
                doc = append(doc,text_line)             
            end if
        end for
    end if
        
    return doc
end function

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
-- Library's Purpose:  Path String Manipulation

-- Copyright 1998 : Mathew Hounsell
-- Version : 1.0
-- Licence : FreeCode

-- For assistance of Euphoria Programmers.

-- For DOS/WINDOWS style file naming system. Supports long file names.

---------------------------------------------------------------------------
-- NAME    : split_path_file()
-- PURPOSE : Determines a files name and from it's full path.
-- ARGS    : sequence s - the files full path.
-- RETURNS : sequence - in the form { path, filename }
-- NOTES   : If it find no '\' path is empty.
--           If there is a path it will be returned ending in a '\'.
--           If there is a drive letter at start of s it will be returned
--              as is at the start of the path.
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
global function split_path_file(sequence s)
sequence val

    val = {}
    for t = length(s) to 1 by -1 do
        if s[t] = '\\' then
            val = {s[1..t],s[t+1..length(s)]}
            exit
        end if
    end for
    if not length(val) then
        val = { {}, s }
    end if
    return val
end function

---------------------------------------------------------------------------
-- NAME    : split_name_ext()
-- PURPOSE : Gets a files name and it's extension from it's full file name.
-- ARGS    : sequence s - the full full name.
-- RETURNS : sequence - of the form { name, extension }
-- NOTE    : If it doesn't find a '.' the extension is empty.
--           If there is an extension it will be returned preceded by a '.'.
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
global function split_name_ext(sequence s)
sequence val

    val = {}
    for t = length(s) to 1 by -1 do
        if s[t] = '.' then
            val = {s[1..t-1],s[t..length(s)]}
            exit
        end if
    end for
    if not length(val) then
        val = { s , {} }
    end if
    return val
end function

---------------------------------------------------------------------------
-- NAME    : split_drive_path()
-- PURPOSE : Gets a files drive from it's full path.
-- ARGS    : sequence s - The files full path.
-- RETURNS : sequence - Of the form { drive, path }
-- NOTES   : If there is no ':' the drive will be returned as empty.
--           If there is a drive it will be returned ending in a ':'.
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
global function split_drive_path(sequence s)
sequence val

    val = {}
    for t = 1 to length(s) by 1 do
        if s[t] = ':' then
            val = {s[1..t],s[t+1..length(s)]}
            exit
        end if
    end for
    if not length(val) then
        val = { {}, s }
    end if
    return val
end function

---------------------------------------------------------------------------
-- NAME    : split_drive_path_name_ext()
-- PURPOSE : Determines a files drive, path, name and extension from
--              it's full path.
-- ARGS    : sequence s - The files full path.
-- RETURNS : sequence - Of the form { drive, path, name, extension }
-- NOTES   : If it was unable to find a part it will be empty.
--           If there is a path it will be returned ending in a '\'.
--           If there is a drive it will be returned ending in a ':'.
--           If there is an extension it will be returned preceded by a '.'.
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
global function split_drive_path_name_ext(sequence s)
object val

    val = {}
    s = split_path_file(s)
    if length(s) = 2 then
        val = split_drive_path(s[1]) & split_name_ext(s[2])
    else
        val = { {}, {}, {}, s }
    end if
    return val
end function

---------------------------------------------------------------------------
-- End Of Code and File
---------------------------------------------------------------------------
