Public Domain Feb. 2002

I have wrapped some functions from msvfw32.dll (microsoft video for windows)
to get access to (what I used to think was) fast dib drawing & palette routines.
The DIB's made can be read/written directly in memory. However, only a 256 palette DIB is made
therefore each pixel = 1 byte. I have experimented with 32bit pixel DIBs and
its very easy to add to this library but it will slug performance severely. You could
make your own 32bit dibs by altering the dibdraw.e code...

The library can work with both Win32lib & EuWinGui but the demos have been written for
the latter. However, it is not difficult to convert the demo code to the former.

If you discover any bugs or have some other useful ideas for improvement then
please send 'em to me.

vulcan@win.co.nz

to do list
==========
- add setPixel & getPixel with enforced clipping
- remove access to memory address because will not then need it
- write some documentation
- make (cool) progress_bar demo
- (perhaps) add routine to read a bitmap from disk and put into a dib


6.2.2002 - added 2 more demos and included all demo code from previous releases
========

31.12.2001 - revision
==========
1. Will now work with both win32lib.ew and Andrea's EuWinGui.ew but win32lib users
must attach namespace modifier to getDC() & releaseDC() when copying to a window.
NB: It does use 2 include files that were with win32lib suite - do I have to give someone credit or something..?

2. No longer requires the width to be a multiple_of_4, DIB creation routine will do this automatically

3. Some alterations to routines have occurred, not that it probably affects anyone..

4. Routines to get the start or finish address of a line have been added to ease use

5. The id returned is the actual windows handle and not my internal index number as was previously.

6. A DIB created is actually a bottom-up entity whereas the screen is top-down. To overcome this
likly confusing situation, the routines that get addresses of lines account for the juxtaposition
so the user can think of the DIB with the same orientation as the Screen



Some important things to note:
==============================

DIB's are comparatively slow when copying to the screen. If you need ultimate game speed then
you'd be better off using some directX library, still, you're not going to get the simplicity of
this library - which ain't all that slow - for windows anyway :-)

This code will work in trucolor, hicolor & 256color modes. However, please note that when
*animating* the palette in 256color a lot of psychedelic flickering will occur as the foreground
& background palettes are manically remapped. Yet, 256color mode is the fastest for displaying
an image, probably  because there is less video memory to deal with. The table below shows some
time tests to scroll a full screen at 800 x 600 resolution

Screen mode-> 8bit  16bit  32bit
====================================
8 bit DIB       15    19    32    -- time in seconds
32 bit DIB     100    58    61    --  "   "   "   "

The DIB's made are actually "DIB Sections" distinguished by the fact that their memory is
directly accessible to a programmer but plain old DIB's can only be accessed by functions
or something to access *lone* pixels (in the case of get/set pixel).

The user is supposed to call GdiFlush() prior to each read/write operation
but I have not found this to be a problem, actually where is GdiFlush() ?

Access to the image data is via poke, peek, mem_set etc.. but no clipping is done.

Access to the palette is via the convenient palette functions.

Each dib should have its own dib dc (see drawDibOpen() below).

The lines are also ONE-based as per the Euphoria convention. This may be changed in the future

I may in the future add setPixel, getPixel in the likeness of Euphoria functions

256 colours might be limiting in a lot of situations but some relief could be possible by
creating several DIBs plus some tricky code..


DIB creation
============
drawDibOpen() -- creates a DIB DC to be used in conjunction with a DIB
drawDibClose() -- closes a DIB DC & frees resources

createDIBsection() -- creates a memory accessible bitmap, returns windows Handle
deleteDIBsection() -- deletes the DIB and frees resources


DIB manipulation
=================
getLineStartAddress() -- returns the memory address where a line starts
getLineFinishAddress() -- returns the memory address where a line finishes
peek(), poke(), mem_set(), mem_copy() -- as you'd expect

drawing to the Screen
=====================
drawDib() (see stretchDIB too)
-- multitalented routine to kinda blit the DIB to the screen
-- it will also do stretching. If you update the palette then you should call
-- this routine to reflect the new colours	

getImageAddress()-- gets the memory address where the image starts
getDC() -- needed to copy DIB to screen. Requires modifier for win32lib
releaseDC() -- needed to copy DIB to screen. Requires modifier for win32lib


Palette functions
=================
getDibPalette() -- Get up to 256 palette entries
setDibPalette() -- Set up to 256 palette entries






