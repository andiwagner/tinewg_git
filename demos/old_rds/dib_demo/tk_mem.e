-- modified for use with tinewg and Phix 0.8.1

-- pAllocated renamed to ppAllocated 
-- peek_string renamed to peek_string for Phix
 
without warning
without trace
--/*
include machine.e
include dll.e
--*/
include tk_misc.e
constant
HEAP_NO_SERIALIZE=#00000001,
HEAP_GROWABLE=#00000002,
HEAP_GENERATE_EXCEPTIONS=#00000004,
HEAP_ZERO_MEMORY=#00000008,
HEAP_REALLOC_IN_PLACE_ONLY=#00000010,
HEAP_TAIL_CHECKING_ENABLED=#00000020,
HEAP_FREE_CHECKING_ENABLED=#00000040,
HEAP_DISABLE_COALESCE_ON_FREE=#00000080,
HEAP_CREATE_ALIGN_16=#00010000,
HEAP_CREATE_ENABLE_TRACING=#00020000,
kernel32=open_dll("kernel32.dll"),
xHeapCreate=define_c_func(kernel32,"HeapCreate",{C_LONG,C_LONG,C_LONG},C_LONG),
xHeapDestroy=define_c_func(kernel32,"HeapDestroy",{C_LONG},C_LONG),
xHeapAlloc=define_c_func(kernel32,"HeapAlloc",{C_LONG,C_LONG,C_LONG},C_LONG),
xHeapFree=define_c_func(kernel32,"HeapFree",{C_LONG,C_LONG,C_LONG},C_LONG)
integer pAbortRtn pAbortRtn=-1
integer pAllocationCounter pAllocationCounter=0
sequence ppAllocated ppAllocated={{},{}}
constant kOwner=1
,kAddr=2
integer pAllotted pAllotted=0
integer pStringSet pStringSet=0
atom pHeap pHeap=0
global function llSetAbort(integer i)
integer lOldRtn
lOldRtn=pAbortRtn
pAbortRtn=i
return lOldRtn
end function
procedure myFree(atom pAddress)
object VOID
if pHeap=0 then
return
end if
VOID=c_func(xHeapFree,{pHeap,0,pAddress})
pAllocationCounter-=1
if pAllocationCounter=0 then
VOID=c_func(xHeapDestroy,{pHeap})
pHeap=0
end if
end procedure
function myAllocate(integer pSize)
atom lAddr
if pHeap=0 then
pHeap=c_func(xHeapCreate,{0,16000,0})
if pHeap=0 then
return 0
end if
end if
lAddr=c_func(xHeapAlloc,{pHeap,HEAP_ZERO_MEMORY,pSize})
pAllocationCounter+=1
return lAddr
end function
global constant
Byte=-1,
Word=-2,
Integer=Word,
Long=-3,
DWord=Long,
UInt=Long,
Ptr=Long,
Lpsz=-4,
Hndl=-5,
HndlAddr=-6,
Strz=-7
constant vSizeNames={Byte,Word,Long,Lpsz,Hndl,HndlAddr,Strz}
constant vSizeLengs={1,2,4,4,4,4,1}
global procedure manage_mem(atom pOwner,atom pAddr)
integer lOwnerSub,lAddrSub
lAddrSub=0
for i=1 to length(ppAllocated[kAddr])do
lAddrSub=find(pAddr,ppAllocated[kAddr][i])
if lAddrSub!=0 then
ppAllocated[kAddr][i]=removeIndex(lAddrSub,ppAllocated[kAddr][i])
exit
end if
end for
lOwnerSub=find(pOwner,ppAllocated[kOwner])
if lOwnerSub=0 then
ppAllocated[kOwner]&=pOwner
lOwnerSub=length(ppAllocated[kOwner])
ppAllocated[kAddr]=append(ppAllocated[kAddr],{})
end if
ppAllocated[kAddr][lOwnerSub]&=pAddr
end procedure
global function acquire_mem(atom pOwner,object pData)
atom at
if sequence(pData)then
at=myAllocate(1+length(pData))
if at!=0 then
poke(at,pData)
poke(at+length(pData),0)
end if
else
if pData<0 then
pData=find(pData,vSizeNames)
if pData!=0 then
pData=vSizeLengs[pData]
end if
end if
if pData<4 then
pData=4
end if
at=myAllocate(pData)
if at!=0 then
mem_set(at,0,pData)
end if
end if
if at=0 then
if pAbortRtn>=0
then
call_proc(pAbortRtn,{"Unable to allocate space.",2})
end if
else
manage_mem(pOwner,at)
end if
return at
end function
global procedure release_mem(atom pData)
integer lOwnerSub
integer lAddrSub,lAddrList
atom lAddrToRelease
integer ls,ss,Phase1
integer debugi
sequence sets
if pData=-1 then
return
end if
lOwnerSub=find(pData,ppAllocated[kOwner])
if lOwnerSub=0 then
lAddrSub=0
for i=1 to length(ppAllocated[kAddr])do
lAddrSub=find(pData,ppAllocated[kAddr][i])
if lAddrSub!=0 then
lAddrList=i
exit
end if
end for
if lAddrSub=0 then
if pAbortRtn>=0 then
call_proc(pAbortRtn,{"Trying to release unacquired memory",2})
end if
else
ppAllocated[kAddr][lAddrList]=removeIndex(lAddrSub,
ppAllocated[kAddr][lAddrList])
end if
myFree(pData)
return
end if
sets={pData}
ss=1
Phase1=1
while Phase1 do
ls=length(sets)
for i=ss to ls do
lOwnerSub=find(sets[i],ppAllocated[kOwner])
for j=1 to length(ppAllocated[kAddr][lOwnerSub])do
lAddrSub=find(ppAllocated[kAddr][lOwnerSub][j],
ppAllocated[kOwner])
if lAddrSub!=0 then
sets&=ppAllocated[kOwner][lAddrSub]
end if
end for
end for
ss=ls+1
Phase1=(ls!=length(sets))
end while
for i=length(sets)to 1 by-1 do
lOwnerSub=find(sets[i],ppAllocated[kOwner])
for j=1 to length(ppAllocated[kAddr][lOwnerSub])do
if ppAllocated[kAddr][lOwnerSub][j]!=0 then
myFree(ppAllocated[kAddr][lOwnerSub][j])
end if
end for
end for
if sets[1]!=0 then
myFree(sets[1])
end if
for i=1 to length(sets)do
lOwnerSub=find(sets[i],ppAllocated[kOwner])
ppAllocated[kAddr]=removeIndex(lOwnerSub,ppAllocated[kAddr])
ppAllocated[kOwner]=removeIndex(lOwnerSub,ppAllocated[kOwner])
for j=1 to length(ppAllocated[kAddr])do
lOwnerSub=find(sets[i],ppAllocated[kAddr][j])
if lOwnerSub!=0 then
ppAllocated[kAddr][j]=removeIndex(lOwnerSub,
ppAllocated[kAddr][j])
exit
end if
end for
end for
end procedure
global procedure release_all_mem()
while length(ppAllocated[kOwner])!=0 do
release_mem(ppAllocated[kOwner][1])
end while
end procedure
global function allot(object pDataType)
integer soFar,diff,size,i,lCnt
if sequence(pDataType)then
lCnt=pDataType[1]
i=pDataType[2]
else
lCnt=1
i=pDataType
end if
soFar=pAllotted
size=find(i,vSizeNames)
if size=0
then
if i>0 then
diff=remainder(soFar,4)
if diff then
soFar=soFar+4-diff
end if
size=i
end if
else
size=vSizeLengs[size]
end if
pAllotted+=(size * lCnt)
return{soFar,i,lCnt}
end function
global function allotted_handle(sequence pHandle)
if length(pHandle)=3
and
pHandle[2]=Hndl
then
return{pHandle[1],HndlAddr,pHandle[3]}
else
return{}
end if
end function
global function allotted_size()
integer soFar
soFar=pAllotted
pAllotted=0
return soFar
end function
global procedure store(atom struct,sequence s,object o)
integer where,datatype,lCnt
atom at
sequence bytes
where=s[1]+struct
datatype=s[2]
lCnt=s[3]
if sequence(o)then
if length(o)<lCnt then
lCnt=length(o)
end if
end if
if datatype=Byte then
if atom(o)then
poke(where,o)
else
poke(where,o[1..lCnt])
end if
elsif datatype=Word then
bytes=int_to_bytes(o)
poke(where,bytes[1..2])
elsif datatype=Long then
if atom(o)then
poke4(where,o)
else
poke4(where,o[1..lCnt])
end if
elsif datatype=Lpsz then
if atom(o)then
poke4(where,o)
else
poke4(where,acquire_mem(struct,o))
end if
elsif datatype=Hndl then
if atom(o)then
poke4(where,o)
else
poke4(where,o[1..lCnt])
end if
elsif datatype=HndlAddr then
at=acquire_mem(struct,4)
poke4(at,o)
poke4(where,at)
elsif datatype=Strz then
bytes=o&0
if lCnt<s[3]then
lCnt+=1
end if
poke(where,bytes[1..lCnt])
else
poke(where,o[1..lCnt])
end if
end procedure
integer pPeekStringBufSize pPeekStringBufSize=256
global function ppeek_string(atom a)
integer i,l,sl
sequence s
s={}
sl=0
l=0
if a then
i=peek(a)
while i do
l+=1
if sl<l then
s&=repeat(0,pPeekStringBufSize)
sl+=pPeekStringBufSize
end if
s[l]=i
a+=1
i=peek(a)
end while
end if
return s[1..l]
end function
global procedure set_peek_string(integer newsize)
if newsize<1
then
if newsize=0 then
newsize=1
else
newsize=256
end if
end if
pPeekStringBufSize=newsize
end procedure
global function peek_handle(atom a)
return peek4u(a)
end function
global function fetch(atom struct,sequence s)
integer size,char,cnt
atom at
at=struct+s[1]
size=s[2]
cnt=s[3]
if size=Byte then
if cnt>1 then
return(peek({at,cnt}))
else
return peek(at)
end if
elsif size=Word then
if cnt=1 then
return bytes_to_int(peek({at,2})&{0,0})
else
s={}
for i=1 to cnt do
s&=bytes_to_int(peek({at,2})&{0,0})
at+=2
end for
return s
end if
elsif size=Long then
if cnt=1 then
return peek4s(at)
else
s={}
for i=1 to cnt do
s&=peek4s(at)
at+=4
end for
return s
end if
elsif size=Lpsz then
at=peek4u(at)
return peek_string(at)
elsif size=Hndl then
if cnt=1 then
return peek4u(at)
else
s={}
for i=1 to cnt do
s&=peek4u(at)
at+=4
end for
return s
end if
elsif size=HndlAddr then
at=peek4u(at)
at=peek_handle(at)
return at
elsif size=Strz then
return peek_string(at)
else
return(peek({at,size}))
end if
end function
global function address(atom addr,object offset)
if atom(offset)then
return addr+offset
elsif length(offset)=3 then
return addr+offset[1]
else
return 0
end if
end function
global function new_memset()
atom ms
ms=acquire_mem(0,UInt)
return ms
end function

