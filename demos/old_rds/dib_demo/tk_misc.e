-- modified for use with tinewg and Phix 0.8.1

--global constant
--False=(1=0),
--True=(1=1)
without warning

global function findKey(object key,sequence list)
for i=1 to length(list)do
if equal(list[i][1],key)then
return i
end if
end for
return 0
end function
global function removeIndex(integer index,sequence list)
integer len,a,z
len=length(list)
if index<1 or index>len then
return list
elsif index=1 then
a=2
z=len
elsif index=len then
a=1
z=len-1
elsif index<(len-index)then
list[2..index]=list[1..index-1]
a=2
z=len
else
list[index..len-1]=list[index+1..len]
a=1
z=len-1
end if
return list[a..z]
end function
global function insertIndex(integer index,sequence list,object pData)
integer len
len=length(list)+1
if index<1 then
return prepend(list,pData)
elsif index>=len then
return append(list,pData)
elsif index<(len-index)then
list=0&list
list[1..index-1]=list[2..index]
else
list&=0
list[index+1..len]=list[index..len-1]
end if
list[index]=pData
return list
end function
global function iff1(atom test,object ifTrue,object ifFalse)
if test then
return ifTrue
else
return ifFalse
end if
end function
global function removeItem(object item,sequence list)
return removeIndex(find(item,list),list)
end function
global function removeKey(object item,sequence list)
return removeIndex(findKey(item,list),list)
end function

