Installation
~~~~~~~~~~~~

Unzip the zip file into a suitable directory, created if required.
The examples assume that you have unzipped the files into 'euphoria/dbgui',
with a subordinate directory called 'examples'. The files idb.e, database3.e
and EuWinGUI.ew (by Andrea Cini) can be in this installation directory,
but will need to be copied or moved into the euphoria/include directory if
you wish to use the IDB interface and Windows GUI interface from other
directories. By default any databases created will be in the same directory
as the calling program, although this may be changed by amending a parameter
or fully specifying the file name.

New for version 0.8:
1) Minor bug fixes to idb.e
2) Updated to work with Euphoria 2.4
3) "Rename Table" command added
4) database.e extended to include additional code, and named database3.e
5) Documentation updated

A.C.Harper
22 February 2003

