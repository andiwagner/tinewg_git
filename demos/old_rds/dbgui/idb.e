-- February 2003
---------------------------------------------------------------------------------------------------
-- IDB v0.8 - interface program for EDS - uses a slightly extended version of EDS, database3.e  
-- which is based on the alpha release EDS version released 22 February 2003
---------------------------------------------------------------------------------------------------
-- DISCLAIMER                                                                                 
--                                                                                             
-- This program is distributed in the hope that it will be useful, but without any warranty;  
-- without even the implied warranty of merchantability or fitness for a particular purpose.                                                                                                  --
-- I do not accept responsibility for any effects, adverse or otherwise, that this code may    
-- have on you, your computer, your data, and anything else that you can think of.              
--                                                                                            
-- Use it at your own risk.                                                               
--                                                                                                
--                                                                                               
-- A.C.Harper                                                           
---------------------------------------------------------------------------------------------------------
-- Changes since v0.5: 
-- DB_parsecomma altered to convert numerical text fields to numbers
-- Currency data improved
-- Bug in export table fixed
-- IDB_sflat added
-- Search routines altered to return table, record position and key of 'hit' record
-- Bug in Close Database fixed
-- Parsing commands altered to accept GUI field entry
-- Search routine altered to be more reliable and handle empty tables better
-- Return description and list description commands added for IDB databases
-- Changes since v0.6
-- database2.e modified to database2f.e 
-- Changes since v0.7
-- Code changed to reflect Euphoria Alpha Release 
-- Minor bug fixes made
-- Rename Table command added
---------------------------------------------------------------------------------------------------------
global constant A_VERB = 1, A_OBJ = 2, A_QUAL = 3, A_QUAL2 = 4 , A_QUAL3 = 5,
                  A_QUAL4 = 6 -- COMMAND constants
global constant C_MSG = 1,C_DIR = 2,C_DB = 3,C_TABLE = 4,C_SIZE = 5,C_POSN = 6,C_KEY = 7,
                  C_STATUS = 8,C_NAME = 9, C_LINKS = 10,C_OWNER = 11,C_SUB = 12,C_PRIOR = 13,
                  C_NEXT = 14,C_DATA = 15,C_TERM= 16, C_TABLIST = 17,C_DBLIST = 18,C_HITS = 19,
                  C_DESC = 20, C_CC = 21, C_BODY = 22  --CURRENT constants
global constant C_MAX = 22 -- Maximum entries for CURRENT, PREVIOUS, DISPLAY
global constant L_CT = 1, L_CK = 2, L_OT = 3, L_OK = 4, L_ST = 5, L_SK = 6, L_PT = 7, L_PK = 8,
                  L_NT = 9, L_NK = 10 -- Linker constants
global constant MAXWORDS = 6 -- Maximum number of words in COMMAND
global constant R_ADD = 1, R_MOD = 2, R_DEL = 3, R_FAIL = 4 -- DELTA constants
global constant S_KEY =1, S_STATUS = 2, S_NAME = 3, S_LINKS = 4, S_DATA = 5,
                  S_BODY =6 -- STACK constants
global constant F_DB = 1, F_TABLE = 2, F_KEY =3, F_STATUS = 4, F_NAME = 5, F_LINKS = 6, F_DATA = 7,
                  F_BODY = 8 -- FNAME constants                  
global integer GMODE
global integer STACKMAX
global integer FIXED
global integer DYNAMIC
global integer FIRSTFLAG
global atom IDBFLAG
global object COMMAND
global object NEXTCOMMAND
global object CURRENT
global object F_CURRENT
global object DESCRIPTION -- Persistent record description-
global object DISPLAY
global object EDBDIR 
global object KEY
global object KEYVAL
global object LINKDAT
global object LINKER
global object NOLINKS
global object OLDCOMMAND
global object OUTPUTFILE
global object ECHOFILE
global object BANKER
global object PREVIOUS
global object PROFDIR
global object RECDAT
global object RECNAM
global object RECSTAT
global object RECBODY
global object WHOLERECORD
global object ZERO
global sequence STACK
global sequence DELTA
sequence activeverbs
sequence nonrawverbs
sequence readonlydb
sequence rawobjects
sequence rawonlydb
include database3.e
--/*
include file.e
include get.e
include graphics.e
include image.e
include machine.e
include misc.e
include msgbox.e
include wildcard.e
--*/
--with profile

COMMAND = repeat("@null",MAXWORDS)
NEXTCOMMAND = {}
DISPLAY = {"Message      :","Directory    :","Database     :","Current Table:",
            "Table size   :","Position     :","Key          :","Status       :",
            "Record Name  :","Links        :","Owner        :","Subordinate  :",
            "Prior        :","Next         :","Data         :","Terminator   :",
            "Table list   :","DB List      :","Hits         :","Description  :","CC           :","Raw Data     :"}
activeverbs = {"add","copy","delete","modify","edit","link","unlink","rename"}   
nonrawverbs = {"link","unlink","commit"}  
rawobjects = {"all","alldb","name","control","database","table","current","first","last",
            "list","size","record","@null","file","edbdir","profdir","global","echofile",
            "outputfile","status","key","template","plus","minus","switch","data","on",
            "off","field","stack","stackmax","recdat","recnam","recstat","keyval","linkdat",
            "recbody","whole","part", "tabkey", "tabpos","tabnam","pause","description"}
readonlydb ={}
rawonlydb = {}
IDBFLAG = 1
FIRSTFLAG = 0
CURRENT = repeat(0,C_MAX)
PREVIOUS = CURRENT
F_CURRENT = CURRENT
LINKER = repeat("@null",10)
NOLINKS = repeat("@",8)
KEYVAL = "999999999"
RECNAM = "@null"
RECDAT = "@null"
RECSTAT = "U"
LINKDAT = NOLINKS
RECBODY = RECDAT
ZERO = CURRENT
GMODE = 261   -- (may be altered by calling program, sets display properties)
STACKMAX = 50
STACK = repeat({"@null","@null","@null","@null","@null","@null"}, STACKMAX)
DELTA = {0,0,0,0}
FIXED = YELLOW
DYNAMIC = GREEN
integer proc_process
atom echo
atom echonum
atom nofile
atom warning
atom datatype
atom readonly
atom report
atom pause
pause = 0                     -- if set to 1 will pause execution at proc_pause points
echo = 0
echonum = 0
nofile = 1                      -- display filenumber for diplay output, <screen> by default 
report = 1                     -- turns on-line display on or off
warning = 1                  -- warnings are on by default
datatype = 0                -- display of datatype (atom, integer, string) is off by default
EDBDIR = {}                -- calling program can use this global variable to manipulate -
                                      -- directory in which database can be created/opened
PROFDIR = {}             -- directory for script file can be altered from calling program
OUTPUTFILE = "output.txt"     -- Output file can be set from calling program, -
                                                    -- current directory is default
ECHOFILE = "echo.txt"            -- Output file to record commands and display,
                                                    -- current directory is default

-- GENERAL FORMAT PROCEDURES AND FUNCTIONS =============================================================
-- Text formatting routines follow (see manual for details):
--
-- IDB_flatprint v1.0 -       "pretty print" routine
--
-- IDB_ftext v1.0 -              formats text to a particular format 
--
-- IDB_fdate v1.0 -            formats EU4 date data into text representation 
--                
-- IDB_texttonum v1.0 -    converts character text sequences to numbers
--
-- IDB_sflatprint v1.0 -      returns a formatted sequence of characters from nested sequences
--
-- IDB_sflat v1.0 -              returns a sequence of characters from nested sequences with tilde separator
--======================================================================================================
global type IDB_string(sequence s)
-- verify that we have a valid Euphoria-representable string
-- (ASCII chars 32-255, 9, 10, 13)
object x
for i = 1 to length(s) do
   x = s[i]
   if integer(x) then
      if (x < 32 or x > 255) and  x != 9 and x != 10 and x != 13 then
         return 0
      end if
   else
      return 0
   end if   
end for
return 1
end type

global type IDB_number(sequence s)
-- verify that we have a valid Euphoria-representable number string
-- (ASCII chars 0 to 9, decimal point, and plus or minus in first position)
object x
for i = 1 to length(s) do
   x = s[i]
   if integer(x) then
      if (x < 48 or x > 57) and  x != '.' and s[1] != '+' and s[1] != '-' then
         return 0
      end if
   else
      return 0
   end if   
end for
return 1
end type


global function IDB_parsetext(sequence dbinput)
integer count 
integer start
integer count2
object dbc
object dbctemp
integer onoff
dbc = {}
onoff=1
if length(dbinput) = 0 then
      dbinput = "@null"
   end if
   for ploop = 1 to length(dbinput) do
      if equal(dbinput[ploop],34) then onoff = onoff*-1 end if
      if equal(dbinput[ploop],44) and onoff = 1 then dbinput[ploop] = 0 end if   
   end for
   count = 1
      count2 = 0
      start = 1
      for ploop = 1 to length(dbinput) do
         if equal(dbinput[ploop],44) then
            count2 = ploop - 1
            dbc[count] = dbinput[start..count2]
            start = ploop + 1
            count += 1
          end if
      end for
      count2 = length(dbinput)
      dbc[count] = dbinput[start..count2]
      
      for counter = 1 to length(dbc) do
         if equal(dbc[counter][1],34) then
            dbctemp = dbc[counter]
            for ploop = 1 to length(dbctemp) do
               if equal(dbctemp[ploop],0) then dbctemp[ploop] = 44 end if  
            end for   
            dbc[counter] = dbctemp[2..(length(dbctemp)-1)]
         end if
   end for   
return dbc
end function


global function IDB_parsecomma(sequence dbinput)
   integer count 
   integer start
   object dbc
   object dummy
   dbc = repeat("",length(dbinput))
   count = 1
   start = 1
   for ploop = 1 to length(dbinput) do
      if equal(dbinput[ploop],44) then
         count = count +1
         start = ploop +1
      else
         dbc[count] = dbinput[start..ploop]
      end if
   end for
   for ploop = 1 to count  do
      if IDB_number(dbc[ploop]) then
            dummy = value(dbc[ploop])
            if dummy[1] = GET_SUCCESS then dbc[ploop]= dummy[2] end if
      end if 
   end for  
   dbc = dbc[1..count]
return dbc
end function

global function IDB_texttonum(object x)
object para1
object para2
para1 = x & "tail"
para2 = value(para1)
para1 = para2[2]
return para1
end function


global function IDB_parsetilde(sequence dbinput)
-- divide tilde separated values into separate sequences/numbers
integer count 
integer start
object dbc
object dummy
dbc = repeat({},length(dbinput))
count = 1
start = 1
for ploop = 1 to length(dbinput) do
   if equal(dbinput[ploop],126) then
      count = count +1
      start = ploop +1
   else
      dbc[count] = dbinput[start..ploop]
   end if
end for
-- store numeric fields as numbers
for ploop = 1 to count  do
   if IDB_number(dbc[ploop]) then
         dummy = value(dbc[ploop])
         if dummy[1] = GET_SUCCESS then dbc[ploop]= dummy[2] end if
   end if 
end for  
dbc = dbc[1..count]
return dbc
end function

global function IDB_parsertn(sequence dbinput)
-- divide linefeed/return separated values into separate sequences/numbers
integer count
object dbc
object dummy
dbc = repeat({},length(dbinput))
count = 1
for ploop = 1 to length(dbinput) do
   if dbinput[ploop] = 13  then
         count+=1
   elsif  dbinput[ploop] = 10 then
   
   else
       dbc[count] = append(dbc[count], dbinput[ploop])
   end if   
end for
dbc = dbc[1..count]
-- store numeric fields as numbers
for ploop = 1 to count  do
   if IDB_number(dbc[ploop]) then
         dummy = value(dbc[ploop])
         if dummy[1] = GET_SUCCESS then dbc[ploop]= dummy[2] end if
   end if   
end for  
dummy ={}
for count2 = 1 to count do
dummy = append(dummy,dbc[count2])
end for
return dbc
end function


global procedure IDB_flatprint(integer fn, object x, sequence form)
-- Procedure to unpick nested sequences into 'flat' sequence for output
-- to screen or file. Recursive calls made to procedure. 
if length(form)=0 then
   form={"%d","%f","%s",34,',','\n'} --default format 
end if
if equal(x,{}) then
   -- drop through procedure
elsif integer(x) then
   printf(fn,form[1],x)
   puts(fn,form[6])
elsif atom(x) then
   printf(fn,form[2],x)
   puts(fn,form[6])
elsif IDB_string(x) then
   printf(fn,form[3],form[4])
   for count = 1 to length(x) do
       printf(fn,form[3],x[count])
   end for
   printf(fn,form[3],form[4])
   puts(fn,form[6])        
elsif sequence(x) then 
   for count = 1 to length(x) do
      if count < length(x)  then
         IDB_flatprint(fn,x[count],{"%d","%f","%s",0,form[5],form[5]})
      else
         IDB_flatprint(fn,x[count],{"%d","%f","%s",0,126,form[6]})
      end if
   end for  
else
   puts(fn,"Error in IDB_flatprint")
   puts(fn,form[6])
end if   
end procedure

global function IDB_sflatprint(object x, object form)
-- As IDB_flatprint, but returns value to memory, not device.
sequence y
sequence z
y = {}
z = {}
if integer(x) then
   y = y & sprintf("%d",x) & form
   return y
elsif atom(x) then
   y = y & sprintf("%d",x) & form
   return y
elsif IDB_string(x) then
   for count = 1 to length(x) do
      y = y & sprintf("%s",x[count])
   end for
   y = y & form
   return y
elsif sequence(x) then 
   for count = 1 to length(x) do
      y = y & IDB_sflatprint(x[count],form)
   end for  
   return y
end if  
end function

global function IDB_sflatrtn(object x)
-- As IDB_flatprint, but returns value to memory, not device, with sequences separated by line feed/returns.
sequence y
y = {}
if integer(x) then
   y = y & sprintf("%d",x) & "\r\n"
elsif atom(x) then
   y = y & sprintf("%d",x) & "\r\n"
elsif IDB_string(x) then
   for count = 1 to length(x) do
      y = y & sprintf("%s",x[count])
   end for
   y = y & "\r\n"
elsif sequence(x) then 
   for count = 1 to length(x) do
      y = y & IDB_sflatrtn(x[count])
      y = y &  "\r\n"
   end for  
end if  
if equal(y[length(y)],'\n') then
   y = y[1..length(y)-2]
end if
return y
end function

global function IDB_sflat(object x)
-- As IDB_flatprint, but returns value to memory, not device.
sequence y
y = {}
if integer(x) then
   y = y & sprintf("%d",x) & "~"
elsif atom(x) then
   y = y & sprintf("%d",x) & "~"
elsif IDB_string(x) then
   for count = 1 to length(x) do
      y = y & sprintf("%s",x[count])
   end for
   y = y &   "~"
elsif sequence(x) then 
   for count = 1 to length(x) do
      y = y & IDB_sflat(x[count])
      y = y &   "~"
   end for  
end if  
if equal(y[length(y)],'~') then
   y = y[1..length(y)-1]
end if
return y
end function

global function IDB_dtype(object x)
-- Identifies datatype
sequence y
if integer(x) then
   y = "(I) "
   return y
elsif atom(x) then
   y = "(A) "
   return y
elsif IDB_string(x) then
   y = "(S) "
   return y
elsif sequence(x) then 
   for count = 1 to length(x) do
      y = IDB_dtype(x[count])
   end for  
   return y
end if  
end function

global function IDB_debugprint(object x)
sequence y
sequence z
y = {}
z = {}
if integer(x) then
   y = y & sprintf("%d",x) & "I"
   return y
elsif atom(x) then
   y = y & sprintf("%d",x) & "A"
   return y
elsif IDB_string(x) then
   for count = 1 to length(x) do
      y = y & sprintf("%s",x[count])
   end for
   y = y & "S"
   return y
elsif sequence(x) then 
   for count = 1 to length(x) do
      y = y & IDB_debugprint(x[count])
   end for  
   return y
end if  
end function

global function IDB_ftext(sequence x, sequence form)
-- General routine for formatting text. See IDB.doc.
integer lenx, lenf, start, width, marker, diff, pow
object tempf
if length(x) < 1 then x = " " end if
lenx = length(x)
lenf = length(form)
if lenf > 1 then
   tempf = form[2..lenf]
   tempf = value(tempf)
   width = tempf[2]
else
   width = 0
end if
if match("U",form[1..1])>0 then --upper case
   x = IDB_sflatprint(x,{})
   lenx = length(x)
   x=upper(x)
   if width >lenx then
      x = x & repeat(32,width)
      x = x[1..width]
   end if
elsif match("L",form[1..1])>0 then --lower case
   x = IDB_sflatprint(x,{})
   lenx = length(x)
   x=lower(x)
   if width >lenx then
      x = x & repeat(32,width)
      x = x[1..width]
   end if
elsif match("T",form[1..1])>0 then --not altered, although padded or truncated
  x = IDB_sflatprint(x,{})
   lenx = length(x)
   if width >lenx then
      x = x & repeat(32,width)
      x = x[1..width]
   end if
elsif match("V",form[1..1])>0 then --variable length item, length and data returned
   x = {sprintf("%d",lenx),x}
elsif match("I",form[1..1])>0 then  -- first letter of each word upper case
   x = IDB_sflatprint(x,{})
   lenx = length(x)
   x[1]=upper(x[1])
   for count=2 to lenx do
      if compare(' ',x[count-1])=0 then
         x[count]=upper(x[count])
      end if
   end for
   if width >lenx then
      x = x & repeat(32,width)
      x = x[1..width]
   end if
elsif match("S",form[1..1])>0 then  -- first letter of field upper case
   x = IDB_sflatprint(x,{})
   lenx = length(x)  
   x[1]=upper(x[1])
   if width >lenx then
      x = x & repeat(32,width)
      x = x[1..width]
   end if
elsif match("C",form[1..1])>0 then  -- Centre text in form[2] characters
   diff = width - length(x)
   diff = floor(diff/2)
   x = {repeat(" ",diff),x}
elsif match("H",form[1..1])>0 then  -- hides text from prying eyes
   x=reverse(x)
   for count = 1 to lenx do
      if remainder(count,2) then pow = 2 else pow = 1 end if
      x[count]+=width+floor(power(count,pow+.1))-floor(power(lenx-count,pow+.2))
   end for   
elsif match("h",form[1..1])>0 then  -- reveals hidden text
   for count = 1 to lenx do
      if remainder(count,2) then pow = 2 else pow = 1 end if
      x[count]-=width+floor(power(count,pow+.1))-floor(power(lenx-count,pow+.2))
   end for 
   x=reverse(x)
elsif match("F",form[1..1])>0 then  -- formats text over several lines  
   x = IDB_sflatprint(x,{})
   lenx = length(x)
   width -= 1 -- looks better on page
   if width >lenx then
      x = x & repeat(32,width)
      x = x[1..width]
   else 
      start = 1
      while start + width < lenx do
         marker = 1
         for count = start to start + width +1 do
            if equal(x[count],32) then marker = count end if
         end for   
         if marker > 1 then
            x = x[1..marker-1]&'\n'&x[marker+1..lenx]
            start = marker 
         else
            start = start + width
            x = x[1..start-2]&"-\n-"&x[start-1..lenx]
         end if  
         lenx = length(x)
      end while
   end if
else 
   CURRENT[C_CC]=8
   CURRENT[C_MSG] = {"Error in IDB_ftext()"}
   x=""
end if
return x
end function

global function IDB_fdate(sequence x, sequence form)
sequence day, month, alphamonth, year, hour, minute, second
atom colon
alphamonth={"January","Febuary","March","April","May","June","July","August","September","October","November","December"}
if length(form) = 0 then
   form="hh:mm:ss dd/mm/yyyy"
end if
day=sprintf("%02d",x[3])
month=sprintf("%02d",x[2])
year=sprintf("%d",1900+x[1])
hour=sprintf("%02d",x[4])
minute=sprintf("%02d",x[5])
second=sprintf("%02d",x[6])
if match("dd/mm/yyyy",form) >0 then
   x= day & "/" & month & "/" & year
elsif match("mm/dd/yyyy",form) > 0 then
   x= month & "/" & day & "/" & year
elsif match("dd mmm yyyy",form) > 0 then
   x= day & " " & alphamonth[x[2]][1..3] & " " & year
elsif match("mmm dd yyyy",form) > 0 then
   x= alphamonth[x[2]][1..3] & " " & day & " " & year   
elsif match("dd MMM yyyy",form) >0 then
   x=  day & " " & alphamonth[x[2]] & " " & year
elsif match("MMM dd yyyy",form) >0 then
   x= alphamonth[x[2]] & " " & day & " " & year   
else
   x="Failed date format"
end if
colon=find(':',form)
if colon = 0 then
   -- no time element returned
elsif colon = 3 then
   -- no date element returned
   if match("hh:mm:ss",form) > 0 then
      x= hour & ":" & minute & ":" & second & " " & x
   elsif match("hh:mm",form) > 0 and match(":ss",form) = 0 then
      x= hour & ":" & minute & " " & x
   end if
elsif colon != 3 then
   -- time element follows date element
   if match("hh:mm:ss",form) > 0 then
      x= x & " " &  hour & ":" & minute & ":" & second
   elsif match("hh:mm",form) > 0 and match(":ss",form) = 0 then
      x= x & " " &  hour & ":" & minute
   end if
else
   CURRENT[C_CC]=8
   CURRENT[C_MSG] = {"Error in IDB_fdate()"}
   x=""
end if
return x
end function


-- GENERAL CURRENCY SETTING/CHECKING AND DATABASE SUB-ROUTINES ============================

global function IDB_zero()
for count = 1 to C_MAX do
   ZERO[count] = {}
end for
ZERO[C_LINKS] = NOLINKS
ZERO[C_CC] = 0
return ZERO
end function

function func_edit(object field, integer width)
object key, dummy
sequence xypos, slot, fore, aft, oldfield
integer offset, lenf, lend
key = 0
if not atom(field) and not integer(field) and not IDB_number(field) then
   if width = 0 then width = 79 end if
   if equal(field,{}) then field = " " end if
   if sequence(field[1]) then field = field[1] end if
   oldfield = field
   lend = width - 10
   if lend < 5 then
      CURRENT[C_MSG] = {"Display is too narrow"}
      CURRENT[C_CC] = 8
      return field
   end if   
   fore = "    >"
   aft = "<   "
   lenf = length(field)
   if lenf < lend then
      field = "     "&field&repeat(' ',lend)
   else
      field = "     "&field
   end if
   slot = field[1..lend]
   xypos = get_position()
   offset = 0
   while key != 13 do
      position(xypos[1],xypos[2])
      --IDB_flatprint(1,{fore&slot&aft},{"%d","%f","%s",0,0,0})
      text_color(BRIGHT_GREEN)
      IDB_flatprint(1,fore,{"%d","%f","%s",0,0,0})
      text_color(GREEN)
      IDB_flatprint(1,slot,{"%d","%f","%s",0,0,0})
      text_color(BRIGHT_GREEN)   
      IDB_flatprint(1,aft,{"%d","%f","%s",0,0,0})
      text_color(GREEN)
      position(xypos[1],xypos[2]+10)
      key = wait_key()
      if key = 333 then       -- move right
         offset += 1
         field = field&' '
      elsif key = 328 then    -- page up
         offset -=lend
         field = repeat(' ',lend)&field
      elsif key = 336 then    -- page down
         offset += lend
         field = field&repeat(' ',lend)
      elsif key = 331 then    -- move left
         offset -=1
         field = ' '&field 
      elsif key = 339 then    -- delete
         field = field[1..offset+5]&field[offset + 7..length(field)]&' '
      elsif key = 13 then     -- return
         exit   
      elsif key = 27 then     -- escape with no changes
         return oldfield
      else                    -- enter text
         field = field[1..offset+5]&key&field[offset + 6..length(field)]
         offset +=1
      end if
      slot = field[1+offset..lend+offset]
   end while
   if not equal(slot,repeat(32,length(slot))) then 
      while 1 do
         if equal(field[1],' ') then
            field = field[2..length(field)]
         else
            exit
         end if   
      end while
      while 1 do
         if equal(field[length(field)],' ') then
            field = field[1..length(field)-1]
         else
            exit
         end if   
      end while
   else
      field = {}
   end if
   if IDB_number(field) then
      dummy = value(field)
      if dummy[1] = GET_SUCCESS then field = dummy[2] end if
   end if   
   puts(1,'\n')
else
   -- edit a number, replace with string if required
   text_color(BRIGHT_GREEN)
   IDB_flatprint(1,"Previous value  > ",{"%d","%f","%s",0,0,0})
   text_color(DYNAMIC)
   IDB_flatprint(1,sprint(field),{"%d","%f","%s",0,0,'\n'})
   text_color(BRIGHT_GREEN)
   IDB_flatprint(1,"Enter new value > ",{"%d","%f","%s",0,0,0})
   text_color(DYNAMIC)
   field = (prompt_string(""))
   if IDB_number(field) then
         dummy = value(field)
         if dummy[1] = GET_SUCCESS then field = dummy[2] end if
   end if   
end if
return field
end function


procedure proc_step(sequence record, integer field)
object key
key = 0
text_color(BRIGHT_GREEN)
IDB_flatprint(1,{"Field ",field,": "},{"%d","%f","%s",0,0,0})
text_color(GREEN)
IDB_flatprint(1,record[field],{"%d","%f","%s",0,0,'\n'})
while key != 27 do
   key = wait_key()
   if key = 328 then    -- up
      field = field - 1
      if field > length(record) then field = 1 end if
      if field < 1 then field = length(record) end if
      proc_step(record, field)
      key = 27
   elsif key = 336 then    -- down
      field = field + 1  
      if field > length(record) then field = 1 end if
      if field < 1 then field = length(record) end if
      proc_step(record, field)
      key = 27
   elsif key = 101 or key = 69 then    -- edit field
   NEXTCOMMAND = {"edit field "&sprintf("%d",field)}
      key = 27
   end if
end while
end procedure

procedure proc_reset() 
sequence recbody
if equal(PREVIOUS[C_POSN],{})
then
   CURRENT = IDB_zero()
else  
   if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
   CURRENT[C_MSG] = db_select(PREVIOUS[C_DB])
   CURRENT[C_MSG] = db_select_table(PREVIOUS[C_TABLE])
   CURRENT[C_SIZE] = db_table_size()
   CURRENT[C_DB] = PREVIOUS[C_DB]
   CURRENT[C_TABLE] = PREVIOUS[C_TABLE]
   CURRENT[C_KEY] = PREVIOUS[C_KEY]
   recbody = db_record_data(PREVIOUS[C_POSN])
   CURRENT[C_POSN] = PREVIOUS[C_POSN]
   if find(db_return_currentdb(),rawonlydb) then
      CURRENT[C_STATUS]= {}
      CURRENT[C_NAME]  = {}
      CURRENT[C_LINKS] = {}
      CURRENT[C_DATA]  = {}
      CURRENT[C_TERM]  = {}
      CURRENT[C_BODY]  = recbody
   else 
      CURRENT[C_STATUS]= recbody[1]
      CURRENT[C_NAME]  = recbody[2]
      CURRENT[C_LINKS] = recbody[3]
      CURRENT[C_DATA]  = recbody[4]
      CURRENT[C_TERM]  = recbody[5]
      CURRENT[C_BODY]  = recbody
   end if
   CURRENT[C_CC] = 0
   WHOLERECORD = {CURRENT[C_DB],CURRENT[C_TABLE],CURRENT[C_KEY],recbody}
end if   
end procedure

function func_readonly()
if find(COMMAND[A_VERB],activeverbs) and find(db_return_currentdb(),readonlydb) then
   proc_reset()
   CURRENT[C_MSG] = {"Activity "&COMMAND[A_VERB]&" not allowed for read only database"}
   CURRENT[C_CC] = 8
   return 1
else
   return 0 
end if   
end function

function func_raw_db()
-- see if opened database is an EDS (raw) database or IDB
if find(db_return_currentdb(),rawonlydb) then return 1 else return 0 end if
end function

function func_rawonly_actions()
if find(db_return_currentdb(),rawonlydb) and find(COMMAND[A_VERB],nonrawverbs) then
   proc_reset()
   CURRENT[C_MSG] = {"Activity "&COMMAND[A_VERB]&" not allowed on EDS database"}
   CURRENT[C_CC] = 8
   return 1
elsif find(db_return_currentdb(),rawonlydb) and not find(COMMAND[A_OBJ],rawobjects) then
   proc_reset()
   CURRENT[C_MSG] = {"Object "&COMMAND[A_OBJ]&" not available in EDS database"}
   CURRENT[C_CC] = 8
   return 1 
else   
   return 0 
end if   
end function

function func_current(object position)
sequence recbody
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_DB] = db_return_currentdb()
if equal(CURRENT[C_DB],"No database current") then
   CURRENT[C_MSG] = {"No database current"}
   CURRENT[C_DB] = "No database current"
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_TABLE] = db_return_currenttable()
if equal(CURRENT[C_TABLE],"No tables current") then
   CURRENT[C_MSG] = {"No tables current"}
   CURRENT[C_DB] = db_return_currentdb()
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_SIZE] = db_table_size() 
if CURRENT[C_SIZE] = 0 then
   CURRENT[C_MSG] = {"Table is empty"}
   CURRENT[C_CC] = 8
   return CURRENT
end if
if not integer(position) or position < 1 then
   CURRENT[C_MSG] = {"No record current"}
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_KEY] = db_record_key(position)
recbody = db_record_data(position)
CURRENT[C_POSN] = position
if func_raw_db() then
   CURRENT[C_STATUS]= {}
   CURRENT[C_NAME]  = {}
   CURRENT[C_LINKS] = {}
   CURRENT[C_DATA]  = {}
   CURRENT[C_TERM]  = {}
   CURRENT[C_BODY] = recbody
else 
   CURRENT[C_STATUS]= recbody[1]
   CURRENT[C_NAME]  = recbody[2]
   CURRENT[C_LINKS] = recbody[3]
   CURRENT[C_DATA]  = recbody[4]
   CURRENT[C_TERM]  = recbody[5]
   CURRENT[C_BODY] = recbody
   -- note record body returned even for an IDB database
end if
CURRENT[C_CC] = 0
WHOLERECORD = {CURRENT[C_DB],CURRENT[C_TABLE],CURRENT[C_KEY],recbody} 
return CURRENT
end function

procedure proc_links()
CURRENT[C_OWNER] = CURRENT[C_LINKS][1..2]
CURRENT[C_SUB] = CURRENT[C_LINKS][3..4]
CURRENT[C_PRIOR] = CURRENT[C_LINKS][5..6]
CURRENT[C_NEXT] = CURRENT[C_LINKS][7..8]
end procedure

function func_dbtabsize()
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_DB] = db_return_currentdb()
if equal(CURRENT[C_DB],"No database current") then
   CURRENT[C_MSG] = {"No database current"}
   CURRENT[C_DB] = "No database current"
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_TABLE] = db_return_currenttable()
if equal(CURRENT[C_TABLE],"No tables current") then
   CURRENT[C_MSG] = {"No tables current"}
   CURRENT[C_DB] = db_return_currentdb()
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_SIZE] = db_table_size()
if CURRENT[C_SIZE] = 0 then
   CURRENT[C_MSG] = {"Table is empty"}
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_CC] = 0
return CURRENT
end function

function func_dbtab()
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_DB] = db_return_currentdb()
if equal(CURRENT[C_DB],"No database current") then
   CURRENT[C_MSG] = {"No database current"}
   CURRENT[C_DB] = "No database current"
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_TABLE] = db_return_currenttable()
if equal(CURRENT[C_TABLE],"No tables current") then
   CURRENT[C_MSG] = {"No tables current"}
   CURRENT[C_DB] = db_return_currentdb()
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_CC] = 0
return CURRENT
end function

function func_tabkey(sequence CURRENT)
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_MSG] = db_select_table(COMMAND[A_QUAL])
if CURRENT[C_MSG] != 0 then
   CURRENT[C_MSG] = {"Table "&COMMAND[A_QUAL]&" does not exist"}
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_POSN] = db_find_key(COMMAND[A_QUAL2])
if CURRENT[C_POSN] < 1 then
   CURRENT[C_MSG] = {"Key "&COMMAND[A_QUAL2]&" not found in table "&COMMAND[A_QUAL]}
   CURRENT[C_CC] = 8
   return CURRENT
end if
CURRENT[C_CC] = 0
return CURRENT
end function

function func_tabkeycur(sequence CURRENT)
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLE])
if CURRENT[C_MSG] < 0 then
   CURRENT[C_MSG] = {"Can't find table ",CURRENT[C_TABLE]}
   CURRENT[C_CC]= 8
   return CURRENT
end if   
CURRENT[C_POSN] = db_find_key(CURRENT[C_KEY])
if CURRENT[C_POSN] < 1 then
   CURRENT[C_MSG] = {"Can't find record key "&CURRENT[C_KEY]&" in table ",CURRENT[C_TABLE]}
   CURRENT[C_CC]= 8
   return CURRENT
end if
CURRENT[C_CC] = 0
return CURRENT
end function

procedure proc_discard_data()
if func_raw_db() then
   CURRENT[C_STATUS]= {}
   CURRENT[C_NAME]  = {}
   CURRENT[C_LINKS] = {}
   CURRENT[C_DATA]  = {}
   CURRENT[C_BODY]  = {}
else 
   CURRENT[C_DATA]  = {}
   CURRENT[C_TERM]  = {}
   CURRENT[C_BODY]  = {}
end if
end procedure

procedure proc_pause()
text_color(FIXED)
puts(1,'\n')
IDB_flatprint(1,"Press any key to continue, <escape> to abort",{"%d","%f","%s",0,'~','\n'})
puts(1,'\n')
text_color(DYNAMIC)
KEY = wait_key()
if KEY = 27 then CURRENT[C_CC] = 16 end if
end procedure

function func_warning(sequence message)
object answer
if platform() = WIN32 then
   answer = message_box(sprintf("%s",message),"WARNING",MB_YESNO)
   if answer = 6 then CURRENT[C_CC] = 0 else CURRENT[C_CC] = 8 end if
else
   text_color(BRIGHT_RED)
   puts(1,'\n')
   IDB_flatprint(1,message,{})
   IDB_flatprint(1,"Press <return> to continue, any other key to abort",{"%d","%f","%s",0,'~','\n'})
   puts(1,'\n')
   text_color(GREEN)
   KEY = wait_key()
   if KEY = 13 then CURRENT[C_CC] = 0 else CURRENT[C_CC] = 8 end if
end if
return CURRENT[C_CC]
end function

function func_delta()
sequence dummy
dummy = " Add "&sprintf("%d",DELTA[R_ADD])&"; Mod "&
      sprintf("%d",DELTA[R_MOD])&"; Del "&sprintf("%d",DELTA[R_DEL])&"; Fail "&
      sprintf("%d",DELTA[R_FAIL])
return dummy
end function


-- START OF MAIN VERB ACTION ROUTINE==========================================================================================================
function db_action(sequence COMMAND)
object filenum,dummy,dummy2,dblist,tablist,recbody,oldrecbody,previoustable,previousposn,
   firstdb,firsttable,seconddb,secondtable,copykey,copyrecord,tablesize,concode
integer recposn,stackpos,editflag
editflag = 0
recbody = {"@null","@null","@null","@null","@null"}
if echo then IDB_flatprint(echonum,{"->  Command      : "}&{COMMAND},{"%d","%f","%s",0,32,'\n'}) end if
if func_readonly() then return CURRENT end if
if func_rawonly_actions() then return CURRENT end if
-- ADD ========================================================================================================== 
if equal(COMMAND[A_VERB],"add") then
   DELTA = {0,0,0,0}
   CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"record") then
      CURRENT = func_dbtab()
      if CURRENT[C_CC] > 0 then return CURRENT end if
      if equal(COMMAND[A_QUAL],"KEYVAL") then COMMAND[A_QUAL] = KEYVAL end if
      if equal(COMMAND[A_QUAL2],"RECNAM") then COMMAND[A_QUAL2] = RECNAM end if
      if equal(COMMAND[A_QUAL2],"RECBODY") then COMMAND[A_QUAL2] = RECBODY end if
      if equal(COMMAND[A_QUAL3],"RECDAT") then COMMAND[A_QUAL3] = RECDAT end if
       
      if not func_raw_db() then
         if equal(COMMAND[A_QUAL],"@null") and equal(COMMAND[A_QUAL2],"@null") and 
               equal(COMMAND[A_QUAL3],"@null") then
            COMMAND[A_QUAL] = KEYVAL
            COMMAND[A_QUAL2] = RECNAM
            COMMAND[A_QUAL3] = RECDAT
         end if   
         if not equal(COMMAND[A_QUAL3],RECDAT) then
             COMMAND[A_QUAL3] = IDB_parsetilde(COMMAND[A_QUAL3])
         end if
         if not equal(COMMAND[A_QUAL2],RECNAM) then
             COMMAND[A_QUAL2] = IDB_parsetilde(COMMAND[A_QUAL2])
         end if
      else
         if equal(COMMAND[A_QUAL],"@null") and equal(COMMAND[A_QUAL2],"@null") and 
               equal(COMMAND[A_QUAL3],"@null") then
            COMMAND[A_QUAL] = KEYVAL
            COMMAND[A_QUAL2] = RECBODY
         end if
         if not equal(COMMAND[A_QUAL2],RECBODY) then
            COMMAND[A_QUAL2] = IDB_parsetilde(COMMAND[A_QUAL2])
         end if
      end if
      if not func_raw_db() then  
         if not equal(COMMAND[A_QUAL3],"@null") then
            COMMAND[A_QUAL2] = {"U",COMMAND[A_QUAL2],NOLINKS,COMMAND[A_QUAL3],"@@"}
         else
            CURRENT[C_MSG] = {"Incorrect syntax of ADD command for IDB database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      else
         if not equal(COMMAND[A_QUAL3],"@null") then
            CURRENT[C_MSG] = {"Incorrect syntax of ADD command for EDS database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      end if
      CURRENT[C_MSG] = db_insert(COMMAND[A_QUAL],COMMAND[A_QUAL2])
      if CURRENT[C_MSG] = 0 then
         DELTA[R_ADD] +=1
         CURRENT[C_MSG] = {"Record with key "}&{COMMAND[A_QUAL]}&{" added. "&func_delta()}
         if equal(CURRENT[C_TABLE],"Description") then 
         end if
      else
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Unable to add record with key "}&{COMMAND[A_QUAL]}&{". "&func_delta()}
         CURRENT[C_CC]= 8
         return CURRENT
      end if
      CURRENT[C_POSN] = db_find_key(COMMAND[A_QUAL])
      if CURRENT[C_POSN] < 1 then
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Key "&COMMAND[A_QUAL]&" not found in table "&CURRENT[C_TABLE]&func_delta()}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(CURRENT[C_POSN])
      end if
   elsif equal(COMMAND[A_OBJ],"control") then
      DELTA = {0,0,0,0}
      dummy = db_return_currentdb()
      concode = db_create_table("Control")
      if concode != DB_OK then
         CURRENT[C_MSG] = {"Unable to create Control table"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      RECDAT = {IDB_fdate(date(),{})}
      concode = db_insert("created",{"U","Control",NOLINKS,RECDAT,"@@"})
      if concode != DB_OK then
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Control record not added. "&func_delta()}
         return CURRENT 
      else
         DELTA[R_ADD] += 1
      end if
      concode = db_insert("IDB version",{"U","Control",NOLINKS,{"IDB v0.5"},"@@"})
      if concode != DB_OK then
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Control record not added. "&func_delta()}
         return CURRENT 
      else
         DELTA[R_ADD] += 1
      end if
      concode = db_insert("Euphoria version",{"U","Control",NOLINKS,{"Euphoria 2.3"},"@@"})
      if concode != DB_OK then
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Control record not added. "&func_delta()}
         return CURRENT 
      else
         DELTA[R_ADD] += 1
      end if
      RECDAT = {"Description field ready for update"}
      concode = db_insert("description",{"U","Control",NOLINKS,RECDAT,"@@"})
      if concode != DB_OK then
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Control record not added. "&func_delta()}
         return CURRENT 
      else
         DELTA[R_ADD] += 1
      end if
      RECDAT = {"Field ready for commit statistics "&IDB_fdate(date(),{})}
      concode = db_insert("commit",{"U","Control",NOLINKS,RECDAT,"@@"})
      if concode != DB_OK then
         DELTA[R_FAIL] +=1
         CURRENT[C_MSG] = {"Control record not added. "&func_delta()}
         return CURRENT 
      else
         DELTA[R_ADD] += 1
      end if
   CURRENT = db_action({"close","database"})
   CURRENT = db_action({"open","database",dummy,"none"})
   CURRENT = db_action({"current","table","Control"})
   CURRENT = func_current(1)
   CURRENT[C_MSG] = {"Table Control created. "&func_delta()}
   end if

-- CLOSE ==========================================================================================================  
elsif equal(COMMAND[A_VERB],"close") then
   if equal(COMMAND[A_OBJ],"database") then
      if equal(db_return_alldb(),"No databases loaded") then
         CURRENT[C_CC]= 4
         CURRENT[C_MSG] ={"No database open"}
      else
         dummy = find(db_return_currentdb(),readonlydb)
         if dummy > 0 then readonlydb[dummy] = {}  end if
         dummy = find(db_return_currentdb(),rawonlydb)
         if dummy > 0 then rawonlydb[dummy] = {}  end if
         db_close()
         CURRENT[C_CC] = 0
         CURRENT[C_MSG]  = {"Database closed"}
         --==== select first open database and first table by default
         if not equal(db_return_alldb(),"No databases loaded") then
            dblist = IDB_parsecomma(db_return_alldb())
            dummy = db_select(dblist[1])
            tablist = db_table_list()
            dummy = db_select_table(tablist[1])
         end if
      end if
   elsif equal(COMMAND[A_OBJ],"file") then
      close(COMMAND[A_QUAL])
      CURRENT[C_CC] = 0
      CURRENT[C_MSG] ={"File closed"}
   end if
  
-- COMMIT =========================================================================================================
elsif equal(COMMAND[A_VERB],"commit") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"database") then
      WHOLERECORD = {db_return_currentdb()}  
      if equal(WHOLERECORD,{"No database open"}) then
         CURRENT[C_MSG] = "No database open"
         CURRENT[C_DB] = "No database open"
         CURRENT[C_CC] = 8
         return CURRENT
      end if      
      CURRENT[C_TABLIST] = db_table_list()
      for count = 1 to length(CURRENT[C_TABLIST]) do
         CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLIST][count])
         WHOLERECORD = WHOLERECORD&{db_return_currenttable(),sprintf("%d",(db_table_size()))}
      end for
      CURRENT[C_MSG] = db_select_table("Control")
      CURRENT[C_POSN] = db_find_key("commit")
      recbody = db_record_data(CURRENT[C_POSN])
      recbody[4] = WHOLERECORD&{IDB_fdate(date(),{})}
      db_replace_data(CURRENT[C_POSN],recbody)
      if CURRENT[C_CC] > 0 then return CURRENT end if
      dummy = open("NUL","w")
      close(dummy)
      for count = 1 to dummy - 1 do flush(count) end for
      proc_reset()
      CURRENT[C_MSG] = {"Commit issued for database "&WHOLERECORD[1]}
   end if
   
-- COMPRESS ========================================================================================================   
elsif equal(COMMAND[A_VERB],"compress") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"database") then
      CURRENT[C_DB] = db_return_currentdb()
      if not equal(CURRENT[C_DB],"No database open") then   
        dummy = db_compress()
         if dummy != DB_OK then 
            CURRENT[C_MSG] = {"Database compression failed"}
            CURRENT[C_CC] = 4
         else
            CURRENT[C_MSG] = {"Database compression worked"}
            CURRENT[C_CC] = 0
         end if   
      end if   
   end if
   
-- CONFIGURE =====================================================================================================   
elsif equal(COMMAND[A_VERB],"configure") then
   if equal(COMMAND[A_OBJ],"switch") then
      if equal(COMMAND[A_QUAL],"report") then 
         if equal(COMMAND[A_QUAL2],"@null") then
            report = remainder(report+1,2)
         else   
            report = IDB_texttonum(COMMAND[A_QUAL2])
         end if
         if report = 0 then
            nofile = open("NUL","w")
         else
            close(nofile)
            nofile = 1
         end if   
         CURRENT[C_MSG] = {"Switch "&COMMAND[A_QUAL]&" value : "&sprintf("%d",report)}
      elsif equal(COMMAND[A_QUAL],"warning") then 
         if equal(COMMAND[A_QUAL2],"@null") then
            warning = remainder(warning+1,2)
         else  
            warning = IDB_texttonum(COMMAND[A_QUAL2])
         end if
         CURRENT[C_MSG] = {"Switch "&COMMAND[A_QUAL]&" value : "&sprintf("%d",warning)}
         CURRENT[C_CC] = 0 
      elsif equal(COMMAND[A_QUAL],"datatype") then 
         if equal(COMMAND[A_QUAL2],"@null") then
            datatype = remainder(datatype+1,2)
         else  
            datatype = IDB_texttonum(COMMAND[A_QUAL2])
         end if
         CURRENT[C_MSG] = {"Switch "&COMMAND[A_QUAL]&" value : "&sprintf("%d",datatype)}
         CURRENT[C_CC] = 0 
      elsif equal(COMMAND[A_QUAL],"pause") then 
         if equal(COMMAND[A_QUAL2],"@null") then
            pause = remainder(pause+1,2)
         else  
            pause = IDB_texttonum(COMMAND[A_QUAL2])
         end if
         CURRENT[C_MSG] = {"Switch "&COMMAND[A_QUAL]&" value : "&sprintf("%d",pause)}
         CURRENT[C_CC] = 0    
      elsif equal(COMMAND[A_QUAL],"echo") then 
         if equal(COMMAND[A_QUAL2],"@null") then
            echo = remainder(echo+1,2)
         else  
            echo = IDB_texttonum(COMMAND[A_QUAL2])
         end if
         if echo then
            echonum = open(ECHOFILE,"w")
            CURRENT[C_MSG] = {"Echo file "&ECHOFILE&" opened"}
         else
            close(echonum)
            CURRENT[C_MSG] = {"Echo file "&ECHOFILE&" closed"}
         end if   
            CURRENT[C_CC] = 0     
      elsif equal(COMMAND[A_QUAL],"readonly") then
         if func_raw_db() then 
            CURRENT[C_CC] = 8
            CURRENT[C_MSG] = {"Cannot make EDS database readonly"}
            return CURRENT
         end if
         if equal(COMMAND[A_QUAL2],"on") then
            -- insert read only record
            dummy = db_select_table("Control")
            if dummy != DB_OK then
               proc_reset()
               CURRENT[C_CC] = 8
               CURRENT[C_MSG] = {"Cannot find Control table to make database readonly"}
               return CURRENT
            end if   
            dummy = db_insert("readonly",{"U","control",NOLINKS,{"Presence of record indicates read only database"},"@@"})
            if dummy != DB_OK then
               proc_reset()
               CURRENT[C_CC] = 8
               CURRENT[C_MSG] = {"Readonly record already exists"}
               return CURRENT
            end if
            readonlydb = append(readonlydb,db_return_currentdb())
            proc_reset()
            CURRENT[C_CC] = 0
            CURRENT[C_MSG] = {"Read only turned on"}
         elsif equal(COMMAND[A_QUAL2],"off") then
            -- delete read only record and remove database from read only list
            dummy = db_select_table("Control")
            if dummy != DB_OK then
               proc_reset()
               CURRENT[C_CC] = 8
               CURRENT[C_MSG] = {"Cannot find Control table to make database writeable"}
               return CURRENT
            end if
            dummy = db_find_key("readonly")
            if dummy < 1 then
               proc_reset()
               CURRENT[C_CC] = 8
               CURRENT[C_MSG] = {"Readonly record does not exist"}
               return CURRENT
            end if
            db_delete_record(dummy)
            dummy = find(db_return_currentdb(),readonlydb)
            readonlydb[dummy] = {}
            proc_reset()
            CURRENT[C_CC] = 0
            CURRENT[C_MSG] = {"Read only turned off"}
         end if      
      else
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Switch "&COMMAND[A_QUAL]&" not recognised"}
      end if
   end if
   
-- COPY ======Change this to work as export?=============================================================   
elsif equal(COMMAND[A_VERB],"copy") then
   DELTA = {0,0,0,0}
   CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"database") then
      firstdb = db_return_currentdb()
      seconddb = COMMAND[A_QUAL]
      dummy = match(seconddb,db_return_alldb())
      if dummy = 0 then
         concode = db_open(seconddb,DB_LOCK_NO)
         if concode != DB_OK then
            concode = db_create(seconddb,DB_LOCK_NO)
         end if   
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't open or create "&seconddb&" target database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      else
         concode = db_select(seconddb)
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't open or create "&seconddb&" target database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      end if
      concode = db_select(firstdb)
      tablist = db_table_list()
      dummy = length(tablist)
      for count2 = 1 to dummy do
         firsttable = tablist[count2]
         concode = db_select_table(firsttable)
         tablesize = db_table_size()
         concode = db_select(seconddb)
         concode = db_select_table(firsttable)
         if concode != DB_OK then
            concode = db_create_table(firsttable)
         end if   
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't open or create "&firsttable&" target table"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if   
         concode = db_select(firstdb)
         concode = db_select_table(firsttable)
         for count = 1 to tablesize do
            copykey = db_record_key(count)
            copyrecord = db_record_data(count)
            concode = db_select(seconddb)
            concode = db_select_table(firsttable)
            if equal(COMMAND[A_QUAL2],"@null") then
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,copyrecord)
                  DELTA[R_ADD] +=1
               else
                  DELTA[R_FAIL] +=1
               end if
            elsif equal(COMMAND[A_QUAL2],"header") then 
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
                  DELTA[R_ADD] +=1
               else
                  DELTA[R_FAIL] +=1
               end if               
            elsif equal(COMMAND[A_QUAL2],"noheader") then 
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,copyrecord[4])
                  DELTA[R_ADD] +=1
               else
                  DELTA[R_FAIL] +=1
               end if
            elsif equal(COMMAND[A_QUAL2],"force") then 
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,copyrecord)
                  DELTA[R_ADD] +=1
               else
                  db_replace_data(dummy,copyrecord)
                  DELTA[R_MOD] +=1
               end if
            elsif equal(COMMAND[A_QUAL2],"noheaderforce") then
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,copyrecord[4])
                  DELTA[R_ADD] +=1             
               else
                  db_replace_data(dummy,copyrecord[4])
                  DELTA[R_MOD] +=1
               end if
            elsif equal(COMMAND[A_QUAL2],"headerforce") then
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
                  DELTA[R_ADD] +=1             
               else
                  db_replace_data(dummy,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
                  DELTA[R_MOD] +=1
               end if   
            else
               CURRENT[C_MSG] = {"Copy option "&COMMAND[A_QUAL3]&" not recognised. "&func_delta()}
               CURRENT[C_CC] = 8
               return CURRENT
            end if
            if concode != DB_OK then
               CURRENT[C_MSG] = {"Can't insert record in target database. "&func_delta()}
               CURRENT[C_CC] = 8
               return CURRENT
            end if
            concode = db_select(firstdb)
            concode = db_select_table(firsttable)
         end for  
      end for
      CURRENT = db_action({"current","database",seconddb})
      CURRENT = db_action({"close","database"})
      CURRENT = db_action({"open","database",seconddb,"none"})
      CURRENT = db_action({"current","database",firstdb})
      CURRENT = db_action({"current","table",firsttable})
   elsif equal(COMMAND[A_OBJ],"table") then
      firstdb = db_return_currentdb()
      firsttable = db_return_currenttable()
      tablesize = db_table_size()
      seconddb = COMMAND[A_QUAL]
      secondtable = COMMAND[A_QUAL2]
      dummy = match(seconddb,db_return_alldb())
      if dummy = 0 then
         concode = db_open(seconddb,DB_LOCK_NO)
         if concode != DB_OK then
            concode = db_create(seconddb,DB_LOCK_NO)
         end if   
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't open or create "&seconddb&" target database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      else
         concode = db_select(seconddb)
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't select "&seconddb&" target database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if         
      end if   
      concode = db_select_table(secondtable)
      if concode != DB_OK then
         concode = db_create_table(secondtable)
      end if   
      if concode != DB_OK then
         CURRENT[C_MSG] = {"Can't open or create "&secondtable&" target table"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if   
      concode = db_select(firstdb)
      concode = db_select_table(firsttable)
      for count = 1 to tablesize do
         copykey = db_record_key(count)
         copyrecord = db_record_data(count)
         concode = db_select(seconddb)
         concode = db_select_table(secondtable)
         if equal(COMMAND[A_QUAL3],"@null") then
            dummy = db_find_key(copykey)
            if dummy < 0 then
               concode = db_insert(copykey,copyrecord)
               DELTA[R_ADD] +=1               
            else
               DELTA[R_FAIL] +=1
            end if
         elsif equal(COMMAND[A_QUAL3],"header") then 
               dummy = db_find_key(copykey)
               if dummy < 0 then
                  concode = db_insert(copykey,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
                  DELTA[R_ADD] +=1
               else
                  DELTA[R_FAIL] +=1
               end if   
         elsif equal(COMMAND[A_QUAL3],"noheader") then 
            dummy = db_find_key(copykey)
            if dummy < 0 then
               concode = db_insert(copykey,copyrecord[4])
               DELTA[R_ADD] +=1                           
            else
               DELTA[R_FAIL] +=1
            end if
         elsif equal(COMMAND[A_QUAL3],"force") then 
            dummy = db_find_key(copykey)
            if dummy < 0 then
               concode = db_insert(copykey,copyrecord)
               DELTA[R_ADD] +=1                                   
            else
               db_replace_data(dummy,copyrecord)
               DELTA[R_MOD] +=1
            end if
         elsif equal(COMMAND[A_QUAL3],"noheaderforce") then
            dummy = db_find_key(copykey)
            if dummy < 0 then
               concode = db_insert(copykey,copyrecord[4])
               DELTA[R_ADD] +=1                   
            else
               db_replace_data(dummy,copyrecord[4])
               DELTA[R_MOD] +=1
            end if
         elsif equal(COMMAND[A_QUAL3],"headerforce") then
            dummy = db_find_key(copykey)
            if dummy < 0 then
               concode = db_insert(copykey,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
               DELTA[R_ADD] +=1             
            else
               db_replace_data(dummy,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
               DELTA[R_MOD] +=1
            end if   
         else
            CURRENT[C_MSG] = {"Copy option "&COMMAND[A_QUAL3]&" not recognised. "&func_delta()}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't insert record in target database. "&func_delta()}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         concode = db_select(firstdb)
         concode = db_select_table(firsttable)
      end for
      CURRENT = db_action({"current","database",seconddb})
      CURRENT = db_action({"close","database"})
      CURRENT = db_action({"open","database",seconddb,"none"})
      CURRENT = db_action({"current","database",firstdb})
      CURRENT = db_action({"current","table",firsttable})
   elsif equal(COMMAND[A_OBJ],"record") then 
      firstdb = db_return_currentdb()
      firsttable = db_return_currenttable()
      copykey = db_record_key(PREVIOUS[C_POSN])
      copyrecord = db_record_data(PREVIOUS[C_POSN])
      seconddb = COMMAND[A_QUAL]
      secondtable = COMMAND[A_QUAL2]
      dummy = match(seconddb,db_return_alldb())
      if dummy = 0 then
         concode = db_open(seconddb,DB_LOCK_NO)
         if concode != DB_OK then
            concode = db_create(seconddb,DB_LOCK_NO)
         end if   
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't open or create "&seconddb&" target database. "&func_delta()}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      else
         concode = db_select(seconddb)
         if concode != DB_OK then
            CURRENT[C_MSG] = {"Can't select "&seconddb&" target database. "&func_delta()}
            CURRENT[C_CC] = 8
            return CURRENT
         end if         
      end if   
      concode = db_select_table(secondtable)
      if concode != DB_OK then
         concode = db_create_table(secondtable)
      end if   
      if concode != DB_OK then
         CURRENT[C_MSG] = {"Can't open or create "&secondtable&" target table. "&func_delta()}
         CURRENT[C_CC] = 8
         return CURRENT
      end if   
      concode = db_select(seconddb)
      concode = db_select_table(secondtable)
      if equal(COMMAND[A_QUAL3],"@null") then
         dummy = db_find_key(copykey)
         if dummy < 0 then
            concode = db_insert(copykey,copyrecord)
            DELTA[R_ADD] +=1
         else
            DELTA[R_FAIL] +=1
         end if
      elsif equal(COMMAND[A_QUAL3],"header") then 
         dummy = db_find_key(copykey)
         if dummy < 0 then
            concode = db_insert(copykey,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
            DELTA[R_ADD] +=1
         else
            DELTA[R_FAIL] +=1
         end if   
      elsif equal(COMMAND[A_QUAL3],"noheader") then 
         dummy = db_find_key(copykey)
         if dummy < 0 then
            concode = db_insert(copykey,copyrecord[4])
            DELTA[R_ADD] +=1
         else
            DELTA[R_FAIL] +=1
         end if
      elsif equal(COMMAND[A_QUAL3],"force") then 
         dummy = db_find_key(copykey)
         if dummy < 0 then
            concode = db_insert(copykey,copyrecord)
            DELTA[R_ADD] +=1
         else
            db_replace_data(dummy,copyrecord)
            DELTA[R_MOD] +=1
         end if
      elsif equal(COMMAND[A_QUAL3],"noheaderforce") then
         dummy = db_find_key(copykey)
         if dummy < 0 then
            concode = db_insert(copykey,copyrecord[4])
            DELTA[R_ADD] +=1
         else
            db_replace_data(dummy,copyrecord[4])
            DELTA[R_MOD] +=1
         end if
      elsif equal(COMMAND[A_QUAL3],"headerforce") then
         dummy = db_find_key(copykey)
         if dummy < 0 then
            concode = db_insert(copykey,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
            DELTA[R_ADD] +=1             
         else
            db_replace_data(dummy,{"U",COMMAND[A_QUAL4],NOLINKS,copyrecord,"@@"})
            DELTA[R_MOD] +=1
         end if    
      else
         CURRENT[C_MSG] = {"Export option "&COMMAND[A_QUAL3]&" not recognised. "&func_delta()}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      if concode != DB_OK then
         CURRENT[C_MSG] = {"Can't insert record in target database. "&func_delta()}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT = db_action({"current","database",seconddb})
      CURRENT = db_action({"close","database"})
      CURRENT = db_action({"open","database",seconddb,"none"})
      CURRENT = db_action({"current","database",firstdb})
      CURRENT = db_action({"current","table",firsttable})
   end if
   CURRENT[C_CC] = 0
   CURRENT[C_MSG] = {"Copy completed. "&func_delta()}   
   return CURRENT 
   
-- CREATE ========================================================================================================== 
elsif equal(COMMAND[A_VERB],"create") then
   DELTA = {0,0,0,0}
   if equal(COMMAND[A_OBJ],"database") then
      if equal(COMMAND[A_QUAL3],"none") then
         COMMAND[A_QUAL3] = DB_LOCK_NO
      else
         COMMAND[A_QUAL3] = DB_LOCK_EXCLUSIVE
      end if
      concode = db_create(EDBDIR&COMMAND[A_QUAL],COMMAND[A_QUAL3])
      if concode != DB_OK then
         CURRENT[C_MSG] = {"Unable to create database "&COMMAND[A_QUAL]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if   
      if concode = 0 and equal(COMMAND[A_QUAL2],"idb") then
         CURRENT = db_action({"create","table","Control"})
         if CURRENT[C_CC] > 1 then return CURRENT end if
         CURRENT = db_action({"add","record","created","control",IDB_fdate(date(),{})})
         if CURRENT[C_CC] > 1 then
            DELTA[R_FAIL] +=1
            return CURRENT
         else
            DELTA[R_ADD] +=1
         end if
         CURRENT = db_action({"add","record","IDB version","control","IDB v0.8"})
         if CURRENT[C_CC] > 1 then
            DELTA[R_FAIL] +=1
            return CURRENT
         else
            DELTA[R_ADD] +=1
         end if
         CURRENT = db_action({"add","record","Euphoria version","control","Euphoria 2.4"})
         if CURRENT[C_CC] > 1 then
            DELTA[R_FAIL] +=1
            return CURRENT
         else
            DELTA[R_ADD] +=1
         end if
         RECDAT = {COMMAND[A_QUAL],"Description field ready for update"}
         CURRENT = db_action({"add","record","description","control",RECDAT})
         if CURRENT[C_CC] > 1 then
            DELTA[R_FAIL] +=1
            return CURRENT
         else
            DELTA[R_ADD] +=1
         end if
         RECDAT = {"Field ready for commit statistics ",IDB_fdate(date(),{})}
         CURRENT = db_action({"add","record","commit","control",RECDAT})
         if CURRENT[C_CC] > 1 then
            DELTA[R_FAIL] +=1
            return CURRENT
         else
            DELTA[R_ADD] +=1
         end if
         CURRENT = db_action({"create","table","Description"})
         if CURRENT[C_CC] > 1 then return CURRENT end if
         CURRENT[C_MSG] = {COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" created. Add "&sprintf("%d",DELTA[R_ADD])&"; Mod "&
      sprintf("%d",DELTA[R_MOD])&"; Del "&sprintf("%d",DELTA[R_DEL])&"; Fail "&
      sprintf("%d",DELTA[R_FAIL])}
      end if
      CURRENT = db_action({"close","database"})
      CURRENT = db_action({"open","database",EDBDIR&COMMAND[A_QUAL],"none"}) 
   elsif equal(COMMAND[A_OBJ],"table") then
      CURRENT[C_MSG] = db_create_table(COMMAND[A_QUAL])
      if CURRENT[C_MSG] = 0 then
         CURRENT[C_MSG] = {COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" created."}
      else
         CURRENT[C_MSG] = {"Unable to create table "&COMMAND[A_QUAL]&" , return code "&sprint(CURRENT[C_MSG])}
         CURRENT[C_CC]= 8
         return CURRENT
      end if
   end if   
-- CURRENT ==========================================================================================================   
elsif equal(COMMAND[A_VERB],"current") then
   CURRENT[C_DB] = db_return_currentdb()
   if not equal(CURRENT[C_DB],"No database open") then
      if equal(COMMAND[A_OBJ],"database") and match(COMMAND[A_QUAL],db_return_alldb()) then
         concode = db_select(COMMAND[A_QUAL])
         if concode = DB_OK then
            CURRENT = func_current(0)
            CURRENT[C_MSG] = {COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" is current"}
            CURRENT[C_TABLE] = {}
            dummy = db_table_list()
            if length(dummy) then
               CURRENT[C_TABLE]  = dummy[1]   
               concode = db_select_table(CURRENT[C_TABLE] )
               if concode = DB_OK then
                  CURRENT[C_CC] = 0 
                  CURRENT[C_SIZE] = db_table_size()
               else
                   CURRENT[C_CC] = 8 
                   CURRENT[C_MSG] = {"Unable to show current table"}
                   CURRENT[C_TABLE] = {"No table current"}
               end if
            else 
            CURRENT[C_CC] = 8 
            CURRENT[C_MSG] = {"Unable to show current table"}
            CURRENT[C_TABLE] = {"No table current"}
            end if
         else
            CURRENT[C_MSG] = {"Unable to make "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" current"}
            CURRENT[C_CC]= 8
         end if
      elsif equal(COMMAND[A_OBJ],"table") and find(COMMAND[A_QUAL],db_table_list()) then
         CURRENT[C_MSG] = db_select_table(COMMAND[A_QUAL])
         CURRENT[C_MSG] = {COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" is current"}
         CURRENT[C_TABLE] = db_return_currenttable()
         CURRENT[C_SIZE] = db_table_size()
         CURRENT[C_POSN] = {}
         CURRENT[C_CC] = 0
      else
         CURRENT[C_MSG] = {"Unable to make "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" current"}
         CURRENT[C_TABLE] = db_return_currenttable()
         CURRENT[C_SIZE] = db_table_size()
         CURRENT[C_CC]= 8
      end if
   else
      CURRENT[C_MSG] = {"No database open"}
      CURRENT[C_CC] = 8
   end if
-- DEBUG ==========================================================================================================  
elsif equal(COMMAND[A_VERB],"debug") then
   CURRENT[C_DB] = db_return_currentdb()
   if equal(CURRENT[C_DB],"No database open") then
      CURRENT[C_MSG] = {"No database open"}
      CURRENT[C_DB] = {"No database open"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if
   if equal(COMMAND[A_QUAL],"screen") then
      if report = 0 then
         CURRENT[C_MSG] = {"Debug to screen command not available in non-report or GUI mode"}
         CURRENT[C_CC] = 8
         return CURRENT
      else  
         filenum = 1
      end if
   else
      if equal(COMMAND[A_QUAL],"@null") then
         filenum = open(OUTPUTFILE,"w")
      else
         filenum = open(COMMAND[A_QUAL],"w")
      end if   
   end if   
   if equal(COMMAND[A_OBJ],"database") then
      CURRENT[C_TABLIST] = db_table_list()
      for count = 1 to length(CURRENT[C_TABLIST]) do
         CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLIST][count])
         CURRENT = func_dbtabsize()
         for count2 = 1 to CURRENT[C_SIZE] do
            CURRENT[C_KEY] = db_record_key(count2)
            recbody = db_record_data(count2)
            print(filenum,{CURRENT[C_TABLE],CURRENT[C_KEY],recbody})
         end for
         if filenum = 1 then puts(1,'\n') end if 
      end for  
   elsif equal(COMMAND[A_OBJ],"table") then
      CURRENT[C_TABLE] = db_return_currenttable()
      if equal(CURRENT[C_TABLE],"No tables current") then
         CURRENT[C_MSG] = {"No tables current"}
         CURRENT[C_DB] = db_return_currentdb()
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT = func_dbtabsize()
      for count = 1 to CURRENT[C_SIZE] do
         CURRENT[C_KEY] = db_record_key(count)
         recbody = db_record_data(count)
         print(filenum,{CURRENT[C_TABLE],CURRENT[C_KEY],recbody})
      end for
      if filenum = 1 then puts(1,'\n') end if 
   elsif equal(COMMAND[A_OBJ],"record") then
      proc_reset()
      recbody = db_record_data(CURRENT[C_POSN])
      print(filenum,{CURRENT[C_TABLE],CURRENT[C_KEY],recbody})
      if filenum = 1 then puts(1,'\n') end if 
   end if
   if filenum > 1 then close(filenum) end if
   CURRENT[C_CC] = 0 
   
-- DELETE ========================================================================================================== 
elsif equal(COMMAND[A_VERB],"delete") then
   DELTA = {0,0,0,0}
   CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"database") then
      if equal(COMMAND[A_QUAL],"@null") then
         CURRENT[C_DB] = db_return_currentdb()
      elsif find(COMMAND[A_QUAL],db_return_alldb()) then
         CURRENT[C_DB] = db_select(COMMAND[A_QUAL])
      end if
      dummy = CURRENT[C_DB]
      CURRENT = db_action({"return","current"})
      CURRENT[C_TABLIST] = db_table_list()
      if warning and nofile then
         CURRENT[C_CC] = func_warning({"Are you sure you want to delete database "&CURRENT[C_DB]&"?"})
         if CURRENT[C_CC] > 4 then return CURRENT end if
      end if
      for count = 1 to length(CURRENT[C_TABLIST]) do
         CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLIST][count])
         CURRENT = func_dbtabsize()
         DELTA[R_DEL] += CURRENT[C_SIZE]
         db_delete_table(CURRENT[C_TABLIST][count])
      end for
      db_close()
      -- ** insert file delete code
      CURRENT = IDB_zero()
      CURRENT[C_MSG] = {"Database "&dummy&" deleted. "&func_delta()}
      CURRENT[C_CC] = 0
   elsif equal(COMMAND[A_OBJ],"table") then
      if equal(COMMAND[A_QUAL],"@null") then
         CURRENT[C_TABLE] = db_return_currenttable()
         if not func_raw_db() then
            for count = 1 to db_table_size() do
               recbody = db_record_data(count)
               if equal(recbody[1],"R") then
                  CURRENT[C_MSG] = {"Locked records in table "&CURRENT[C_TABLE]&". Table not deleted"}
                  CURRENT[C_CC] = 8
                  DELTA[R_FAIL] = count - DELTA[R_DEL]
               elsif not equal(recbody[3],NOLINKS) then
                  CURRENT[C_MSG] = {"Linked records in table "&CURRENT[C_TABLE]&". Table not deleted"}
                  CURRENT[C_CC] = 8
                  DELTA[R_FAIL] = count - DELTA[R_DEL]
               else
                  DELTA[R_DEL] +=1
               end if
               if CURRENT[C_CC] > 7 then
                  return CURRENT
               end if   
            end for 
         end if
         db_delete_table(db_return_currenttable())
         CURRENT[C_CC] = 0
         CURRENT[C_MSG] = {"Table "&CURRENT[C_TABLE]&" deleted. "&func_delta()}
         if equal(CURRENT[C_TABLE],"Control") then
            dummy = db_return_currentdb()
            CURRENT = db_action({"close","database"})
            CURRENT = db_action({"open","database",dummy,"none"})
         end if
      elsif find(COMMAND[A_QUAL],db_table_list()) then
         CURRENT[C_MSG] = db_select_table(COMMAND[A_QUAL])
         if not func_raw_db() then
            for count = 1 to db_table_size() do
               recbody = db_record_data(count)
               if equal(recbody[1],"R") then
                  CURRENT[C_MSG] = {"Locked records in table "&CURRENT[C_TABLE]&". Table not deleted"}
                  CURRENT[C_CC] = 8
                  DELTA[R_FAIL] = count - DELTA[R_DEL]
               elsif not equal(recbody[3],NOLINKS) then
                  CURRENT[C_MSG] = {"Linked records in table "&CURRENT[C_TABLE]&". Table not deleted"}
                  CURRENT[C_CC] = 8
                  DELTA[R_FAIL] = count - DELTA[R_DEL]
               else
                  DELTA[R_DEL] +=1
               end if
               if CURRENT[C_CC] > 7 then
                  return CURRENT
               end if   
            end for 
         end if
         db_delete_table(db_return_currenttable())
         CURRENT[C_CC] = 0
         CURRENT[C_MSG] = {"Table "&CURRENT[C_TABLE]&" deleted. "&func_delta()}
         if equal(CURRENT[C_TABLE],"Control") then
            dummy = db_return_currentdb()
            CURRENT = db_action({"close","database"})
            CURRENT = db_action({"open","database",dummy,"none"})
         end if
      else
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Table "&COMMAND[A_QUAL]&" not recognised"}
      end if
   elsif equal(COMMAND[A_OBJ],"record") then
      if equal(COMMAND[A_QUAL],"@null") then
         if equal(PREVIOUS[C_POSN],{}) then
            proc_reset()
            CURRENT[C_MSG] = {"No record current"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         CURRENT = db_action({"get","record",(PREVIOUS[C_POSN])})
         dummy = PREVIOUS[C_POSN]
         if not func_raw_db() then
            if equal(CURRENT[C_STATUS],"R") then
               DELTA[R_FAIL]+=1
               proc_reset()
               CURRENT[C_MSG] = {"Record locked, not deleted. "&func_delta()}
               CURRENT[C_CC] = 8
               return CURRENT
            elsif not equal(CURRENT[C_LINKS],NOLINKS) then
               DELTA[R_FAIL]+=1
               proc_reset()
               CURRENT[C_MSG] = {"Record linked, not deleted. "&func_delta()}
               CURRENT[C_CC] = 8
               return CURRENT
            end if  
         end if  
         db_delete_record(CURRENT[C_POSN])
         DELTA[R_DEL]+=1
         if dummy <= CURRENT[C_SIZE] and dummy > 1 then
            CURRENT = IDB_zero()
            CURRENT = func_current(dummy-1)
         else
            CURRENT = db_action({"return","current"})
         end if
         CURRENT[C_MSG] = {"Record deleted. "&func_delta()}
      else   
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
         CURRENT = db_action({"get","record",(COMMAND[A_QUAL])})
         if CURRENT[C_CC] > 7 then
            CURRENT[C_MSG] = {"No current record to be deleted"}
            return CURRENT
         end if   
         if COMMAND[A_QUAL] > 0 and COMMAND[A_QUAL]<= db_table_size() then
            dummy = COMMAND[A_QUAL]
            if not func_raw_db() then
               if equal(CURRENT[C_STATUS],"R") then
                  DELTA[R_FAIL]+=1
                  proc_reset()
                  CURRENT[C_MSG] = {"Record locked, not deleted. "&func_delta()}
                  CURRENT[C_CC] = 8
                  return CURRENT
               elsif not equal(CURRENT[C_LINKS],NOLINKS) then
                  DELTA[R_FAIL]+=1
                  proc_reset()
                  CURRENT[C_MSG] = {"Record linked, not deleted. "&func_delta()}
                  CURRENT[C_CC] = 8
                  return CURRENT
               end if  
            end if
            db_delete_record(CURRENT[C_POSN])
            DELTA[R_DEL]+=1
            CURRENT[C_MSG] = {"Record "&sprintf("%d",CURRENT[C_POSN])&" deleted ."&func_delta()}
         end if
         CURRENT[C_SIZE] = db_table_size()
         if dummy <= CURRENT[C_SIZE] and dummy > 1 then
            CURRENT = IDB_zero()
            CURRENT = func_current(dummy-1)
         else
            CURRENT = db_action({"return","current"})
         end if
         CURRENT[C_MSG] = {"Record deleted. "&func_delta()}
      end if
   end if   
-- EDIT ============================================================================================================ 
elsif equal(COMMAND[A_VERB],"edit") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if report = 0 then
       CURRENT[C_MSG] = {"Edit command not available in non-report or GUI mode"}
       CURRENT[C_CC] = 8
       return CURRENT
   end if
   CURRENT = func_current(PREVIOUS[C_POSN])
   if CURRENT[C_CC] > 0 then
      return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"field") then
      recbody = CURRENT[C_BODY]
      if not func_raw_db() then
         if equal(recbody[1],"U") then
            COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
            if COMMAND[A_QUAL] > length(recbody[4]) then
               CURRENT[C_MSG] = "Field number too big"
               CURRENT[C_CC] = 8
               return CURRENT
            end if   
            text_color(BRIGHT_GREEN)
            IDB_flatprint(1,{"EDIT MODE "},{"%d","%f","%s",0,0,'\n'})
            text_color(GREEN)
            recbody[4][COMMAND[A_QUAL]] = func_edit(recbody[4][COMMAND[A_QUAL]],80)
            db_replace_data(CURRENT[C_POSN],recbody)
            CURRENT = func_current(CURRENT[C_POSN])
         else
            proc_reset()
            CURRENT[C_CC] = 8
            CURRENT[C_MSG] = {"Record locked against update"}
         end if
      else
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
         if COMMAND[A_QUAL] > length(recbody) then
            CURRENT[C_MSG] = "Field number too big"
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         text_color(BRIGHT_GREEN)
         IDB_flatprint(1,{"EDIT MODE "},{"%d","%f","%s",0,0,'\n'})
         text_color(GREEN)         
         recbody[COMMAND[A_QUAL]] = func_edit(recbody[COMMAND[A_QUAL]],80)
         db_replace_data(CURRENT[C_POSN],recbody)
         CURRENT = func_current(CURRENT[C_POSN])
      end if
      text_color(BRIGHT_GREEN)
      puts(1,'\n')
      IDB_flatprint(1,{"END EDIT MODE "},{"%d","%f","%s",0,0,'\n'})
      text_color(GREEN)
   end if         
   
-- EXIT ========================================================================================================
elsif equal(COMMAND[A_VERB],"exit") then
   CURRENT = IDB_zero()
   CURRENT[C_CC] = 12

-- EXPORT  ========================================================================================================  
elsif equal(COMMAND[A_VERB],"export") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"database") or equal(COMMAND[A_OBJ],"table") or 
      equal(COMMAND[A_OBJ],"record") then
      if equal(COMMAND[A_QUAL],"screen") then
         if report = 0 then
            CURRENT[C_MSG] = {"Export to screen command not available in non-report or GUI mode"}
            CURRENT[C_CC] = 8
            return CURRENT
         else  
            filenum = 1
         end if
      else
         if equal(COMMAND[A_QUAL],"@null") or equal(COMMAND[A_QUAL],"csv") then
            filenum = open(OUTPUTFILE,"w")
            COMMAND[A_QUAL2] = "csv"
         else
            filenum = open(COMMAND[A_QUAL],"w")
         end if   
      end if
      if equal(COMMAND[A_OBJ],"database") then
         CURRENT = func_current(1)
         if CURRENT[C_CC] > 0 then
            return CURRENT
         end if
         CURRENT[C_TABLIST] = db_table_list()
         for count = 1 to length(CURRENT[C_TABLIST]) do
            dummy = db_select_table(CURRENT[C_TABLIST][count])
            dummy = CURRENT[C_TABLIST][count]
            CURRENT = func_dbtabsize()
            for count2 = 1 to CURRENT[C_SIZE] do
               copykey = db_record_key(count2)
               copyrecord = db_record_data(count2)
               if equal(COMMAND[A_QUAL2],"csv") then
                  IDB_flatprint(filenum,dummy,{"%d","%f","%s",34,126,{}})
                  puts(filenum,",")
                  IDB_flatprint(filenum,copykey,{"%d","%f","%s",34,126,{}})
                  puts(filenum,",\"")
                  IDB_flatprint(filenum,copyrecord,{"%d","%f","%s",34,"\",\"",34})
                  puts(filenum,'\n')
               else
                  IDB_flatprint(filenum,dummy,{"%d","%f","%s",0,44,44})
                  IDB_flatprint(filenum,copykey,{"%d","%f","%s",0,44,44})
                  IDB_flatprint(filenum,copyrecord,{"%d","%f","%s",0,44,'\n'})
               end if
            end for
         end for  
      elsif equal(COMMAND[A_OBJ],"table") then
         CURRENT = func_current(1)
         if CURRENT[C_CC] > 0 then
             return CURRENT
         end if
         CURRENT = func_dbtabsize()
         dummy = CURRENT[C_TABLIST]
         for count = 1 to CURRENT[C_SIZE] do
            copykey = db_record_key(count)
            copyrecord = db_record_data(count)
            if equal(COMMAND[A_QUAL2],"csv") then
               IDB_flatprint(filenum,dummy,{"%d","%f","%s",34,126,{}})
               puts(filenum,",")
               IDB_flatprint(filenum,copykey,{"%d","%f","%s",34,126,{}})
               puts(filenum,",\"")
               IDB_flatprint(filenum,copyrecord,{"%d","%f","%s",34,"\",\"",34})
               puts(filenum,'\n')
            else
               IDB_flatprint(filenum,dummy,{"%d","%f","%s",0,44,44})
               IDB_flatprint(filenum,copykey,{"%d","%f","%s",0,44,44})
               IDB_flatprint(filenum,copyrecord,{"%d","%f","%s",0,44,'\n'})
            end if  
         end for
      elsif equal(COMMAND[A_OBJ],"record") then
         CURRENT = func_current(PREVIOUS[C_POSN])
         if CURRENT[C_CC] > 0 then
               return CURRENT
         end if 
         if equal(COMMAND[A_QUAL2],"csv") then
            IDB_flatprint(filenum,CURRENT[C_TABLE],{"%d","%f","%s",34,126,{}})
            puts(filenum,",")
            IDB_flatprint(filenum,CURRENT[C_KEY],{"%d","%f","%s",34,126,{}})
            puts(filenum,",\"")
            IDB_flatprint(filenum,CURRENT[C_BODY],{"%d","%f","%s",34,"\",\"",34})
            puts(filenum,'\n')
         else
            IDB_flatprint(filenum,CURRENT[C_TABLE],{"%d","%f","%s",0,44,44})
            IDB_flatprint(filenum,CURRENT[C_KEY],{"%d","%f","%s",0,44,44})
            IDB_flatprint(filenum,CURRENT[C_BODY],{"%d","%f","%s",0,44,'\n'})
         end if
      end if
      if filenum > 1 then close(filenum) end if
   else
      CURRENT = PREVIOUS
      CURRENT[C_MSG] = {"Export object "&COMMAND[A_OBJ]&" not recognised"}
      CURRENT[C_CC] = 8
   end if   
   
-- FIND ========================================================================================================
elsif equal(COMMAND[A_VERB],"find") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if func_raw_db() and (equal(COMMAND[A_OBJ],"owner") or equal(COMMAND[A_OBJ],"prior") or
      equal(COMMAND[A_OBJ],"next") or equal(COMMAND[A_OBJ],"sub")) then
      CURRENT[C_MSG] ={"Find links not available in EDS database"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if
   CURRENT = func_dbtabsize()
   LINKER = repeat("@null",10)
   if CURRENT[C_CC]= 8 then return CURRENT end if
   if equal(COMMAND[A_OBJ],"first") then
      CURRENT = func_current(1)
      proc_discard_data()
   elsif equal(COMMAND[A_OBJ],"last") then
      CURRENT = func_current(db_table_size())
      proc_discard_data()
   elsif equal(COMMAND[A_OBJ],"record") then
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      end if
      if not integer(COMMAND[A_QUAL]) then
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if
      if not integer(COMMAND[A_QUAL]) or COMMAND[A_QUAL] < 1 then
         proc_reset()
         CURRENT[C_MSG] = {"Value is not a positive integer"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT[C_SIZE] = db_table_size()
      if COMMAND[A_QUAL] > CURRENT[C_SIZE] then
         proc_reset()
         CURRENT[C_MSG] = {"Table has fewer records"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if   
      CURRENT = func_current(COMMAND[A_QUAL])
      proc_discard_data()
   elsif equal(COMMAND[A_OBJ],"plus") and not equal(db_return_currenttable(),"No tables current") then
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      end if
      if not integer(COMMAND[A_QUAL]) then
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if
      if not integer(COMMAND[A_QUAL]) then
         proc_reset()
         CURRENT[C_MSG] = {"Increment is not an integer"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT[C_DB] = db_return_currentdb()
      if equal(PREVIOUS[C_POSN],"") then
         PREVIOUS[C_POSN] = 0
      end if
      recposn = PREVIOUS[C_POSN] + COMMAND[A_QUAL]
      if  recposn > CURRENT[C_SIZE] then
         CURRENT[C_MSG] = {"Reached end of "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
      else
         CURRENT = func_current(recposn)
         proc_discard_data()
      end if
   elsif equal(COMMAND[A_OBJ],"minus") then
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      end if
      if not integer(COMMAND[A_QUAL]) then
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if
      if not integer(COMMAND[A_QUAL]) then
         proc_reset()
         CURRENT[C_MSG] = {"Increment is not an integer"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      if equal(PREVIOUS[C_POSN],"") then
         PREVIOUS[C_POSN] = db_table_size()+1
      end if
      recposn = PREVIOUS[C_POSN] - COMMAND[A_QUAL]
      if  recposn < 1 then
         proc_reset()
         CURRENT[C_MSG] = {"Reached start of "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
      else
         CURRENT = func_current(recposn)
         proc_discard_data()
      end if
   elsif equal(COMMAND[A_OBJ],"key") then
      recposn = db_find_key(COMMAND[A_QUAL])
      if recposn < 1 then
         recposn = db_find_key(IDB_texttonum(COMMAND[A_QUAL]))
      end if
      if recposn < 1 then
         proc_reset()
         CURRENT[C_MSG] = {"Key "&COMMAND[A_QUAL]&" not found in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(recposn)
         proc_discard_data()
      end if
   elsif equal(COMMAND[A_OBJ],"tabkey") then
      dummy = db_select_table(COMMAND[A_QUAL])
      if dummy != DB_OK then
         CURRENT[C_MSG] = {"Table "&CURRENT[C_TABLE]&" not found."}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      recposn = db_find_key(COMMAND[A_QUAL2])
      if recposn < 1 then
         recposn = db_find_key(IDB_texttonum(COMMAND[A_QUAL2]))
      end if
      if recposn < 1 then
         CURRENT[C_MSG] = {"Key "&COMMAND[A_QUAL2]&" not found in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(recposn)
         proc_discard_data()
      end if    
   elsif equal(COMMAND[A_OBJ],"tabpos") then
      CURRENT = db_action({"current","table",COMMAND[A_QUAL]})
      if CURRENT[C_CC] != 0 then
         CURRENT[C_MSG] = {"Table "&CURRENT[C_TABLE]&" not found."}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      dummy = IDB_texttonum(COMMAND[A_QUAL2])
      if not integer(dummy) or dummy < 1 or dummy > db_table_size() then
         CURRENT[C_MSG] = {"Position "&COMMAND[A_QUAL2]&" not found in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(dummy)
         proc_discard_data()
      end if     
   else  
      if equal(PREVIOUS[C_POSN],{}) then 
         CURRENT[C_MSG] = {"No record current"}
         CURRENT[C_CC] = 8
      return CURRENT 
      end if   
      oldrecbody = db_select_table(PREVIOUS[C_TABLE])
      oldrecbody = db_record_data(PREVIOUS[C_POSN])
      CURRENT = repeat({},C_MAX)
      if equal(COMMAND[A_OBJ],"owner") then
         if equal(oldrecbody[3][1],"@") then 
            proc_reset()
            CURRENT[C_MSG] ={"Owner record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][1]
            CURRENT[C_KEY] = oldrecbody[3][2]
         end if   
      elsif equal(COMMAND[A_OBJ],"prior") then
         if equal(oldrecbody[3][5],"@") then 
            proc_reset()
            CURRENT[C_MSG] ={"Prior record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][5]
            CURRENT[C_KEY] = oldrecbody[3][6]
         end if 
      elsif equal(COMMAND[A_OBJ],"next") then
         if equal(oldrecbody[3][7],"@") then 
            proc_reset()
            CURRENT[C_MSG] ={"Next record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][7]
            CURRENT[C_KEY] = oldrecbody[3][8]
         end if 
      elsif equal(COMMAND[A_OBJ],"sub") then
         if equal(oldrecbody[3][3],"@") then 
            proc_reset()
            CURRENT[C_MSG] = {"Sub record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][3]
            CURRENT[C_KEY] = oldrecbody[3][4]
         end if 
      else
         CURRENT[C_MSG] = {"Can't get "&COMMAND[A_OBJ]&", "&COMMAND[A_QUAL]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if
      CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLE])
      if CURRENT[C_MSG] < 0 then
         CURRENT[C_MSG] = {"Can't find table "&CURRENT[C_TABLE]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if   
      CURRENT[C_POSN] = db_find_key(CURRENT[C_KEY])
      if CURRENT[C_POSN] < 1 then
         CURRENT[C_MSG] = {"Can't find record key "&CURRENT[C_KEY]&" in table "&CURRENT[C_TABLE]&" GET routine"}
         CURRENT[C_CC]= 8
         return CURRENT
      end if   
      CURRENT = func_current(CURRENT[C_POSN])
      CURRENT[C_DATA] = {}
      CURRENT[C_MSG] ={COMMAND[A_OBJ]&" record found"}
      CURRENT[C_CC] = 0
   end if   
   
-- GET ==========================================================================================================
elsif equal(COMMAND[A_VERB],"get") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if func_raw_db() and (equal(COMMAND[A_OBJ],"owner") or equal(COMMAND[A_OBJ],"prior") or
      equal(COMMAND[A_OBJ],"next") or equal(COMMAND[A_OBJ],"sub")) then
      CURRENT[C_MSG] ={"Get links not available in EDS database"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if
   CURRENT = func_dbtabsize()
   LINKER = repeat("@null",10)
   if CURRENT[C_CC]= 8 then return CURRENT end if
   if equal(COMMAND[A_OBJ],"first") then
      CURRENT = func_current(1)
   elsif equal(COMMAND[A_OBJ],"last") then
      CURRENT = func_current(db_table_size())
   elsif equal(COMMAND[A_OBJ],"record") then
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      end if
      if not integer(COMMAND[A_QUAL]) then
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if
      if not integer(COMMAND[A_QUAL]) or COMMAND[A_QUAL] < 1 then
         proc_reset()
         CURRENT[C_MSG] = {"Value is not a positive integer"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT[C_SIZE] = db_table_size()
      if COMMAND[A_QUAL] > CURRENT[C_SIZE] then
         proc_reset()
         CURRENT[C_MSG] = {"Table has fewer records"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if   
      CURRENT = func_current(COMMAND[A_QUAL])
   elsif equal(COMMAND[A_OBJ],"plus") and not equal(db_return_currenttable(),"No tables current") then
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      end if
      if not integer(COMMAND[A_QUAL]) then
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if
      if not integer(COMMAND[A_QUAL]) then
         proc_reset()
         CURRENT[C_MSG] = {"Increment is not an integer"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT[C_DB] = db_return_currentdb()
      if equal(PREVIOUS[C_POSN],"") then
         PREVIOUS[C_POSN] = 0
      end if
      recposn = PREVIOUS[C_POSN] + COMMAND[A_QUAL]
      if  recposn > CURRENT[C_SIZE] then
         CURRENT[C_MSG] = {"Reached end of "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
      else
         CURRENT = func_current(recposn)
      end if
   elsif equal(COMMAND[A_OBJ],"minus") then
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      end if
      if not integer(COMMAND[A_QUAL]) then
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if
      if not integer(COMMAND[A_QUAL]) then
         proc_reset()
         CURRENT[C_MSG] = {"Increment is not an integer"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      if equal(PREVIOUS[C_POSN],"") then
         PREVIOUS[C_POSN] = db_table_size()+1
      end if
      recposn = PREVIOUS[C_POSN] - COMMAND[A_QUAL]
      if  recposn < 1 then
         CURRENT[C_MSG] = {"Reached start of "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
      else
         CURRENT = func_current(recposn)
      end if
   elsif equal(COMMAND[A_OBJ],"key") then
      recposn = db_find_key(COMMAND[A_QUAL])
      if recposn < 1 then
               recposn = db_find_key(IDB_texttonum(COMMAND[A_QUAL]))
      end if
      if recposn < 1 then
         CURRENT[C_MSG] = {"Key "&COMMAND[A_QUAL]&" not found in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(recposn)
      end if
   elsif equal(COMMAND[A_OBJ],"tabkey") then
      dummy = db_select_table(COMMAND[A_QUAL])
      if dummy != DB_OK then
         CURRENT[C_MSG] = {"Table "&CURRENT[C_TABLE]&" not found."}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      recposn = db_find_key(COMMAND[A_QUAL2])
      if recposn < 1 then
         recposn = db_find_key(IDB_texttonum(COMMAND[A_QUAL2]))
      end if
      if recposn < 1 then
         CURRENT[C_MSG] = {"Key "&COMMAND[A_QUAL2]&" not found in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(recposn)
      end if
   elsif equal(COMMAND[A_OBJ],"tabpos") then
   CURRENT = db_action({"current","table",COMMAND[A_QUAL]})
      if CURRENT[C_CC] != 0 then
         CURRENT[C_MSG] = {"Table "&CURRENT[C_TABLE]&" not found."}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      dummy = IDB_texttonum(COMMAND[A_QUAL2])
      if not integer(dummy) or dummy < 1 or dummy > db_table_size() then
         CURRENT[C_MSG] = {"Position "&COMMAND[A_QUAL2]&" not found in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC] = 8
         return CURRENT
      else
         CURRENT = func_current(dummy)
      end if   
   else  
      if equal(PREVIOUS[C_POSN],{}) then 
         CURRENT[C_MSG] = {"No record current"}
         CURRENT[C_CC] = 8
      return CURRENT 
      end if
      oldrecbody = db_select_table(PREVIOUS[C_TABLE])
      oldrecbody = db_record_data(PREVIOUS[C_POSN])
      CURRENT = repeat({},C_MAX)
      if equal(COMMAND[A_OBJ],"owner") then
         if equal(oldrecbody[3][1],"@") then 
            proc_reset()
            CURRENT[C_MSG] ={"Owner record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][1]
            CURRENT[C_KEY] = oldrecbody[3][2]
         end if   
      elsif equal(COMMAND[A_OBJ],"prior") then
         if equal(oldrecbody[3][5],"@") then
            proc_reset()
            CURRENT[C_MSG] ={"Prior record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][5]
            CURRENT[C_KEY] = oldrecbody[3][6]
         end if 
      elsif equal(COMMAND[A_OBJ],"next") then
         if equal(oldrecbody[3][7],"@") then 
            proc_reset()
            CURRENT[C_MSG] ={"Next record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][7]
            CURRENT[C_KEY] = oldrecbody[3][8]
         end if 
      elsif equal(COMMAND[A_OBJ],"sub") then
         if equal(oldrecbody[3][3],"@") then 
            proc_reset()
            CURRENT[C_MSG] ={"Sub record does not exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         else
            CURRENT[C_TABLE] = oldrecbody[3][3]
            CURRENT[C_KEY] = oldrecbody[3][4]
         end if 
      else
         CURRENT[C_MSG] = {"Can't get "&COMMAND[A_OBJ]&", "&COMMAND[A_QUAL]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if
      CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLE])
      if CURRENT[C_MSG] < 0 then
         CURRENT[C_MSG] = {"Can't find table "&CURRENT[C_TABLE]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if   
      CURRENT[C_POSN] = db_find_key(CURRENT[C_KEY])
      if CURRENT[C_POSN] < 1 then
         CURRENT[C_MSG] = {"Can't find record key "&CURRENT[C_KEY]&" in table "&CURRENT[C_TABLE]&" GET routine"}
         CURRENT[C_CC]= 8
         return CURRENT
      end if   
      CURRENT = func_current(CURRENT[C_POSN])
      CURRENT[C_MSG] ={COMMAND[A_OBJ]&" record obtained"}
      CURRENT[C_CC] = 0
   end if
   
-- LINK ==========================================================================================================   
elsif equal(COMMAND[A_VERB],"link") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if func_raw_db() then
      CURRENT[C_MSG] ={"Links not available in EDS database"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if
   LINKER = repeat("@null",10)
   if equal(PREVIOUS[C_POSN],{}) then 
      CURRENT[C_MSG] = {"No record current"}
      CURRENT[C_CC] = 8
      return CURRENT 
   end if
   dummy = db_select_table(PREVIOUS[C_TABLE])
   oldrecbody = db_record_data(PREVIOUS[C_POSN])
   if equal(oldrecbody[1],"R") then 
      CURRENT[C_MSG] = {"Record locked against update"}
      proc_reset()
      return CURRENT
   end if   
   LINKER[L_CT] = PREVIOUS[C_TABLE]
   LINKER[L_CK] = PREVIOUS[C_KEY]
   CURRENT = func_tabkey(CURRENT) -- checks entered table and key, and returns record position for O/S/P/N record
   if CURRENT[C_CC] > 7 then return CURRENT end if
   -- Add link details of O/S/P/N record and original current record to linker sequence
   if equal(COMMAND[A_OBJ],"owner") then
      LINKER[L_OT] = COMMAND[A_QUAL]
      LINKER[L_OK] = COMMAND[A_QUAL2]
      -- retrieve owner record data 
      recbody = db_record_data(CURRENT[C_POSN])
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Owner record locked"}
         proc_reset()
         return CURRENT
      else
        if equal(recbody[3][3],"@") and equal(oldrecbody[3][1],"@") then
         --if owner sub link and current owner link are both free, link records
            recbody[3][3] = LINKER[L_CT]
            recbody[3][4] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody)
            -- retrieve original record data 
            oldrecbody[3][1] = LINKER[L_OT]
            oldrecbody[3][2] = LINKER[L_OK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody)
        elsif not equal(recbody[3][3],"@") and equal(oldrecbody[3][1],"@") and equal(oldrecbody[3][3],"@") then
         --if owner sub link odummyupied, but current sub link and current owner link are free, insert current between owner and sub
            LINKER[L_ST] = recbody[3][3]
            LINKER[L_SK] = recbody[3][4] 
            recbody[3][3] = LINKER[L_CT]
            recbody[3][4] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody)
           -- update original record data 
            oldrecbody[3][1] = LINKER[L_OT]
            oldrecbody[3][2] = LINKER[L_OK]
            oldrecbody[3][3] = LINKER[L_ST]
            oldrecbody[3][4] = LINKER[L_SK]
            dummy = db_select_table(PREVIOUS[C_TABLE])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody)
            -- find sub record and update
            CURRENT[C_MSG] = db_select_table(LINKER[L_ST])
            CURRENT[C_POSN] = db_find_key(LINKER[L_SK])
            recbody = db_record_data(CURRENT[C_POSN])
            if equal(recbody[1],"R") then 
               CURRENT[C_MSG] = {"Sub record locked"}
               proc_reset()
               return CURRENT
            else  
               recbody[3][1] = LINKER[L_CT]
               recbody[3][2] = LINKER[L_CK]
               db_replace_data(CURRENT[C_POSN],recbody)
            end if   
         else
            CURRENT[C_MSG] = {"Unable to link record to "&COMMAND[A_OBJ]&" as links already exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if   
      end if
      CURRENT[C_CC] = 0
   elsif equal(COMMAND[A_OBJ],"sub") then 
      LINKER[L_ST] = COMMAND[A_QUAL]
      LINKER[L_SK] = COMMAND[A_QUAL2]
      -- retrieve sub record data 
      recbody = db_record_data(CURRENT[C_POSN]) -- "linked to" record
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Sub record locked"}
         proc_reset()
         return CURRENT
      else
         if equal(recbody[3][1],"@") and equal(oldrecbody[3][3],"@") then
         --if linked to owner link and current sub link are both free, link records
            recbody[3][1] = LINKER[L_CT]
            recbody[3][2] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody)
            oldrecbody[3][3] = LINKER[L_ST]
            oldrecbody[3][4] = LINKER[L_SK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody)
         elsif not equal(recbody[3][1],"@") and equal(oldrecbody[3][3],"@") and equal(oldrecbody[3][1],"@") then
         --if linked to owner link odummyupied, but current sub link and current owner link are free, insert current between sub and owner
            LINKER[L_OT] = recbody[3][1]
            LINKER[L_OK] = recbody[3][2] 
            recbody[3][1] = LINKER[L_CT]
            recbody[3][2] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody) -- update linked to record
            -- update original record data 
            oldrecbody[3][1] = LINKER[L_OT]
            oldrecbody[3][2] = LINKER[L_OK]
            oldrecbody[3][3] = LINKER[L_ST]
            oldrecbody[3][4] = LINKER[L_SK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody) -- update old (inserted) record
            -- find owner record and update
            CURRENT[C_MSG] = db_select_table(LINKER[L_OT])
            CURRENT[C_POSN] = db_find_key(LINKER[L_OK])
            recbody = db_record_data(CURRENT[C_POSN])
            if equal(recbody[1],"R") then 
               CURRENT[C_MSG] = {"Owner record locked"}
               proc_reset()
               return CURRENT
            else  
               recbody[3][3] = LINKER[L_CT]
               recbody[3][4] = LINKER[L_CK]
               db_replace_data(CURRENT[C_POSN],recbody) -- update existing owner record
            end if
         else
            CURRENT[C_MSG] = {"Unable to link record to "&COMMAND[A_OBJ]&" as links already exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         CURRENT[C_CC] = 0
      end if   
   elsif equal(COMMAND[A_OBJ],"prior") then
      LINKER[L_PT] = COMMAND[A_QUAL]
      LINKER[L_PK] = COMMAND[A_QUAL2]
      -- retrieve prior record data 
      recbody = db_record_data(CURRENT[C_POSN]) -- "linked to" record
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Prior record locked"}
         proc_reset()
         return CURRENT
      else
         if equal(recbody[3][7],"@") and equal(oldrecbody[3][5],"@") then
         --if linked to next link and current prior link are both free, link records
            recbody[3][7] = LINKER[L_CT]
            recbody[3][8] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody)
            oldrecbody[3][5] = LINKER[L_PT]
            oldrecbody[3][6] = LINKER[L_PK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody)
         elsif not equal(recbody[3][7],"@") and equal(oldrecbody[3][5],"@") and equal(oldrecbody[3][7],"@") then
         --if linked to next link odummyupied, but current next link and current prior link are free, insert current between prior and next
            LINKER[L_NT] = recbody[3][7]
            LINKER[L_NK] = recbody[3][8] 
            recbody[3][7] = LINKER[L_CT]
            recbody[3][8] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody) -- update linked to record
            -- update original record data 
            oldrecbody[3][5] = LINKER[L_PT]
            oldrecbody[3][6] = LINKER[L_PK]
            oldrecbody[3][7] = LINKER[L_NT]
            oldrecbody[3][8] = LINKER[L_NK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody) -- update old (inserted) record
            -- find owner record and update
            CURRENT[C_MSG] = db_select_table(LINKER[L_NT])
            CURRENT[C_POSN] = db_find_key(LINKER[L_NK])
            recbody = db_record_data(CURRENT[C_POSN])
            if equal(recbody[1],"R") then 
               CURRENT[C_MSG] = {"Next record locked"}
               proc_reset()
               return CURRENT
            else
               recbody[3][5] = LINKER[L_CT]
               recbody[3][6] = LINKER[L_CK]
               db_replace_data(CURRENT[C_POSN],recbody) -- update existing next record
            end if   
         else
            CURRENT[C_MSG] = {"Unable to link record to "&COMMAND[A_OBJ]&" as links already exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         CURRENT[C_CC] = 0
      end if   
   elsif equal(COMMAND[A_OBJ],"next") then
      LINKER[L_NT] = COMMAND[A_QUAL]
      LINKER[L_NK] = COMMAND[A_QUAL2]
      -- retrieve prior record data 
      recbody = db_record_data(CURRENT[C_POSN]) -- "linked to" record
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Next record locked"}
         proc_reset()
         return CURRENT
      else
         if equal(recbody[3][5],"@") and equal(oldrecbody[3][7],"@") then
         --if linked to prior link and current next link are both free, link records
            recbody[3][5] = LINKER[L_CT]
            recbody[3][6] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody)
            oldrecbody[3][7] = LINKER[L_NT]
            oldrecbody[3][8] = LINKER[L_NK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody)
         elsif not equal(recbody[3][5],"@") and equal(oldrecbody[3][5],"@") and equal(oldrecbody[3][7],"@") then
         --if linked to prior link odummyupied, but current next link and current prior link are free, insert current between prior and next
            LINKER[L_PT] = recbody[3][5]
            LINKER[L_PK] = recbody[3][6] 
            recbody[3][5] = LINKER[L_CT]
            recbody[3][6] = LINKER[L_CK]
            db_replace_data(CURRENT[C_POSN],recbody) -- update linked to record
            -- update original record data 
            oldrecbody[3][5] = LINKER[L_PT]
            oldrecbody[3][6] = LINKER[L_PK]
            oldrecbody[3][7] = LINKER[L_NT]
            oldrecbody[3][8] = LINKER[L_NK]
            dummy = db_select_table(LINKER[L_CT])
            db_replace_data(PREVIOUS[C_POSN],oldrecbody) -- update old (inserted) record
            -- find owner record and update
            CURRENT[C_MSG] = db_select_table(LINKER[L_PT])
            CURRENT[C_POSN] = db_find_key(LINKER[L_PK])
            recbody = db_record_data(CURRENT[C_POSN])
            if equal(recbody[1],"R") then 
               CURRENT[C_MSG] = {"Prior record locked"}
               proc_reset()
               return CURRENT
            else
               recbody[3][7] = LINKER[L_CT]
               recbody[3][8] = LINKER[L_CK]
               db_replace_data(CURRENT[C_POSN],recbody) -- update existing prior record
            end if   
         else
            CURRENT[C_MSG] = {"Unable to link record to "&COMMAND[A_OBJ]&" as links already exist"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
      end if   
   else
      CURRENT[C_MSG] = {"Can't link "&COMMAND[A_OBJ]&", "&COMMAND[A_QUAL]}
      CURRENT[C_CC]= 8
   end if   
   CURRENT = repeat({},C_MAX)
   CURRENT[C_MSG] ={"Record linked to "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" "&COMMAND[A_QUAL2]}
   CURRENT[C_CC] = 0
   proc_reset()
-- LIST ==========================================================================================================   
elsif equal(COMMAND[A_VERB],"list") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   CURRENT = db_action({"return","current"})
   if equal(COMMAND[A_QUAL],"screen") then
      if report = 0 then
          CURRENT[C_MSG] = {"List to screen command not available in non-report or GUI mode"}
          CURRENT[C_CC] = 8
          return CURRENT
      else  
         filenum = 1
      end if
   else
      if equal(COMMAND[A_QUAL],"@null") then
         filenum = open(OUTPUTFILE,"w")
      else
         filenum = open(COMMAND[A_QUAL],"w")
      end if   
   end if 
   if equal(COMMAND[A_OBJ],"stack") then
      for count = 1 to STACKMAX do
         IDB_flatprint(filenum,{STACK[count]},{})
      end for  
   elsif equal(COMMAND[A_OBJ],"description") then
         IDB_flatprint(filenum,{DESCRIPTION},{})
   elsif equal(COMMAND[A_OBJ],"global") then
      IDB_flatprint(filenum,{"KEYVAL",KEYVAL,"RECSTAT",RECSTAT,"RECNAM",RECNAM,"LINKDAT",
         LINKDAT,"RECDAT",RECDAT,"RECBODY",RECBODY},{})
   end if   
   if filenum > 1 then close(filenum) end if
   CURRENT[C_CC] = 0 
   CURRENT = db_action({"current","database",BANKER[C_DB]})
   CURRENT = db_action({"current","table",BANKER[C_TABLE]})
   CURRENT = db_action({"get","record",BANKER[C_POSN]})
-- MODIFY ========================================================================================================== 
elsif equal(COMMAND[A_VERB],"modify") then
   DELTA = {0,0,0,0}
   CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if equal(PREVIOUS[C_POSN], {}) then
      CURRENT[C_MSG] = {"No current record found"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if   
   if equal(COMMAND[A_OBJ],"data") then
      if not equal(PREVIOUS[C_STATUS],"R") then
         CURRENT[C_POSN] = PREVIOUS[C_POSN]
         if not func_raw_db() then
            if not equal(COMMAND[A_QUAL],RECDAT) then
            -- Avoid parsetilde if gui input
               if find('~',COMMAND[A_QUAL]) then
                  COMMAND[A_QUAL] = IDB_parsetilde(COMMAND[A_QUAL])  
               end if   
            end if 
         else
            if not equal(COMMAND[A_QUAL],RECBODY) then
                if find('~',COMMAND[A_QUAL]) then
                  COMMAND[A_QUAL] = IDB_parsetilde(COMMAND[A_QUAL])
                end if  
            end if 
         end if
         if not func_raw_db() then
            COMMAND[A_QUAL] = {"U",PREVIOUS[C_NAME],PREVIOUS[C_LINKS],COMMAND[A_QUAL],"@@"}       
         end if
         db_replace_data(CURRENT[C_POSN],COMMAND[A_QUAL])
         DELTA[R_MOD]+=1
      else
         proc_reset()
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Record locked against update"}
      end if   
   elsif equal(COMMAND[A_OBJ],"field") then
      if not equal(PREVIOUS[C_STATUS],"R") then
         CURRENT[C_POSN] = PREVIOUS[C_POSN]
         recbody = db_record_data(CURRENT[C_POSN])
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
         if equal(COMMAND[A_QUAL2],"@null") then
            COMMAND[A_QUAL2] = {{}}
         else
            if not func_raw_db() then
               if not equal(COMMAND[A_QUAL2],RECDAT) then
                  if IDB_number(COMMAND[A_QUAL2]) then
                     dummy = value(COMMAND[A_QUAL2])
                     COMMAND[A_QUAL2] = dummy[2]
                  end if 
               end if 
            else
               if not equal(COMMAND[A_QUAL2],RECBODY) then
                  if IDB_number(COMMAND[A_QUAL2]) then
                     dummy = value(COMMAND[A_QUAL2])
                     COMMAND[A_QUAL2] = dummy[2]
                  end if 
               end if 
            end if
         end if
         if not func_raw_db() then
            recbody[4][COMMAND[A_QUAL]] = COMMAND[A_QUAL2]
         else
            recbody[COMMAND[A_QUAL]] = COMMAND[A_QUAL2]
         end if   
         db_replace_data(CURRENT[C_POSN],recbody)
         CURRENT = func_current(PREVIOUS[C_POSN])
         CURRENT[C_MSG] = {"Field modified"}
         DELTA[R_MOD]+=1
      else
         proc_reset()
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Record locked against update"}
      end if   
   elsif equal(COMMAND[A_OBJ],"links") then
      if func_raw_db() then
         CURRENT[C_MSG] ={"Modify record link not available in EDS database"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      if equal(PREVIOUS[C_STATUS],"U") then
         CURRENT[C_POSN] = PREVIOUS[C_POSN]
         recbody = db_record_data(CURRENT[C_POSN])
         if equal(COMMAND[A_QUAL],"LINKDAT") then
            recbody[3] = LINKDAT
         elsif equal(COMMAND[A_QUAL],"NOLINKS") then
            recbody[3] = NOLINKS
         elsif equal(COMMAND[3],"@null") then
            recbody[3] = LINKDAT
         else           
            recbody[3] = IDB_parsetilde(COMMAND[A_QUAL])
         end if
         db_replace_data(CURRENT[C_POSN],recbody)
         DELTA[R_MOD]+=1
         CURRENT = func_current(CURRENT[C_POSN])
         CURRENT[C_MSG] ={"Link data modified "&sprintf("%d",DELTA[R_MOD])}
         CURRENT[C_CC] = 0
         return CURRENT
      else
         DELTA[R_FAIL] +=1
         proc_reset()
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Record locked against update "&sprintf("%d",DELTA[R_FAIL])}
      end if   
   elsif equal(COMMAND[A_OBJ],"name") then
      if func_raw_db() then
         CURRENT[C_MSG] ={"Modify record name not available in EDS database"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      if equal(PREVIOUS[C_STATUS],"U") then
         recbody = db_record_data(PREVIOUS[C_POSN])
         recbody[2] = COMMAND[A_QUAL]
         db_replace_data(PREVIOUS[C_POSN],recbody)
         CURRENT = func_current(PREVIOUS[C_POSN])
         DELTA[R_MOD]+=1
      else
         proc_reset()
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Record locked against update"}
      end if   
   elsif equal(COMMAND[A_OBJ],"status") then
      if func_raw_db() then
         CURRENT[C_MSG] ={"Modify record status not available in EDS database"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      recbody = db_record_data(PREVIOUS[C_POSN])
      recbody[1] = COMMAND[A_QUAL]
      db_replace_data(PREVIOUS[C_POSN],recbody)
      CURRENT = func_current(PREVIOUS[C_POSN])
      DELTA[R_MOD]+=1
      CURRENT[C_MSG] = {"Record status changed"}
      CURRENT[C_CC] = 0 
   elsif equal(COMMAND[A_OBJ],"table") then
      if func_raw_db() then
         CURRENT[C_MSG] ={"Modify table status not available in EDS database"}
         CURRENT[C_CC] = 8
         return CURRENT
      end if
      CURRENT = func_current(1)
      for count = 1 to CURRENT[C_SIZE] do
         recbody = db_record_data(count)
         recbody[1] = COMMAND[A_QUAL]
         db_replace_data(count,recbody)
         DELTA[R_MOD]+=1
    end for  
      CURRENT = func_current(1)
      CURRENT[C_MSG] = {"Mod "&sprintf("%d",DELTA[R_MOD])&"; Table record status changed to "&COMMAND[A_QUAL]}
      CURRENT[C_CC] = 0 
    elsif equal(COMMAND[A_OBJ],"tabnam") then
         if func_raw_db() then
            CURRENT[C_MSG] ={"Modify all record names in table not available in EDS database"}
            CURRENT[C_CC] = 8
            return CURRENT
         end if
         CURRENT = func_current(1)
         for count = 1 to CURRENT[C_SIZE] do
            recbody = db_record_data(count)
            recbody[2] = COMMAND[A_QUAL]
            db_replace_data(count,recbody)
            DELTA[R_MOD]+=1
       end for  
       CURRENT = func_current(1)
       CURRENT[C_MSG] = {"Mod "&sprintf("%d",DELTA[R_MOD])&"; Record names changed to "&COMMAND[A_QUAL]}
       CURRENT[C_CC] = 0 
   end if   
-- OPEN ========================================================================================================  
elsif equal(COMMAND[A_VERB],"open") then
   if equal(COMMAND[A_OBJ],"database") then
      if equal(COMMAND[A_QUAL2],"none") then
         COMMAND[A_QUAL2] = DB_LOCK_NO
      elsif equal(COMMAND[A_QUAL2],"share") then
         COMMAND[A_QUAL2] = DB_LOCK_SHARED
      else
         COMMAND[A_QUAL2] = DB_LOCK_EXCLUSIVE
      end if
      concode = db_open(EDBDIR&COMMAND[A_QUAL],COMMAND[A_QUAL2])
      if concode  =  DB_OK then
         CURRENT[C_MSG] = {"IDB "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" opened"}
         CURRENT[C_CC] = 0
         CURRENT[C_DB] = db_return_currentdb()
         concode = db_select_table("Control")
         if concode = DB_OK then
            CURRENT[C_TABLE] = "Control"
            CURRENT[C_SIZE] = db_table_size()
            CURRENT[C_POSN] = 1
            dummy = db_find_key("readonly")
            if dummy > 0 then
               readonlydb = append(readonlydb,db_return_currentdb())
               readonly = 1
            end if
            recposn = db_find_key("IDB version")
            if recposn < 1 then
               CURRENT[C_MSG] = {"EDS "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" opened"}
               rawonlydb = append(rawonlydb,db_return_currentdb())
               CURRENT[C_DB] = db_return_currentdb()
              CURRENT[C_TABLE] = {}
              dummy = db_table_list()
              CURRENT[C_TABLE]  = dummy[1]   
               concode = db_select_table(CURRENT[C_TABLE] )
               if concode = DB_OK then
                  CURRENT[C_SIZE] = db_table_size()
                  CURRENT[C_POSN] = 1
               end if
            else
               recbody = db_record_data(recposn)
               if not equal(recbody[length(recbody)],"@@") then  
                  CURRENT[C_MSG] = {"EDS "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" opened"}
                  rawonlydb = append(rawonlydb,db_return_currentdb())
                  CURRENT[C_DB] = db_return_currentdb()
                  CURRENT[C_TABLE] = {}
                  dummy = db_table_list()
                  CURRENT[C_TABLE]  = dummy[1]   
                  concode = db_select_table(CURRENT[C_TABLE] )
                  if concode = DB_OK then
                     CURRENT[C_SIZE] = db_table_size()
                     CURRENT[C_POSN] = 1
                  end if
               end if 
            end if   
         else
            CURRENT[C_MSG] = {"EDS "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" opened"}
            rawonlydb = append(rawonlydb,db_return_currentdb())
            CURRENT[C_DB] = db_return_currentdb()
            CURRENT[C_TABLE] = {}
            dummy = db_table_list()
            if length(dummy) then
               CURRENT[C_TABLE]  = dummy[1]   
               concode = db_select_table(CURRENT[C_TABLE] )
               if concode = DB_OK then
                  CURRENT[C_SIZE] = db_table_size()
                  CURRENT[C_POSN] = 1
               end if
            else
               CURRENT[C_TABLE]  = {"No tables exist"}
            end if   
         end if
      else
         CURRENT[C_DATA] = concode
         CURRENT[C_MSG] = {"Unable to open "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]}
         CURRENT[C_CC]= 8
      end if
   elsif equal(COMMAND[A_OBJ],"file") and find(COMMAND[A_QUAL2],{"r","rb","w","wb","u","ub","a","ab"}) then
      if equal(OUTPUTFILE,{}) then
         CURRENT[C_MSG] = open(COMMAND[A_QUAL],COMMAND[A_QUAL2])
      else
         CURRENT[C_MSG] = open(OUTPUTFILE,COMMAND[A_QUAL2])
      end if   
      if CURRENT[C_MSG] = -1 then
         CURRENT[C_MSG] = {"Can't open (or locate) "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]}
         CURRENT[C_CC]= 8
      else
         CURRENT[C_MSG] = {"File "&COMMAND[A_QUAL]&" opened"}
         CURRENT[C_CC] = 0
      end if
   else
      CURRENT[C_MSG] = {"Can't open "&COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" with attribute "&COMMAND[A_QUAL2]}
      CURRENT[C_CC]= 8
   end if
   
-- PROCESS ==========================================================================================================   
elsif equal(COMMAND[A_VERB],"process") then
   if equal(COMMAND[A_OBJ],"file") then
      call_proc(proc_process,PROFDIR&{COMMAND[A_QUAL]})
      CURRENT[C_CC] = 0
   end if
-- QUIT ==========================================================================================================
elsif equal(COMMAND[A_VERB],"quit") then
   CURRENT[C_CC]= 16
   CURRENT[C_MSG] = {"Program halted"}  

-- RENAME ==========================================================================================================
elsif equal(COMMAND[A_VERB],"rename") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
	      CURRENT[C_MSG] = {"No database open"}
	      CURRENT[C_DB] = {"No database open"}
	      CURRENT[C_CC] = 8
	      return CURRENT
   end if
   if equal(COMMAND[A_OBJ],"table") then
   	if db_select_table(COMMAND[A_QUAL]) = DB_OK then 
			db_rename_table(COMMAND[A_QUAL], COMMAND[A_QUAL2])
			CURRENT[C_MSG] = db_select_table(COMMAND[A_QUAL2])
			CURRENT[C_TABLE] = db_return_currenttable()
			CURRENT[C_SIZE] = db_table_size()
			CURRENT[C_MSG] = {COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" has been renamed "&COMMAND[A_QUAL2]}
			CURRENT[C_POSN] = {}
			CURRENT[C_CC] = 0
		else
			CURRENT = db_action({"return","current"})
			CURRENT[C_MSG] = {COMMAND[A_OBJ]&" "&COMMAND[A_QUAL]&" not in current database"}
			CURRENT[C_POSN] = {}
			CURRENT[C_CC] = 8
		end if
   end if 

-- RETURN ==========================================================================================================  
elsif equal(COMMAND[A_VERB],"return") then
     if equal(COMMAND[A_OBJ],"current") then
         if not integer (CURRENT[C_POSN]) then CURRENT[C_POSN] = PREVIOUS[C_POSN] end if
         if not integer (CURRENT[C_POSN]) then CURRENT[C_POSN] = 1 end if
         CURRENT = func_current(CURRENT[C_POSN])
         CURRENT[C_MSG] ={"Current record details returned"}
         if CURRENT[C_CC] > 0 then
             CURRENT = IDB_zero()
             CURRENT[C_MSG] = {"Can't return "&COMMAND[A_OBJ]}
             CURRENT[C_CC]= 8
        end if
   elsif equal(COMMAND[A_OBJ],"list") then
      CURRENT[C_TABLIST] = db_table_list()
      if not integer (CURRENT[C_POSN]) then CURRENT[C_POSN] = PREVIOUS[C_POSN] end if
      if not integer (CURRENT[C_POSN]) then CURRENT[C_POSN] = 1 end if
      CURRENT = func_current(CURRENT[C_POSN])      
      CURRENT[C_MSG] ={"List of tables in current database returned"}
      if CURRENT[C_CC] > 0 then
          CURRENT = IDB_zero()
          CURRENT[C_MSG] = {"Can't return "&COMMAND[A_OBJ]}
          CURRENT[C_CC]= 8
        end if
   elsif equal(COMMAND[A_OBJ],"alldb") then
       CURRENT[C_DBLIST] = db_return_alldb()
       CURRENT[C_TABLE] = db_return_currenttable() 
       CURRENT[C_DB] = db_return_currentdb() 
       CURRENT[C_MSG] ={"List of open databases returned"}
       if CURRENT[C_CC] > 0 then
             CURRENT = IDB_zero()
             CURRENT[C_MSG] = {"Can't return "&COMMAND[A_OBJ]}
             CURRENT[C_CC]= 8
        end if
   elsif equal(COMMAND[A_OBJ],"description") then
     BANKER = PREVIOUS
        if not equal(BANKER[C_DB],{}) then            
        if IDBFLAG then
            CURRENT[C_CC] = 8
            CURRENT[C_MSG] = {"Field definitions not available in EDS database"}
        else
           concode = db_select_table("Description")
           DESCRIPTION = {}
           if concode = DB_OK then
             if not equal(BANKER[C_NAME],{}) then
                 F_CURRENT = db_action({"get","key",IDB_sflat(BANKER[C_NAME])})
                 if F_CURRENT[C_CC] = 0 then
                    CURRENT = db_action({"current","database",BANKER[C_DB]})
                    CURRENT = db_action({"current","table",BANKER[C_TABLE]})
                    CURRENT = db_action({"get","record",BANKER[C_POSN]})
                    CURRENT[C_DESC] = F_CURRENT[C_DATA]
                    DESCRIPTION = F_CURRENT[C_DATA]
                    CURRENT[C_MSG] = {"Field definitions found"}
                 else
                    CURRENT = db_action({"current","database",BANKER[C_DB]})
                    CURRENT = db_action({"current","table",BANKER[C_TABLE]})
                    CURRENT = db_action({"get","record",BANKER[C_POSN]})
                    CURRENT[C_CC] = 8
                    CURRENT[C_MSG] = {"Field definitions for record "&BANKER[C_NAME]&" not found"}
                 end if
              else
                  CURRENT = db_action({"current","database",BANKER[C_DB]})
                  CURRENT = db_action({"current","table",BANKER[C_TABLE]})
                  CURRENT = db_action({"get","record",BANKER[C_POSN]})
                  CURRENT[C_CC] = 8
                  CURRENT[C_MSG] = {"No current record"}
              end if
           else
               CURRENT = db_action({"current","database",BANKER[C_DB]})
               CURRENT = db_action({"current","table",BANKER[C_TABLE]})
               CURRENT = db_action({"get","record",BANKER[C_POSN]})
               CURRENT[C_CC] = 8
               CURRENT[C_MSG] = {"Table for field definitions not found"}
           end if   
        end if   
     end if   
   end if
    
-- SEARCH ======================================================================================================
elsif equal(COMMAND[A_VERB],"search") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   PREVIOUS[C_POSN] = 1
   if equal(COMMAND[A_OBJ],"table") then
      CURRENT = func_current(1)
      
      if CURRENT[C_CC]> 0 then
         return CURRENT
      end if
     -- proc_reset()
      CURRENT[C_HITS] = {}
      if equal(COMMAND[A_QUAL],"record") then
         --proc_reset()
         CURRENT[C_HITS] = {}
         for count = 1 to CURRENT[C_SIZE] do
            recbody = db_record_data(count)
            dummy = db_record_key(count)
            recbody = IDB_sflat(recbody)
            if match(COMMAND[A_QUAL2],recbody) > 0 then CURRENT[C_HITS] = 
                  append(CURRENT[C_HITS],{CURRENT[C_TABLE],count,dummy}) end if
         end for
         if equal(CURRENT[C_HITS],{}) then
            CURRENT[C_MSG] = {"No matches found in table "&CURRENT[C_TABLE]}
            CURRENT[C_CC] = 4
         else
            CURRENT[C_MSG] = {sprintf("%d",length(CURRENT[C_HITS]))&" match(es) found in database "&CURRENT[C_DB]}
         end if   
      elsif equal(COMMAND[A_QUAL],"key") then 
        -- proc_reset()
         CURRENT[C_HITS] = {}
         for count = 1 to CURRENT[C_SIZE] do
            recbody = db_record_key(count)
            recbody = IDB_sflat(recbody)
            if match(COMMAND[A_QUAL2],recbody) > 0 then CURRENT[C_HITS] = 
                  append(CURRENT[C_HITS],{CURRENT[C_TABLE],count,recbody}) end if
         end for
         if equal(CURRENT[C_HITS],{}) then
            CURRENT[C_MSG] = {"No matches found in table "&CURRENT[C_TABLE]}
            CURRENT[C_CC] = 4
         else
            CURRENT[C_MSG] = {sprintf("%d",length(CURRENT[C_HITS]))&" match(es) found in database "&CURRENT[C_DB]}
         end if
      else
         CURRENT[C_MSG] = {"Key or record not specified - "&COMMAND[A_QUAL]}
         CURRENT[C_CC] = 8
      end if   
   elsif equal(COMMAND[A_OBJ],"database") then 
      CURRENT[C_HITS] = {}
      CURRENT[C_TABLIST] = db_table_list()
      if equal(COMMAND[A_QUAL],"record") then
        for count = 1 to length(CURRENT[C_TABLIST]) do
            dummy = db_select_table(CURRENT[C_TABLIST][count])
             tablesize = db_table_size()
            if  tablesize > 0 then
               for count2 = 1 to  tablesize do
                  recbody = db_record_data(count2)
                  dummy2 = db_record_key(count2)
                  recbody = IDB_sflat(recbody)
                  if match(COMMAND[A_QUAL2],recbody) > 0 then CURRENT[C_HITS] = 
                        append(CURRENT[C_HITS],{CURRENT[C_TABLIST][count],count2,dummy2}) end if
               end for
            end if   
         end for
         if equal(CURRENT[C_HITS],{}) then
            CURRENT[C_MSG] = {"No matches found in database "&CURRENT[C_DB]}
            CURRENT[C_CC] = 4
         else
            CURRENT[C_MSG] = {sprintf("%d",length(CURRENT[C_HITS]))&" match(es) found in database "&CURRENT[C_DB]}
         end if      
      elsif equal(COMMAND[A_QUAL],"key") then 
         for count = 1 to length(CURRENT[C_TABLIST]) do
            dummy = db_select_table(CURRENT[C_TABLIST][count])
            tablesize = db_table_size() 
             if  tablesize > 0 then
               for count2 = 1 to  tablesize do
                  recbody = db_record_key(count2)
                  recbody = IDB_sflat(recbody)
                  if match(COMMAND[A_QUAL2],recbody) > 0 then CURRENT[C_HITS] = 
                        append(CURRENT[C_HITS],{CURRENT[C_TABLIST][count],count2,recbody}) end if
               end for
            end if   
         end for
         if equal(CURRENT[C_HITS],{}) then
            CURRENT[C_MSG] = {"No matches found in database "&CURRENT[C_DB]}
            CURRENT[C_CC] = 4
         else
            CURRENT[C_MSG] = {sprintf("%d",length(CURRENT[C_HITS]))&" match(es) found in database "&CURRENT[C_DB]}
         end if   
      else
         CURRENT[C_MSG] = {"Key or record not specified - "&COMMAND[A_QUAL]}
         CURRENT[C_CC] = 8
      end if   
   end if
   
-- SET ==========================================================================================================
elsif equal(COMMAND[A_VERB],"set") then
   if equal(COMMAND[A_OBJ],"edbdir") then
      if equal(COMMAND[A_QUAL],"@null") then EDBDIR = {} else EDBDIR = COMMAND[A_QUAL] end if
   elsif equal(COMMAND[A_OBJ],"profdir") then   
      if equal(COMMAND[A_QUAL],"@null") then PROFDIR = {} else PROFDIR = COMMAND[A_QUAL] end if
   elsif equal(COMMAND[A_OBJ],"outputfile") then   
      if equal(COMMAND[A_QUAL],"@null") then OUTPUTFILE = "output.txt" else OUTPUTFILE = COMMAND[A_QUAL] end if
   elsif equal(COMMAND[A_OBJ],"echofile") then   
      if equal(COMMAND[A_QUAL],"@null") then ECHOFILE = "echo.txt" else ECHOFILE = COMMAND[A_QUAL] end if   
   elsif equal(COMMAND[A_OBJ],"recdat") then 
      RECDAT = IDB_parsetilde(COMMAND[A_QUAL])
   elsif equal(COMMAND[A_OBJ],"keyval") then 
      KEYVAL = IDB_parsetilde(COMMAND[A_QUAL]) 
   elsif equal(COMMAND[A_OBJ],"recstat") then   
      RECSTAT = IDB_parsetilde(COMMAND[A_QUAL])
   elsif equal(COMMAND[A_OBJ],"recnam") then 
      RECNAM = IDB_parsetilde(COMMAND[A_QUAL]) 
   elsif equal(COMMAND[A_OBJ],"linkdat") then   
      LINKDAT = IDB_parsetilde(COMMAND[A_QUAL])
   elsif equal(COMMAND[A_OBJ],"recbody") then   
      RECBODY = IDB_parsetilde(COMMAND[A_QUAL])   
   elsif equal(COMMAND[A_OBJ],"stackmax") then  
      dummy = IDB_texttonum(COMMAND[A_QUAL])
      if dummy > STACKMAX then
         STACK = append(STACK,repeat({"@null","@null","@null","@null","@null"},dummy - STACKMAX))
         STACKMAX = dummy
      else
         STACK = STACK[1..dummy]
         STACKMAX = dummy
      end if   
   end if

-- (STACK) ==========================================================================================================
elsif equal(COMMAND[A_VERB],"push") then
   if equal(COMMAND[A_QUAL],"@null") then
      stackpos = STACKMAX + 1
      for count = 1 to STACKMAX do
         if equal(STACK[count][S_KEY],"@null") then
            stackpos = count
            exit
         end if
      end for 
      if stackpos > STACKMAX then
         CURRENT = PREVIOUS
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Stack full"}
         return CURRENT
      end if   
   else
      stackpos = IDB_texttonum(COMMAND[A_QUAL])
   end if   
   if stackpos > 0 and stackpos <= STACKMAX then
      CURRENT = PREVIOUS
      STACK[stackpos] = {CURRENT[C_KEY],CURRENT[C_STATUS],CURRENT[C_NAME],
            CURRENT[C_LINKS],CURRENT[C_DATA],CURRENT[C_BODY]}
      CURRENT = PREVIOUS
      CURRENT[C_CC] = 0
      CURRENT[C_MSG] = {"Data pushed onto stack in slot "&sprintf("%d",stackpos)}
      return CURRENT
   else
      CURRENT = PREVIOUS
      CURRENT[C_CC] = 8
      CURRENT[C_MSG] = {"Slot must be between 0 and "&sprintf("%d",STACKMAX)}
      return CURRENT
   end if
elsif equal(COMMAND[A_VERB],"pull") then
   if equal(COMMAND[A_QUAL],"@null") then
      stackpos = STACKMAX
      for count = STACKMAX to 1 by -1 do
         if equal(STACK[count][S_KEY],"@null") then
            stackpos = count - 1
         end if
      end for
      if stackpos > 0 then
         KEYVAL  = STACK[stackpos][S_KEY]
         RECSTAT = STACK[stackpos][S_STATUS]
         RECNAM  = STACK[stackpos][S_NAME]
         LINKDAT = STACK[stackpos][S_LINKS]
         RECDAT  = STACK[stackpos][S_DATA]
         RECBODY = STACK[stackpos][S_BODY]
         STACK[stackpos] = {"@null","@null","@null","@null","@null","@null"}
         CURRENT = PREVIOUS
         CURRENT[C_CC] = 0
         CURRENT[C_MSG] = {"Data pulled from slot "&sprintf("%d",stackpos)}
         return CURRENT
      else
         CURRENT = PREVIOUS
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Stack empty"}
         return CURRENT
      end if
   elsif equal(COMMAND[A_QUAL],"all") then
      STACK = repeat({"@null","@null","@null","@null","@null","@null"}, STACKMAX)
      CURRENT[C_CC] = 0
      CURRENT[C_MSG] = {"Stack entries deleted"}
   else
      stackpos = IDB_texttonum(COMMAND[A_QUAL])
      if stackpos > 0 and stackpos <= STACKMAX then
         KEYVAL  = STACK[stackpos][S_KEY]
         RECSTAT = STACK[stackpos][S_STATUS]
         RECNAM  = STACK[stackpos][S_NAME]
         LINKDAT = STACK[stackpos][S_LINKS]
         RECDAT  = STACK[stackpos][S_DATA]
         RECBODY = STACK[stackpos][S_BODY]
         STACK[stackpos] = {"@null","@null","@null","@null","@null","@null"}
         CURRENT[C_CC] = 0
         CURRENT[C_MSG] = {"Data pulled from slot "&sprintf("%d",stackpos)}
      else
         CURRENT = PREVIOUS
         CURRENT[C_CC] = 8
         CURRENT[C_MSG] = {"Slot must be between 1 and "&sprintf("%d",STACKMAX)}
         return CURRENT
      end if
   end if
   
-- STEP ============================================================================================================ 
elsif equal(COMMAND[A_VERB],"step") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if report = 0 then
      CURRENT[C_MSG] = {"Step command not available in non-report or GUI mode"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if
   CURRENT = func_current(PREVIOUS[C_POSN])
   if CURRENT[C_CC]= 8 then return CURRENT end if
   if editflag = 0 then
      text_color(BRIGHT_GREEN)
      IDB_flatprint(1,{"STEP MODE "},{"%d","%f","%s",0,0,'\n'})
      text_color(GREEN)
   end if
   editflag = 1
   if equal(COMMAND[A_OBJ],"field") then
      recbody = db_record_data(CURRENT[C_POSN])
      if equal(COMMAND[A_QUAL],"@null") then
         COMMAND[A_QUAL] = 1
      else  
         COMMAND[A_QUAL] = IDB_texttonum(COMMAND[A_QUAL])
      end if   
      if not func_raw_db() then
         if COMMAND[A_QUAL] > length(recbody[4]) then
            CURRENT[C_MSG] = {"Field number too large"}
            CURRENT[C_CC] = 8
         return CURRENT
         end if 
         proc_step(recbody[4],COMMAND[A_QUAL])
      else
         if COMMAND[A_QUAL] > length(recbody) then
            CURRENT[C_MSG] = {"Field number too large"}
            CURRENT[C_CC] = 8
            return CURRENT
            end if 
         proc_step(recbody,COMMAND[A_QUAL])
      end if
   end if         
   editflag = 0   
   text_color(BRIGHT_GREEN)
   IDB_flatprint(1,{"END STEP MODE "},{"%d","%f","%s",0,0,'\n'})
   text_color(GREEN)
   
-- UNLINK ==========================================================================================================
elsif equal(COMMAND[A_VERB],"unlink") then
	CURRENT[C_DB] = db_return_currentdb()
	if equal(CURRENT[C_DB],"No database open") then
		CURRENT[C_MSG] = {"No database open"}
		CURRENT[C_DB] = {"No database open"}
		CURRENT[C_CC] = 8
		return CURRENT
   end if
   if func_raw_db() then
      CURRENT[C_MSG] ={"Unlink not possible in EDS database"}
      CURRENT[C_CC] = 8
      return CURRENT
   end if
   LINKER = repeat("@null",10)
   -- get previous current record data
   if equal(PREVIOUS[C_LINKS],NOLINKS) then
      CURRENT[C_MSG] = {"No links in current record"}
      CURRENT[C_CC] = 4
      return CURRENT
   end if   
   if equal(PREVIOUS[C_POSN],{}) then 
      CURRENT[C_MSG] = {"No record current"}
      CURRENT[C_CC] = 8
      return CURRENT 
   end if  
   if equal(PREVIOUS[C_STATUS],"R") then 
      CURRENT[C_MSG] = {"Current record locked"}
      CURRENT[C_CC] = 8
      proc_reset()
      return CURRENT 
   end if 
   CURRENT[C_MSG] = db_select_table(PREVIOUS[C_TABLE])
   oldrecbody = db_record_data(PREVIOUS[C_POSN])
   LINKER[L_CT] = PREVIOUS[C_TABLE]
   LINKER[L_CK] = PREVIOUS[C_KEY]
   if equal(COMMAND[A_OBJ],"owner") then
      CURRENT[C_TABLE] = oldrecbody[3][1]
      CURRENT[C_KEY] = oldrecbody[3][2]
      if equal(CURRENT[C_TABLE],"@") then
         CURRENT[C_MSG] = {"No owner record found"}
         CURRENT[C_CC] = 4
         return CURRENT 
      end if   
      CURRENT[C_MSG] = db_select_table(CURRENT[C_TABLE])
      if CURRENT[C_MSG] < 0 then
         CURRENT[C_MSG] = {"Can't find table "&CURRENT[C_TABLE]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if   
      CURRENT[C_POSN] = db_find_key(CURRENT[C_KEY])
      if CURRENT[C_POSN] < 1 then
         CURRENT[C_MSG] = {"Can't find record key "&CURRENT[C_KEY]&" in table "&CURRENT[C_TABLE]}
         CURRENT[C_CC]= 8
         return CURRENT
      end if
      recbody = db_record_data(CURRENT[C_POSN])
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Owner record locked"}
         CURRENT[C_CC] = 8
         proc_reset()
         return CURRENT 
      else 
         recbody[3][3] = "@"
         recbody[3][4] = "@"
         db_replace_data(CURRENT[C_POSN],recbody)
         oldrecbody[3][1] = "@"
         oldrecbody[3][2] = "@"
         dummy = db_select_table(PREVIOUS[C_TABLE])
         db_replace_data(PREVIOUS[C_POSN],oldrecbody)
      end if
   elsif equal(COMMAND[A_OBJ],"sub") then
      CURRENT[C_TABLE] = oldrecbody[3][3]
      CURRENT[C_KEY] = oldrecbody[3][4]
      if equal(CURRENT[C_TABLE],"@") then
         CURRENT[C_MSG] = {"No owner record found"}
         CURRENT[C_CC] = 4
         return CURRENT 
      end if
      CURRENT = func_tabkeycur(CURRENT)   
      recbody = db_record_data(CURRENT[C_POSN])
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Sub record locked"}
         CURRENT[C_CC] = 8
         proc_reset()
         return CURRENT 
      else
         recbody[3][1] = "@"
         recbody[3][2] = "@"
         db_replace_data(CURRENT[C_POSN],recbody)
         oldrecbody[3][3] = "@"
         oldrecbody[3][4] = "@"
         dummy = db_select_table(PREVIOUS[C_TABLE])
         db_replace_data(PREVIOUS[C_POSN],oldrecbody)
      end if
   elsif equal(COMMAND[A_OBJ],"prior") then
      CURRENT[C_TABLE] = oldrecbody[3][5]
      CURRENT[C_KEY] = oldrecbody[3][6]
      if equal(CURRENT[C_TABLE],"@") then
         CURRENT[C_MSG] = {"No owner record found"}
         CURRENT[C_CC] = 4
         return CURRENT 
      end if
      CURRENT = func_tabkeycur(CURRENT)
      recbody = db_record_data(CURRENT[C_POSN])
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Prior record locked"}
         proc_reset()
         return CURRENT
      else
         recbody[3][7] = "@"
         recbody[3][8] = "@"
         db_replace_data(CURRENT[C_POSN],recbody)
         oldrecbody[3][5] = "@"
         oldrecbody[3][6] = "@"
         dummy = db_select_table(PREVIOUS[C_TABLE])
         db_replace_data(PREVIOUS[C_POSN],oldrecbody) 
      end if   
   elsif equal(COMMAND[A_OBJ],"next") then
      CURRENT[C_TABLE] = oldrecbody[3][7]
      CURRENT[C_KEY] = oldrecbody[3][8]
      if equal(CURRENT[C_TABLE],"@") then
         CURRENT[C_MSG] = {"No owner record found"}
         CURRENT[C_CC] = 4
         return CURRENT 
      end if
      CURRENT = func_tabkeycur(CURRENT)   
      recbody = db_record_data(CURRENT[C_POSN])
      if equal(recbody[1],"R") then 
         CURRENT[C_MSG] = {"Next record locked"}
         CURRENT[C_CC] = 8
         proc_reset()
         return CURRENT
      else
         recbody[3][5] = "@"
         recbody[3][6] = "@"
         db_replace_data(CURRENT[C_POSN],recbody)
         oldrecbody[3][7] = "@"
         oldrecbody[3][8] = "@"
         dummy = db_select_table(PREVIOUS[C_TABLE])
         db_replace_data(PREVIOUS[C_POSN],oldrecbody)
      end if
   elsif equal(COMMAND[A_OBJ],"all") then
      previoustable = PREVIOUS[C_TABLE]
      previousposn = PREVIOUS[C_POSN]
      CURRENT = db_action({"unlink","owner"})
      PREVIOUS[C_TABLE] = previoustable
      PREVIOUS[C_POSN] = previousposn
      CURRENT = db_action({"unlink","sub"})
      PREVIOUS[C_TABLE] = previoustable
      PREVIOUS[C_POSN] = previousposn
      CURRENT = db_action({"unlink","prior"})
      PREVIOUS[C_TABLE] = previoustable
      PREVIOUS[C_POSN] = previousposn
      CURRENT = db_action({"unlink","next"})
   elsif equal(COMMAND[A_OBJ],"table") then
      previoustable = PREVIOUS[C_TABLE]
      previousposn = PREVIOUS[C_POSN]
      CURRENT = func_current(1)
      for count = 1 to db_table_size() do
         PREVIOUS[C_TABLE] = previoustable
         PREVIOUS[C_POSN] = count
         recbody = db_record_data(count)
         if not equal(recbody[3],NOLINKS) then
            CURRENT = db_action({"unlink","all"})
         end if
      end for
      CURRENT = func_current(1)
   end if
   proc_reset()
   CURRENT[C_MSG] ={"Record(s) unlinked"}
   CURRENT[C_CC] = 0
-- "--" ====================================================================================================   
elsif equal(COMMAND[A_VERB],"--") then
   CURRENT = IDB_zero()
   CURRENT[C_CC] = 0 
   
-- END OF VERB SELECTION ===================================================================================
else
   proc_reset()
   CURRENT[C_CC]=  12
   CURRENT[C_MSG] = {"Command "&COMMAND[A_VERB]&COMMAND[A_OBJ]&" not processed"}
end if   
IDBFLAG = func_raw_db()
return CURRENT
end function
------------------------------------------------------------------------------------------------------------
global function IDB_parse(sequence dbinput)
-- Takes a string (entered on-line or through a program) and converts it from format
-- "verb object qualifier1 qualifier2 qualifier3 qualifier4" to 
-- COMMAND[{verb},{object},{qualifier1},{qualifier2},{qualifier3},{qualifier4}]
-- Also expands short form commands, and ensures verb and object are in lower case.
integer start, count, count2
integer onoff
object dbcomtemp
object para1, para2
if report = 0 and nofile !=1 then
   nofile = open("NUL","w")
elsif report = 1 and nofile !=1 then
  close(nofile)
  nofile = 1
end if 
PREVIOUS = CURRENT
CURRENT = repeat({},C_MAX)
CURRENT[C_TERM] = {"@@"}
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_CC] = 0
onoff = -1
COMMAND = repeat("@null",MAXWORDS)
if IDB_string(dbinput) then
   if length(dbinput) = 0 then   
      dbinput = "@null"
   end if
   --remove leading spaces
   while equal(dbinput[1],32) do
      dbinput = dbinput[2..length(dbinput)]
   end while
   --remove trailing spaces
   while equal(dbinput[length(dbinput)],32) do
      dbinput = dbinput[1..length(dbinput)-1]
   end while
   -- remove spaces from item in quotes, replace by 333
   for ploop = 1 to length(dbinput) do
         if equal(dbinput[ploop],34) then onoff = onoff*-1 end if
         if equal(dbinput[ploop],32) and onoff = 1 then dbinput[ploop] = 333 end if
   end for
   -- remove multiple spaces outside quotes
   while match("  ",dbinput) do
      dbinput = dbinput[1..match("  ",dbinput)] & dbinput[match("  ",dbinput)+2..length(dbinput)]
   end while  
   count = 1
   count2 = 0
   start = 1
   for ploop = 1 to length(dbinput) do
      if equal(dbinput[ploop],32) then
         count2 = ploop - 1
         COMMAND[count] = dbinput[start..count2]
         start = ploop + 1
         count += 1
       end if
   end for
   count2 = length(dbinput)
   COMMAND[count] = dbinput[start..count2]
   
   dbcomtemp = {}
   for count3 = 1 to length(COMMAND) do
      if length(COMMAND[count3]) > 0 then 
         dbcomtemp = append(dbcomtemp,COMMAND[count3])
      end if
   end for
   COMMAND = dbcomtemp
   --replace 333 in quotes with spaces, and remove quote marks 
   for counter = 1 to length(COMMAND) do
      if equal(COMMAND[counter][1],34) then
         dbcomtemp = COMMAND[counter]
         for ploop = 1 to length(dbcomtemp) do
            if equal(dbcomtemp[ploop],333) then dbcomtemp[ploop] = ' ' end if
         end for
         COMMAND[counter] = dbcomtemp[2..(length(dbcomtemp)-1)]
      end if   
   end for
   COMMAND = COMMAND & repeat("@null",MAXWORDS)
else
   dbinput = dbinput & repeat("@null",MAXWORDS)
   for ploop = 1 to MAXWORDS do
      COMMAND[ploop] = dbinput[ploop]
   end for
end if
COMMAND = COMMAND[1..MAXWORDS]
if equal(COMMAND[1],"@null") then
   COMMAND = OLDCOMMAND
else
   OLDCOMMAND = COMMAND
end if   
COMMAND[A_VERB] = lower(COMMAND[A_VERB])
if equal(COMMAND[A_VERB],"addr") then
   COMMAND = {"add","record"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL],"KEYVAL") then COMMAND[A_QUAL] = KEYVAL end if
   if equal(COMMAND[A_QUAL2],"RECNAM") then COMMAND[A_QUAL2] = RECNAM   end if
   if equal(COMMAND[A_QUAL3],"RECDAT") then COMMAND[A_QUAL3] = RECDAT end if 
elsif equal(COMMAND[A_VERB],"addc") then
   COMMAND = {"add","control"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"clod") then
   COMMAND = {"close","database"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"clof") then
   COMMAND = {"close","file"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"comd") then
   COMMAND = {"commit","database"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"comp") then
   COMMAND = {"compress","database"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"cons") then
   COMMAND = {"configure","switch"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"copd") then
   COMMAND = {"copy","database"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"copt") then
   COMMAND = {"copy","table"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"copr") then
   COMMAND = {"copy","record"}&COMMAND[2..5]    
elsif equal(COMMAND[A_VERB],"cred") then
   COMMAND = {"create","database"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"cret") then
   COMMAND = {"create","table"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"curd") then
   COMMAND = {"current","database"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"curt") then
   COMMAND = {"current","table"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"debd") then
   COMMAND = {"debug","database"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"debt") then
   COMMAND = {"debug","table"}&COMMAND[2..5] 
elsif equal(COMMAND[A_VERB],"debr") then
   COMMAND = {"debug","record"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"deldb") then
   COMMAND = {"delete","database"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"delr") then
   COMMAND = {"delete","record"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"deltt") then
   COMMAND = {"delete","table"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"edif") then
   COMMAND = {"edit","field"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"e") then
   COMMAND = {"exit","@null"}&COMMAND[2..5]     
elsif equal(COMMAND[A_VERB],"expd") then
   COMMAND = {"export","database"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"expt") then
   COMMAND = {"export","table"}&COMMAND[2..5] 
elsif equal(COMMAND[A_VERB],"expr") then
   COMMAND = {"export","record"}&COMMAND[2..5]    
elsif equal(COMMAND[A_VERB],"fin-") then
   COMMAND = {"find","minus"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"finf") then
   COMMAND = {"find","first"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"finl") then
   COMMAND = {"find","last"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"fin+") then
   COMMAND = {"find","plus"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"finn") then
   COMMAND = {"find","next"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"fino") then
   COMMAND = {"find","owner"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"finp") then
   COMMAND = {"find","prior"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"finr") then
   COMMAND = {"find","record"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"fins") then
   COMMAND = {"find","sub"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"fint") then
   COMMAND = {"find","tabkey"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"fini") then
   COMMAND = {"find","tabpos"}&COMMAND[2..5]     
elsif equal(COMMAND[A_VERB],"fink") then
   COMMAND = {"find","key"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL],"KEYVAL") then COMMAND[A_QUAL] = KEYVAL end if 
elsif equal(COMMAND[A_VERB],"get-") then
   COMMAND = {"get","minus"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"-") then
   COMMAND = {"get","minus"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"getf") then
   COMMAND = {"get","first"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"getl") then
   COMMAND = {"get","last"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"get+") then
   COMMAND = {"get","plus"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"+") then
   COMMAND = {"get","plus"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"getn") then
   COMMAND = {"get","next"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"geto") then
   COMMAND = {"get","owner"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"getp") then
   COMMAND = {"get","prior"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"getr") then
   COMMAND = {"get","record"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"gets") then
   COMMAND = {"get","sub"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"gett") then
   COMMAND = {"get","tabkey"}&COMMAND[2..5] 
elsif equal(COMMAND[A_VERB],"geti") then
   COMMAND = {"get","tabpos"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"getk") then
   COMMAND = {"get","key"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL],"KEYVAL") then COMMAND[A_QUAL] = KEYVAL end if   
elsif equal(COMMAND[A_VERB],"linn") then
   COMMAND = {"link","next"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"lino") then
   COMMAND = {"link","owner"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"linp") then
   COMMAND = {"link","prior"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"lins") then
   COMMAND = {"link","sub"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"liss") then
   COMMAND = {"list","stack"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"lisd") then
   COMMAND = {"list","description"}&COMMAND[2..5]    
elsif equal(COMMAND[A_VERB],"lisg") then
   COMMAND = {"list","global"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"modd") then
   COMMAND = {"modify","data"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL],"RECDAT") then COMMAND[A_QUAL] = RECDAT end if
   if equal(COMMAND[A_QUAL],"RECBODY") then COMMAND[A_QUAL] = RECBODY end if
elsif equal(COMMAND[A_VERB],"modl") then
   COMMAND = {"modify","links"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL],"LINKDAT") then COMMAND[A_QUAL] = LINKDAT end if
elsif equal(COMMAND[A_VERB],"modn") then
   COMMAND = {"modify","name"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL],"RECNAM") then COMMAND[A_QUAL] = RECNAM end if
elsif equal(COMMAND[A_VERB],"mods") then
   COMMAND = {"modify","status"}&COMMAND[2..5]  
   if equal(COMMAND[A_QUAL],"RECSTAT") then COMMAND[A_QUAL] = RECSTAT end if
elsif equal(COMMAND[A_VERB],"modt") then
   COMMAND = {"modify","table"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"mode") then
   COMMAND = {"modify","tabnam"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"modf") then
   COMMAND = {"modify","field"}&COMMAND[2..5]
   if equal(COMMAND[A_QUAL2],"RECDAT") then COMMAND[A_QUAL2] = RECDAT end if
   if equal(COMMAND[A_QUAL],"RECBODY") then COMMAND[A_QUAL] = RECBODY end if
elsif equal(COMMAND[A_VERB],"oped") then
   COMMAND = {"open","database"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"opef") then
   COMMAND = {"open","file"}&COMMAND[2..5]     
elsif equal(COMMAND[A_VERB],"prof") then
   COMMAND = {"process","file"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"push") then
   COMMAND = {"push","stack"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"pull") then
   COMMAND = {"pull","stack"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"rent") then
   COMMAND = {"rename","table"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"reta") then
   COMMAND = {"return","alldb"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"retc") then
   COMMAND = {"return","current"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"*") then
   COMMAND = {"return","current"}&COMMAND[2..5] 
elsif equal(COMMAND[A_VERB],"retd") then
   COMMAND = {"return","description"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"retl") then
   COMMAND = {"return","list"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"seat") then
   COMMAND = {"search","table"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"sead") then
   COMMAND = {"search","database"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"sete") then
   COMMAND = {"set","edbdir"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"setp") then
   COMMAND = {"set","profdir"}&COMMAND[2..5] 
elsif equal(COMMAND[A_VERB],"seto") then
   COMMAND = {"set","outputfile"}&COMMAND[2..5] 
elsif equal(COMMAND[A_VERB],"setc") then
   COMMAND = {"set","echofile"}&COMMAND[2..5]    
elsif equal(COMMAND[A_VERB],"stef") then
   COMMAND = {"step","field"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"unla") then
   COMMAND = {"unlink","all"}&COMMAND[2..5]  
elsif equal(COMMAND[A_VERB],"unln") then
   COMMAND = {"unlink","next"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"unlo") then
   COMMAND = {"unlink","owner"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"unlp") then
   COMMAND = {"unlink","prior"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"unls") then
   COMMAND = {"unlink","sub"}&COMMAND[2..5]
elsif equal(COMMAND[A_VERB],"unlt") then
   COMMAND = {"unlink","table"}&COMMAND[2..5]   
elsif equal(COMMAND[A_VERB],"--") then 
   COMMAND = {"--","@null"}&COMMAND[2..5]
end if
COMMAND[A_OBJ] = lower(COMMAND[A_OBJ])
if equal(COMMAND[A_OBJ],"db") then
   COMMAND[A_OBJ] = "database"
end if
para1 = find(COMMAND[A_VERB],{"add","close","commit","compress","configure","copy","create",
      "current","debug","delete","edit","exit","export","find","get","import","link","list",
      "modify","open","process","quit","rename","return","search","set","step","push","pull","unlink","--"})
para2 = find(COMMAND[A_OBJ],{"all","alldb","name","control","database","table","current","first","last",
      "list","size","record","@null","file","owner","sub","prior","edbdir","profdir","echofile","global",
      "outputfile","status","next","key","template","plus","minus","switch","data","links","on",
      "off","field","stack","stackmax","recdat","recnam","recstat","keyval","linkdat","recbody",
      "whole","part","tabkey","tabpos","tabnam","pause","description"})
if para1 !=0 and para2 != 0 then
   CURRENT = db_action(COMMAND)
else  
   CURRENT[C_CC]= 8
   CURRENT[C_MSG] = {"Command "&IDB_sflatprint(dbinput,{})&" not recognised"}
end if
if not equal(CURRENT[C_STATUS],"H") then
   return CURRENT
else
   CURRENT = repeat({},C_MAX)
   CURRENT[C_CC] = 4
   CURRENT[C_MSG] = {"Record hidden"}
   return CURRENT
end if   
end function
----------------------------------------------------------------------------------------------------
procedure proc_display()
object pad, dummy
if func_raw_db() then
   FIXED = YELLOW
   DYNAMIC = WHITE
else
   FIXED = YELLOW
   DYNAMIC = GREEN
end if
text_color(FIXED)
for count = 1 to C_KEY do
   if equal(CURRENT[count],{}) then
   -- display no entries
   else
      text_color(FIXED)
      printf(nofile,"%s",{DISPLAY[count]&" "})
      text_color(DYNAMIC)
      if integer(CURRENT[count]) then
         dummy = sprint(CURRENT[count])
      elsif atom(CURRENT[count]) then
         dummy = sprint(CURRENT[count])
      else 
         dummy = CURRENT[count]
      end if   
      IDB_flatprint(nofile,{dummy},{"%d","%f","%s",0,'~','\n'})
      if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[count]&" "}&{dummy},{"%d","%f","%s",0,32,'\n'}) end if
   end if
end for
if not func_raw_db() then
   for count = C_STATUS to C_TERM do
      if equal(CURRENT[count],{}) then
      -- display no entries
      elsif count = C_DATA then
         if length(CURRENT[C_NAME])= 0 then
            CURRENT[C_DATA] = {}
         elsif equal(CURRENT[C_NAME][1],'*') then
            text_color(FIXED)
            printf(nofile,"%s",{DISPLAY[count]&" "})
            text_color(DYNAMIC)
            IDB_flatprint(nofile,"<special format>",{"%d","%f","%s",0,'~','\n'})
            if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[count]&" "}&{"<special format>"},{"%d","%f","%s",0,32,'\n'}) end if
         else
            for count2 = 1 to length(CURRENT[C_DATA]) do
               text_color(FIXED)
               if count2 <10 then pad = " " else pad = "" end if
               if datatype = 1 then
                 printf(nofile,"%s",{"Item "&sprint(count2)&" "&IDB_dtype(CURRENT[count][count2])&pad&" : "})
               else
                 printf(nofile,"%s",{"Data item "&pad&sprint(count2)&" : "})
               end if   
               text_color(DYNAMIC)
               if integer(CURRENT[count][count2]) then
                  dummy = sprint(CURRENT[count][count2])
               elsif atom(CURRENT[count][count2]) then
                  dummy = sprint(CURRENT[count][count2])
               else 
                  dummy = CURRENT[count][count2]
               end if   
               IDB_flatprint(nofile,{dummy},{"%d","%f","%s",0,'~','\n'})
               if echo then
                  if datatype = 1 then
                     if echo then IDB_flatprint(echonum,{"<- "}&{"Item "&sprint(count2)&" "&IDB_dtype(CURRENT[count][count2])&pad&" : "}&{dummy},{"%d","%f","%s",0,32,'\n'}) end if
                  else
                     if echo then IDB_flatprint(echonum,{"<- "}&{"Data item "&pad&sprint(count2)&" : "}&{dummy},{"%d","%f","%s",0,32,'\n'}) end if
                  end if 
               end if
            end for  
         end if
      elsif count = C_LINKS and equal(CURRENT[C_LINKS],NOLINKS) and equal(CURRENT[C_POSN],{}) then
            -- skip line if no record current
      elsif count = C_LINKS and equal(CURRENT[C_LINKS],NOLINKS) then 
         CURRENT[C_LINKS] = "<none>"
         text_color(FIXED)
         printf(nofile,"%s",{DISPLAY[count]&" "})
         text_color(DYNAMIC)
         IDB_flatprint(nofile,{CURRENT[count]},{"%d","%f","%s",0,'~','\n'})
         if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[count]&" "}&{CURRENT[count]},{"%d","%f","%s",0,32,'\n'}) end if
         CURRENT[C_LINKS] = NOLINKS
      elsif count = C_LINKS and  not equal(CURRENT[C_LINKS],NOLINKS) then
         proc_links()
      elsif count = C_POSN or count = C_SIZE then
         text_color(FIXED)
         printf(nofile,"%s",{DISPLAY[count]&" "})
         text_color(DYNAMIC)
         printf(nofile,"%d",CURRENT[count])
         if echo then printf(echonum,"%s",{{"<- "}&{DISPLAY[count]&" "}&{IDB_texttonum(CURRENT[count])}}) end if
         puts(nofile,'\n')
         if echo then puts(echonum,'\n') end if
      else
         text_color(FIXED)
         if datatype = 1 and count = C_DATA then
            printf(nofile,"%s",{DISPLAY[count][1..length(DISPLAY[count])-6]&IDB_dtype(CURRENT[count])&" "&": "})
         else
            printf(nofile,"%s",{DISPLAY[count]&" "})
         end if
         text_color(DYNAMIC)
         IDB_flatprint(nofile,{CURRENT[count]},{"%d","%f","%s",0,'~','\n'})
         if datatype = 1 and count = C_DATA then
            if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[count][1..length(DISPLAY[count])-6]&IDB_dtype(CURRENT[count])&" "&": "}&{CURRENT[count]},{"%d","%f","%s",0,32,'\n'}) end if
         else
            if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[count]&" "}&{CURRENT[count]},{"%d","%f","%s",0,32,'\n'}) end if
         end if
      end if
   end for 
else 
   FIXED = YELLOW
   DYNAMIC = WHITE
   for count2 = 1 to length(CURRENT[C_BODY]) do
      text_color(FIXED)
      if count2 <10 then pad = " " else pad = "" end if
      if datatype = 1 then
        printf(nofile,"%s",{"Item "&sprint(count2)&" "&IDB_dtype(CURRENT[C_BODY][count2])&pad&" : "})
      else
         printf(nofile,"%s",{"Data item "&pad&sprint(count2)&" : "})
      end if   
      text_color(DYNAMIC)
      if integer(CURRENT[C_BODY][count2]) then
         dummy = sprint(CURRENT[C_BODY][count2])
      elsif atom(CURRENT[C_BODY][count2]) then
         dummy = sprint(CURRENT[C_BODY][count2])
      else 
         dummy = CURRENT[C_BODY][count2]
      end if   
      if datatype = 1 then
        if echo then IDB_flatprint(echonum,{"<- "}&{"Item "&sprint(count2)&" "&IDB_dtype(CURRENT[C_BODY][count2])&pad&" : "}&{dummy},{"%d","%f","%s",0,32,'\n'}) end if
      else
        if echo then IDB_flatprint(echonum,{"<- "}&{"Data item "&pad&sprint(count2)&" : "}&{dummy},{"%d","%f","%s",0,32,'\n'}) end if
      end if      
      IDB_flatprint(nofile,{dummy},{"%d","%f","%s",0,'~','\n'})
   end for  
end if  
for count = C_TABLIST to C_DESC do
   if equal(CURRENT[count],{}) then
      -- display no entries
   else
      text_color(FIXED)
      printf(1,"%s",{DISPLAY[count]&" "})
      text_color(DYNAMIC)
      if integer(CURRENT[count]) then
         dummy = sprint(CURRENT[count])
      elsif atom(CURRENT[count]) then
         dummy = sprint(CURRENT[count])
      else 
         dummy = CURRENT[count]
      end if   
      IDB_flatprint(nofile,{dummy},{"%d","%f","%s",0,'~','\n'})
      if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[count]&" "}&{dummy},{"%d","%f","%s",0,32,'\n'}) end if
   end if
end for 
text_color(FIXED)
printf(nofile,"%s",{DISPLAY[C_MAX-1]&" "})
if CURRENT[C_CC] = 0 then text_color(GREEN) end if
if CURRENT[C_CC] = 4 then text_color(BRIGHT_GREEN) end if
if CURRENT[C_CC] = 8 then text_color(YELLOW) end if
if CURRENT[C_CC] = 12 then text_color(RED) end if
if CURRENT[C_CC] = 16 then text_color(BRIGHT_RED) end if
IDB_flatprint(nofile,{CURRENT[C_CC]},{"%d","%f","%s",{},',','\n'})  
if echo then IDB_flatprint(echonum,{"<- "}&{DISPLAY[C_MAX-1]&" "}&{CURRENT[C_CC]},{"%d","%f","%s",0,32,'\n'}) end if
if echo then puts(echonum,'\n') end if
text_color(DYNAMIC)
if pause then proc_pause() end if
end procedure
------------------------------------------------------------------------------------------------------------
procedure file_process(object filename)
-- Process for reading 'script' file and performing database actions, e.g. data load
object dbinput
object fn
if FIRSTFLAG = 0 then
   CURRENT = IDB_parse("*")
   FIRSTFLAG = 1
end if
PREVIOUS = CURRENT
CURRENT = repeat({},C_MAX)
if equal(EDBDIR,{}) then CURRENT[C_DIR] = current_dir() else CURRENT[C_DIR] = EDBDIR end if
CURRENT[C_CC] = 0
fn = open(filename,"r")
dbinput = gets(fn)
if not atom(dbinput) then
   dbinput = dbinput[1..length(dbinput)-1]
end if
while not atom(dbinput) do
   if report then
      text_color(FIXED)
      printf(nofile,"%s",{"File command : "})
      text_color(DYNAMIC)
      printf(nofile,"%s",{dbinput})
      puts(nofile,'\n')
      CURRENT = IDB_parse(dbinput)
      if report then
         proc_display()
         puts(nofile,'\n')
         if CURRENT[C_CC]  > 4 and CURRENT[C_CC]  < 16 then
            if warning and nofile then
               CURRENT[C_CC] = func_warning({"AUTOMATIC PROCESSING PAUSED\nDo you want to continue?"})
               if CURRENT[C_CC] = 8 then
                  CURRENT[C_MSG] = {"Automatic processing paused"}
                  exit 
               end if
            else
               abort(4)
            end if   
         elsif CURRENT[C_CC]  = 16 then
            abort(16)
         end if
      end if
   else
      CURRENT = IDB_parse(dbinput)
   end if
   if CURRENT[C_CC] > 4 then
      CURRENT[C_MSG] = {"Automatic processing ended abnormally"}
      exit
   else
      CURRENT[C_MSG] = {"Automatic processing ended OK"}
   end if
   dbinput = gets(fn)
   if not atom(dbinput) then
      dbinput = dbinput[1..length(dbinput)-1]
   end if
end while
close(fn)
end procedure
------------------------------------------------------------------------------------------------------------
global procedure IDB_online_process()
-- capture on-line input, including multi-line input.
object dbinput
dbinput = {}
if FIRSTFLAG = 0 then
   CURRENT = IDB_parse("*")
   FIRSTFLAG = 1
end if
text_color(FIXED)
if length(NEXTCOMMAND) then
   NEXTCOMMAND = NEXTCOMMAND[1]
   dbinput = NEXTCOMMAND
   NEXTCOMMAND = {}
   text_color(DYNAMIC)
else
   IDB_flatprint(nofile,{"Enter command:"},{"%d","%f","%s",{},',',32})
   text_color(DYNAMIC)
   dbinput = prompt_string("")
   if length(dbinput) > 0 then
      while equal(dbinput[length(dbinput)],'&') do
         dbinput = dbinput[1..length(dbinput)-1]
         dbinput = dbinput&prompt_string("")
      end while   
   end if
end if
CURRENT = IDB_parse(dbinput)
proc_display()
puts(nofile,'\n')
end procedure
------------------------------------------------------------------------------------------------------------
proc_process = routine_id("file_process")
------------------------------------------------------------------------------------------------------------
