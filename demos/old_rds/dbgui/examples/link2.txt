-- "Link2 script to link book records to authors and genres"
current table book
get last
link owner author "David Weber"
get minus
link owner author "Stephen King"
get minus
link owner author "Peter F. Hamilton"
get minus
link owner author "Robert Jordan"
get minus
link owner author "Terry Pratchett"
get minus
link owner author "David Weber"
get minus
link owner author "David Weber"
get minus
link owner author "David Weber"
get minus
link owner author "Stephen King"
get minus
get last
link prior genre Fantasy
get minus
link prior genre Horror
get minus
link prior genre "Science Fiction"
get minus
link prior genre Fantasy
get minus
link prior genre Fantasy
get minus
link prior genre Fantasy
get minus
link prior genre "Science Fiction"
get minus
link prior genre "Science Fiction"
get minus
link prior genre Horror
quit

