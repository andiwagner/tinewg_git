library tinlib ;
{$MODE FPC}{$H+}

// fpc -otinlib32.dll -O2  "%f"
// ppcrossx64.exe  -otinlib64.dll -O2  "%f" 

Uses Strings,Windows,CommDlg,CommCtrl,ShlObj;

type
	TCustomColors = array[0..15] of COLORREF;
	TFileName = Array[0..Max_Path] Of Char;

const
	StrBufferConst :Pchar ='Stringbuffer';
	MaxCustomColors = 16;
	tinlibversion = 1;
	CustClrStat : TCustomColors = ($40,$80,$bf,$ff,$4000,$8000,$bf00,$ff00,
								$400000,$800000,$bf0000,$ff0000,
								$4040,$8080,$bfbf,$ffff);

var 
	SaveExit: Pointer;
	CustClr : TCustomColors;
	StrBuffer : Pchar;
	

function ChildFromPoint(hParent :HWnd; xcoord : integer; ycoord : integer) : HWnd; StdCall;
var coord : Tpoint;
begin
	coord.x:=xcoord;
	coord.y:=ycoord;
	ChildFromPoint:=HWnd(ChildWindowFromPoint(hParent,coord));
end;

function Strtoutf8(text : Pchar) : Pchar; StdCall;
var buffer : Pchar;
begin
	StrDispose(StrBuffer);
	buffer:=Pchar(AnsiToUtf8(text));
	StrBuffer:=StrNew(buffer);
	Strtoutf8:=StrBuffer;	
end;

function Utf8tostr(text : Pchar) : Pchar; StdCall;
var buffer : Pchar;
begin
	StrDispose(StrBuffer);
	buffer:=Pchar(Utf8ToAnsi(text));
	StrBuffer:=StrNew(buffer);
	Utf8tostr:=StrBuffer;
end;

procedure ResetCustCol(); StdCall;
begin
	CustClr:=CustClrStat;
end;

procedure SetCustCol(index : integer; col : COLORREF); StdCall;
begin
	if index < MaxCustomColors then
	begin
		CustClr[index]:=col;
	end;
end;

function GetCustCol (index : integer) : COLORREF; StdCall;
begin
	if index < MaxCustomColors then
		GetCustCol:=CustClr[index]
	else
		GetCustCol:=0;
end;

Function ChooseCol(HOwner : HWnd) : COLORREF; StdCall;	
var cc : TCHOOSECOLOR;
	
begin
    FillByte(cc,SizeOf(cc),0);   
	cc.lStructSize:=SizeOf(cc);
	cc.hwndOwner:=HOwner;
	cc.lpCustColors := @CustClr;
	cc.Flags:=CC_FULLOPEN;
	ChooseColor(@cc);
	ChooseCol:=cc.rgbResult;
end;


function callbackselect(handle :hwnd;umsg : uint; param : lparam; data : lparam): longint; StdCall;
begin
  if uMsg = 1 then
    SendMessage(handle, 1126, 1, data);
    
    callbackselect:=0;
end;

Function Dirdialog(handle : hwnd;title : Pchar; Defdir : Pchar;flags : uint ): Pchar; StdCall;
Var bi : BrowseInfo;
	pathstring : Tfilename;
	pidl : LPITEMIDLIST;

begin
	With bi Do
		Begin
			hwndOwner:=handle;
			pszDisplayName:= @pathstring;
			lpszTitle := title;
			ulFlags := flags;
			lpfn := @callbackselect;
			lParam := wParam(Defdir);
			iImage := 0;
		end;
		pidl:=shbrowseforfolder(@bi);
		if pointer(pidl) > pointer(0) then
			begin
				if SHGetPathFromIDList(pidl,@pathstring) then
					dirdialog:=@pathstring
				else
					dirdialog:=Pchar('');
				
			end;
end;

Function SelectFile(Save : Boolean;FName : Pchar; Filter : Pchar; HWindow : hwnd): Pchar; StdCall;
  
Var
  NameRec : OpenFileName;
  erg	  : boolean;
Begin
  FillChar(NameRec,SizeOf(NameRec),0);
  With NameRec Do
    Begin
      LStructSize := SizeOf(NameRec);
      HWndOwner   := HWindow;
      LpStrFilter := Filter;
      LpStrFile   := FName;
      NMaxFile    := Max_Path;
      Flags       := OFN_Explorer Or OFN_HideReadOnly;
      If not(Save) Then
        Begin
          Flags := Flags Or OFN_FileMustExist;
        End;
     // LpStrDefExt := Ext;
    End;
  If Save Then
      erg := GetSaveFileName(@NameRec)
  Else
      erg := GetOpenFileName(@NameRec);
      
  erg:=	erg or true; // bullshitcode to keep the compiler quiet
  SelectFile := NameRec.LpStrFile;
End;



function WinRegister( AppName :Pchar;backcol : handle; WindowProc : Pointer): Boolean; StdCall;
var
  WindowClass: WndClass;
begin
  WindowClass.Style := cs_hRedraw or cs_vRedraw;
  WindowClass.lpfnWndProc := WndProc(WindowProc);
  WindowClass.cbClsExtra := 0;
  WindowClass.cbWndExtra := 0;
  WindowClass.hInstance := system.MainInstance;
  WindowClass.hIcon := LoadIcon(0, idi_Application);
  WindowClass.hCursor := LoadCursor(0, idc_Arrow);
  WindowClass.hbrBackground := backcol;
  WindowClass.lpszMenuName := nil;
  WindowClass.lpszClassName := AppName;

  WinRegister := RegisterClass(WindowClass) <> 0;
end;

procedure SetTooltip(Tip : handle; control : handle; text : Pchar; width : integer); StdCall;
var ti : TOOLINFO;
begin
	FillByte(ti,SizeOf(ti),0);  
	ti.cbsize:=sizeof(ti);
	ti.uflags:=(TTF_IDISHWND or TTF_PARSELINKS or TTF_SUBCLASS);
	ti.uid:=control;
	ti.lpsztext:=text;
	SendMessage(tip,TTM_ADDTOOL,0,wparam(@ti));
	SendMessage(tip,TTM_SETMAXTIPWIDTH,0,lparam(width));
end;

procedure UpdateTooltip(Tip : handle; control : handle; text : Pchar; width : integer); StdCall;
var ti : TOOLINFO;
begin
	FillByte(ti,SizeOf(ti),0);  
	ti.cbsize:=sizeof(ti);
	ti.uflags:=(TTF_IDISHWND or TTF_PARSELINKS or TTF_SUBCLASS);
	ti.uid:=control;
	ti.lpsztext:=text;
	SendMessage(tip,TTM_UPDATETIPTEXTA,0,lparam(@ti));
	SendMessage(tip,TTM_SETMAXTIPWIDTH,0,lparam(width));
end;

procedure ShowBalloon(control : handle; title : Pchar; text : Pchar; icon : integer); StdCall;
var bi : EDITBALLOONTIP;
	ti : PWideChar;
	te : PWideChar;

begin
	//FillChar(x,sizeof(x),0);
	//FillChar(y,sizeof(y),0);
	//ti:=@x[1];
	//te:=@y[1];
	ti:=AllocMem(1024); //Allocate and clear mem
	te:=AllocMem(1024);
	StringToWideChar(title,ti,1024);
	StringToWideChar(text,te,1024);
	bi.cbStruct:=sizeof(bi);
	bi.pszTitle:=ti;
	bi.pszText:=te;
	bi.ttiIcon:=icon;
	SendMessage(control,EM_SHOWBALLOONTIP,0,lparam(@bi));
	FreeMem(ti);
	FreeMem(te);
end; 

function GetWindow(control : handle; index : integer): IntPtr; StdCall;
begin
	GetWindow:=IntPtr(GetWindowLongPtr(control,index));
end;

function SetWindow(control : handle; index : integer; newlong : LongInt): IntPtr; StdCall;
begin
	SetWindow:=IntPtr(SetWindowLongPtr(control,index,newlong));
end;
procedure DllExit;
begin
  // library exit code
  // writeln('Dll Exit.');
  StrDispose(StrBuffer);
  ExitProc := SaveExit;  // restore exit procedure chain
end;

exports
ChooseCol,
GetCustCol,
SetCustCol,
ResetCustCol,
WinRegister,
SetTooltip,
SelectFile,
Strtoutf8,
Utf8tostr,
ChildFromPoint,
UpdateTooltip,
ShowBalloon,
GetWindow,
SetWindow,
Dirdialog;

begin
    // writeln('DLL loaded.');
    StrBuffer:=StrNew(StrBufferCOnst);
    CustClr:=CustClrStat;
	SaveExit := ExitProc;  // save exit procedure chain
    ExitProc := @DllExit;  // install LibExit exit procedure
end .
