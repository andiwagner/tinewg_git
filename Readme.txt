tinEWG

tinEWG can be used to quickly create simple WinApi Programs for the OpenEuphoria (OE) 
Programinglanguage.
You will find OpenEuphoria on http://www.openeuphoria.org.

Installation:
Copy the files tinEWG.exw and tinEWG_const.ew, to your OpenEuphoria include folder, 
or put a copy of them in the
folder of your OE program.

A 'Hello World' program might look like this:

file: hello.exw

include tinewg.exw

Window("Hallo Welt")
WinMain()

execute it with: euiw hello.exw

 
